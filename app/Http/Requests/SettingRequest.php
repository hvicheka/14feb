<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'key' => [
                'required',
                'string',
                'regex:/(^[a-zA-Z_]+[a-zA-Z0-9\\-]*$)/u',
            ],
            'value' => ['required', 'string'],
            'image' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:5120'],  // 5MB
            'link' => ['nullable', 'url']
        ];
    }
}
