<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AboutUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $require = !empty($this->route('about')) ? 'nullable' : 'required';
        return [
            'title' => ['required', 'string'],
            'image' => [$require, 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:5120'],  // 5MB
            'description' => ['required', 'string'],
            'style' => ['nullable', 'integer', 'min:1']
        ];
    }
}
