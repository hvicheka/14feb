<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'categories.*' => ['number', 'distinct'],
            'ingredients.*' => ['number', 'distinct'],
            'benefits.*' => ['number', 'distinct'],
            'description' => ['string'],
            'status' => ['boolean'],
        ];
    }
}
