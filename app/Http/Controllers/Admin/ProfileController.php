<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeProfilePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Traits\ImageUploadTrait;
use App\Models\User;

class ProfileController extends Controller
{
    use ImageUploadTrait;

    public function profile()
    {
        return view('back-end.profile.index');
    }

    public function update_profile(UpdateProfileRequest $request)
    {
        $user = User::findOrFail(auth()->id());

        $user->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        if ($request->file('image')) {
            $user->update([
                'image' => $this->updateImage(auth()->user()->image, $request->file('image'))
            ]);
        }
        return redirect()->route('admin.profile')->with('success', 'Profile updated successfully');
    }

    public function change_password(ChangeProfilePasswordRequest $request)
    {
        $user = User::findOrFail(auth()->id());
        $user->update(['password' => bcrypt($request->input('new_password'))]);
        auth()->logout();

        return redirect()->route('login');
    }
}
