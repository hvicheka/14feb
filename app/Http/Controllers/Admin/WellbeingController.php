<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\WellbeingRequest;
use App\Http\Traits\ImageUploadTrait;
use App\Models\Wellbeing;

class WellbeingController extends Controller
{
    use ImageUploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wellbeings = Wellbeing::query()
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('back-end.wellbeing.index', [
            'wellbeings' => $wellbeings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wellbeing = new Wellbeing();
        return view('back-end.wellbeing.create', [
            'wellbeing' => $wellbeing
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(WellbeingRequest $request)
    {
        Wellbeing::create([
            'title' => $request->title,
            'description' => $request->description,
            'short_description' => $request->short_description,
            'image' => $this->upload($request->file('image')),
            'status' => isset($request->status)
        ]);

        return redirect()->route('admin.wellbeing.index')->with('success', 'Welbeing updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Wellbeing $wellbeing)
    {
        return view('back-end.wellbeing.show', [
            'wellbeing' => $wellbeing
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Wellbeing $wellbeing)
    {
        return view('back-end.wellbeing.edit', [
            'wellbeing' => $wellbeing
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(WellbeingRequest $request, Wellbeing $wellbeing)
    {
        $wellbeing->update([
            'title' => $request->title,
            'description' => $request->description,
            'short_description' => $request->short_description,
            'status' => isset($request->status)
        ]);

        if ($request->file('image')) {
            $imageName = $this->updateImage($wellbeing->image, $request->file('image'));
            $wellbeing->update(['image' => $imageName]);
        }

        return redirect()->route('admin.wellbeing.index')->with('success', 'Welbeing updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
