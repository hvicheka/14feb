<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use App\Http\Traits\ImageUploadTrait;
use App\Models\Page;

class PageController extends Controller
{
    use ImageUploadTrait;

    protected $message = 'Page updated successfully';

    public function index(Page $page)
    {
        return view('back-end.pages.index', [
            'page' => $page
        ]);
    }

    public function update(PageRequest $request, Page $page)
    {
        $this->update_page_info($page, $request);
        return redirect()->route('admin.pages.index', $page)->with('success', $this->message);
    }

    public function vibe_page()
    {
        $page = Page::findOrFail(Page::VIBE_PAGE);
        return view('back-end.pages.vibe', [
            'page' => $page
        ]);
    }


    public function update_vibe_page(PageRequest $request)
    {
        $page = Page::findOrFail(Page::VIBE_PAGE);
        $this->update_page_info($page, $request);
        return redirect()->route('admin.pages.vibe')->with('success', $this->message);
    }

    public function wellbeing_page()
    {
        $page = Page::findOrFail(Page::WELL_BEING_PAGE);
        return view('back-end.pages.wellbeing', [
            'page' => $page
        ]);
    }

    public function update_wellbeing_page(PageRequest $request)
    {
        $page = Page::findOrFail(Page::WELL_BEING_PAGE);
        $this->update_page_info($page, $request);
        return redirect()->route('admin.pages.wellbeing')->with('success', $this->message);
    }

    public function news_page()
    {
        $page = Page::findOrFail(Page::NEWS_PAGE);
        return view('back-end.pages.news', [
            'page' => $page
        ]);
    }

    public function update_page_info(Page $page, $data)
    {
        $page->update([
            'name' => $data->name,
            'description' => $data->description,
            'terms' => $data->terms,
            'enable_faq' => isset($data->enable_faq)
        ]);

        if ($data->file('image')) {
            $imageName = $this->updateImage($page->image, $data->file('image'));
            $page->update(['image' => $imageName]);
        }

        if ($data->file('title_image')) {
            $imageName = $this->updateImage($page->title_image, $data->file('title_image'));
            $page->update(['title_image' => $imageName]);
        }
    }
}
