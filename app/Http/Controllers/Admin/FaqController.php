<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FaqRequest;
use App\Models\Page;
use App\Models\PageFaq;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Page $page)
    {

        $faqs = PageFaq::query()
            ->with('page')
            ->where('page_id', $page->id)
            ->paginate(10);

        return view('back-end.faq.index', [
            'page' => $page,
            'faqs' => $faqs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Page $page)
    {
        $faq = new PageFaq();
        return view('back-end.faq.create', [
            'page' => $page,
            'faq' => $faq,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqRequest $request, Page $page)
    {
        $page->faqs()->create($request->validated());
        return redirect()->route('admin.pages.faq.index', $page)->with('success', 'Faq created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page, PageFaq $faq)
    {
        return redirect()->route('admin.pages.faq.index', $page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page, PageFaq $faq)
    {
        return view('back-end.faq.edit', $page, [
            'page' => $page,
            'faq' => $faq,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqRequest $request, Page $page, PageFaq $faq)
    {
        $faq->update($request->validated());
        return redirect()->route('admin.pages.faq.index', $page)->with('success', 'Faq updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page, PageFaq $faq)
    {
        $faq->delete();
        return redirect()->route('admin.pages.faq.index', $page)->with('success', 'Faq deleted successfully');
    }
}
