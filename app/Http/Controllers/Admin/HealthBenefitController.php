<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\HealthBenefitRequest;
use App\Http\Traits\ImageUploadTrait;
use App\Models\HealthBenefit;

class HealthBenefitController extends Controller
{
    use ImageUploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $benefits = HealthBenefit::orderBy('id', 'desc')->paginate(10);
        return view('back-end.benefits.index', [
            'benefits' => $benefits
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $benefit = new HealthBenefit();
        return view('back-end.benefits.create', [
            'benefit' => $benefit
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(HealthBenefitRequest $request)
    {
        HealthBenefit::create([
            'name' => $request->name,
            'image' => $this->upload($request->file('image'))
        ]);
        return redirect()->route('admin.benefits.index')->with('success', 'Health benefit created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admin.benefits.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(HealthBenefit $benefit)
    {
        return view('back-end.benefits.edit', [
            'benefit' => $benefit
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(HealthBenefitRequest $request, HealthBenefit $benefit)
    {
        if ($request->file('image')) {
            $imageName = $this->updateImage($benefit->image, $request->file('image'));
        } else {
            $imageName = $benefit->image;
        }
        $benefit->update([
            'name' => $request->name,
            'image' => $imageName
        ]);
        return redirect()->route('admin.benefits.index')->with('success', 'Health benefit updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(HealthBenefit $benefit)
    {
        $benefit->delete();
        return redirect()->route('admin.categories.index')->with('success', 'Health benefit deleted successfully');
    }
}
