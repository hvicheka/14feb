<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AboutUsRequest;
use App\Http\Traits\ImageUploadTrait;
use App\Models\AboutUs;

class AboutUsController extends Controller
{
    use ImageUploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = AboutUs::query()
            ->orderBy('id', 'desc')
            ->paginate(10);
        return view('back-end.about_us.index', [
            'abouts' => $abouts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $about = new AboutUs();
        return view('back-end.about_us.create', [
            'about' => $about
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutUsRequest $request)
    {
        AboutUs::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $this->upload($request->file('image')),
            'style' => $request->style
        ]);
        return redirect()->route('admin.about.index')->with('success', 'About us created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(AboutUs $about)
    {
        return redirect()->route('admin.about.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AboutUs $about)
    {
        return view('back-end.about_us.edit', [
            'about' => $about
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AboutUsRequest $request, AboutUs $about)
    {
        $about->update([
            'title' => $request->title,
            'description' => $request->description,
            'status' => isset($request->status)
        ]);
        if ($request->file('image')) {
            $imageName = $this->updateImage($about->image, $request->file('image'));
            $about->update(['image' => $imageName]);
        }
        return redirect()->route('admin.about.index')->with('success', 'About us updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AboutUs $about)
    {
        $about->delete();
        return redirect()->route('admin.about.index')->with('success', 'About us deleted successfully');
    }
}
