<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Traits\ImageUploadTrait;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $except_user;

    use ImageUploadTrait;

    public function __construct()
    {
        $this->except_user = [1];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        array_push($this->except_user, auth()->id());
        $users = User::query()
            ->whereNotIn('id', $this->except_user)
            ->paginate(10);

        return view('back-end.users.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        return view('back-end.users.create', [
            'user' => $user
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'image' => $this->upload($request->file('image')),
        ]);
        return redirect()->route('admin.users.index')->with('success', 'User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return redirect()->route('admin.users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('back-end.users.edit', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        array_push($this->except_user, auth()->id());
        if (in_array($user->id, $this->except_user)) {
            abort(404);
        }
        $user->update([
            'name' => $request->name,
            'email' => $request->email
        ]);
        if ($request->password) {
            $user->update(['password'], bcrypt($request->password));
        }
        if ($request->file('image')) {
            $user->update(['image' => $this->updateImage($user->image, $request->file('image'))]);
        }
        return redirect()->route('admin.users.index')->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        array_push($this->except_user, auth()->id());
        if (in_array($user->id, $this->except_user)) {
            abort(404);
        }
        $user->delete();
        return redirect()->route('admin.users.index')->with('success', 'User deleted successfully');
    }
}
