<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Traits\ImageUploadTrait;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ImageUploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->paginate(10);
        return view('back-end.products.index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product();
        return view('back-end.products.create', [
            'product' => $product
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'status' => isset($request->status)
        ]);

        $categories = $request->categories;
        $ingredients = $request->ingredients;
        $benefits = $request->benefits;

        if (is_array($categories)) {
            $product->categories()->attach($categories);
        }
        if (is_array($ingredients)) {
            $product->ingredients()->attach($ingredients);
        }
        if (is_array($benefits)) {
            $product->benefits()->attach($benefits);
        }
        $imagUpdate = $product->images;

        $main_image = $request->file('main_image');
        $thumbnail_image = $request->file('thumbnail_image');
        $cup_shadow_image = $request->file('cup_shadow_image');
        $ingredient_image = $request->file('ingredient_image');
        if ($main_image) {
            $imagUpdate['main_image'] = $this->updateImage($imagUpdate['main_image'] ?? 'no.jpg', $main_image);
        }
        if ($thumbnail_image) {
            $imagUpdate['thumbnail_image'] = $this->updateImage($imagUpdate['thumbnail_image'] ?? 'no.jpg', $thumbnail_image);
        }
        if ($cup_shadow_image) {
            $imagUpdate['cup_shadow_image'] = $this->updateImage($imagUpdate['cup_shadow_image'] ?? 'no.jpg', $cup_shadow_image);
        }
        if ($ingredient_image) {
            $imagUpdate['ingredient_image'] = $this->updateImage($imagUpdate['ingredient_image'] ?? 'no.jpg', $ingredient_image);
        }
        $product->update(['images' => $imagUpdate]);
        return redirect()->route('admin.products.index')->with('success', 'Product created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admin.products.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product = $product->load(['categories', 'ingredients', 'benefits']);
        return view('back-end.products.edit', [
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'status' => isset($request->status)
        ]);
        $categories = $request->categories;
        $ingredients = $request->ingredients;
        $benefits = $request->benefits;

        if (is_array($categories)) {
            $product->categories()->sync($categories);
        }

        if (is_array($ingredients)) {
            $product->ingredients()->sync($ingredients);
        }
        if ($benefits && is_array($benefits)) {
            $product->benefits()->sync($benefits);
        }
        $imagUpdate = $product->images;

        $main_image = $request->file('main_image');
        $thumbnail_image = $request->file('thumbnail_image');
        $cup_shadow_image = $request->file('cup_shadow_image');
        $ingredient_image = $request->file('ingredient_image');
        if ($main_image) {
            $imagUpdate['main_image'] = $this->updateImage($imagUpdate['main_image'] ?? 'no.jpg', $main_image);
        }
        if ($thumbnail_image) {
            $imagUpdate['thumbnail_image'] = $this->updateImage($imagUpdate['thumbnail_image'] ?? 'no.jpg', $thumbnail_image);
        }
        if ($cup_shadow_image) {
            $imagUpdate['cup_shadow_image'] = $this->updateImage($imagUpdate['cup_shadow_image'] ?? 'no.jpg', $cup_shadow_image);
        }
        if ($ingredient_image) {
            $imagUpdate['ingredient_image'] = $this->updateImage($imagUpdate['ingredient_image'] ?? 'no.jpg', $ingredient_image);
        }
        $product->update(['images' => $imagUpdate]);
        return redirect()->route('admin.products.index')->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('admin.products.index')->with('success', 'Product deleted successfully');
    }
}
