<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use App\Http\Traits\ImageUploadTrait;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    use ImageUploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->input('q');
        $settings = Setting::query()
            ->when($q, function ($query) use ($q) {
                return $query->where('key', 'like', "%$q%")
                    ->orWhere('value', 'like', "%$q%");
            })
            ->paginate(30);
        return view('back-end.settings.index', [
            'settings' => $settings
        ]);
    }


    public function create()
    {
        $setting = new Setting();
        return view('back-end.settings.create', [
            'setting' => $setting
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingRequest $request)
    {
        Setting::updateOrCreate([
            'key' => $request->key
        ], [
            'value' => $request->value,
            'link' => $request->link,
            'image' => $this->upload($request->file('image'))
        ]);

        return redirect()->route('admin.settings.index')->with('success', 'Setting created successfully');
    }


    public function show(Setting $setting)
    {
        return redirect()->route('admin.settings.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        return view('back-end.settings.edit', [
            'setting' => $setting
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingRequest $request, Setting $setting)
    {
        $setting->update([
            'value' => $request->value,
            'link' => $request->link,
        ]);
        if ($request->file('image')) {
            $imageName = $this->updateImage($setting->image, $request->file('image'));
            $setting->update(['image' => $imageName]);
        }
        return redirect()->route('admin.settings.index')->with('success', 'Setting updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        $setting->delete();
        return redirect()->route('admin.settings.index')->with('success', 'Setting deleted successfully');

    }
}
