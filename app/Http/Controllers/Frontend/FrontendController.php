<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Page;
use App\Models\PageFaq;
use App\Models\Post;
use App\Models\Product;
use App\Models\Wellbeing;

class FrontendController extends Controller
{

    //    Home Page
    public function home()
    {
        $home_page = Page::findOrFail(Page::HOME_PAGE);

        $post = Post::query()->latest()->first();

        $vibe = Page::findOrFail(Page::VIBE_PAGE);

        $find_us = Page::findOrFail(Page::FIND_US_PAGE);

        return view('front_end.home', [
            'post' => $post,
            'vibe' => $vibe,
            'find_us' => $find_us,
            'home_page' => $home_page
        ]);
    }

    // Drink Page

    public function products()
    {
        $page = Page::findOrFail(Page::DRINK_PAGE);
        $products = Product::query()->with(['categories', 'ingredients', 'benefits'])->active()->orderBy('id', 'desc')->get();
        return view('front_end.drinks', [
            'products' => $products,
            'page' => $page
        ]);
    }

    public function product_detail($slug)
    {
        $page = Page::findOrFail(Page::DRINK_PAGE);

        $product = Product::query()
            ->where('slug', $slug)
            ->where('status', 1)
            ->firstOrFail();

        $products = Product::query()
            ->where('status', 1)
            ->whereNotIn('id', [$product->id])
            ->get();

        return view('front_end.drink_detail', [
            'product' => $product,
            'products' => $products,
            'page' => $page
        ]);
    }

    // Post or News Page
    public function posts()
    {
        $page = Page::findOrFail(Page::NEWS_PAGE);
        $posts = Post::query()
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->paginate(20);

        $mostViewsPosts = Post::query()
            ->where('status', 1)
            ->orderBy('view', 'desc')
            ->take(10)
            ->get();

        return view('front_end.news.index', [
            'posts' => $posts,
            'page' => $page,
            'most_view_posts' => $mostViewsPosts
        ]);
    }

    public function postShow($slug)
    {
        $page = Page::findOrFail(Page::NEWS_PAGE);

        $post = Post::query()
            ->where('status', 1)
            ->where('slug', $slug)
            ->firstOrFail();

        if (!request()->session()->get('post_id') == $post->id) {
            $post->increment('view');
            request()->session()->put('post_id', $post->id);
        }

        $mostViewsPosts = Post::query()
            ->whereNotIn('id', [$post->id])
            ->where('status', 1)
            ->orderBy('view', 'desc')
            ->take(10)
            ->get();

        return view('front_end.news.detail', [
            'post' => $post,
            'most_view_posts' => $mostViewsPosts,
            'page' => $page
        ]);
    }

    // About us Page

    public function about_us()
    {
        $page = Page::findOrFail(Page::ABOUT_PAGE);
        $abouts = AboutUs::query()
            ->paginate(20);
        return view('front_end.about_us', [
            'abouts' => $abouts,
            'page' => $page
        ]);
    }

    // Wellbeing Page

    public function wellbeing()
    {
        $page = Page::findOrFail(Page::WELL_BEING_PAGE);
        $wellbeings = Wellbeing::query()
            ->select(['id', 'title', 'slug', 'image', 'short_description'])
            ->where('status', 1)
            ->paginate(20);
        return view('front_end.wellbeing.index', [
            'wellbeings' => $wellbeings,
            'page' => $page
        ]);
    }

    public function wellbeing_detail($slug)
    {
        $page = Page::findOrFail(Page::WELL_BEING_PAGE);

        $wellbeing = Wellbeing::query()
            ->where('status', 1)
            ->where('slug', $slug)
            ->firstOrFail();
        if (!request()->session()->get('wellbeing_id') == $wellbeing->id) {
            $wellbeing->increment('view');
            request()->session()->put('wellbeing_id', $wellbeing->id);
        }

        $popular = Wellbeing::query()
            ->select(['id', 'title', 'slug', 'image'])
            ->where('status', 1)
            ->whereNotIn('id', [$wellbeing->id])
            ->orderBy('view', 'desc')
            ->take(10)
            ->get();

        return view('front_end.wellbeing.detail', [
            'wellbeing' => $wellbeing,
            'popular' => $popular,
            'page' => $page
        ]);
    }

    // Find Us Page

    public function store()
    {
        $page = Page::findOrFail(Page::FIND_US_PAGE);
        return view('front_end.stores', [
            'page' => $page
        ]);
    }

    // Vibe Page

    public function vibe()
    {
        $page = Page::findOrFail(Page::VIBE_PAGE);
        $faqs = PageFaq::query()
            ->with('page')
            ->where('page_id', $page->id)
            ->get();;
        return view('front_end.vibe', [
            'page' => $page,
            'faqs' => $faqs
        ]);
    }

    public function privacy()
    {
        $page = Page::findOrFail(Page::PRIVACY_PAGE);
        return view('front_end.privacy_policy', [
            'page' => $page
        ]);
    }

    public function term_and_condition()
    {
        $page = Page::findOrFail(Page::TERM_AND_CONDITION_PAGE);
        return view('front_end.term', [
            'page' => $page
        ]);
    }

    public function contact()
    {
        $page = Page::findOrFail(Page::CONTACT_PAGE);

        return view('front_end.contact', [
            'page' => $page,
        ]);
    }

    public function jobs()
    {
        $page = Page::findOrFail(Page::JOB_PAGE);
        $faqs = PageFaq::query()
            ->with('page')
            ->where('page_id', $page->id)
            ->get();

        return view('front_end.jobs', [
            'page' => $page,
            'faqs' => $faqs
        ]);
    }

    public function franchising()
    {
        $page = Page::findOrFail(Page::FRANCHISING_PAGE);

        $faqs = PageFaq::query()
            ->with('page')
            ->where('page_id', $page->id)
            ->get();

        return view('front_end.franchising.index', [
            'page' => $page,
            'faqs' => $faqs
        ]);
    }
}
