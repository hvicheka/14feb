<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\HealthBenefit;
use App\Models\Ingredient;
use App\Models\Page;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use stdClass;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        view()->composer(['back-end.products.create', 'back-end.products.edit'], function ($view) {
            $categories = Category::all(['id', 'name', 'slug', 'image']);
            $ingredients = Ingredient::all(['id', 'name']);
            $benefits = HealthBenefit::all(['id', 'name', 'slug', 'image']);
            $view->with([
                'categories' => $categories,
                'ingredients' => $ingredients,
                'benefits' => $benefits
            ]);
        });

        view()->composer(['front_end.drinks'], function ($view) {
            $categories = Category::all(['id', 'name', 'slug', 'image']);
            $benefits = HealthBenefit::all(['id', 'name', 'slug', 'image']);
            $view->with([
                'categories' => $categories,
                'benefits' => $benefits
            ]);
        });

        view()->composer(['back-end.layout.partials.sidebar'], function ($view) {
            $all_pages = Page::query()->whereNotIn('id', [Page::VIBE_PAGE])->orderBy('id')->get();
            $faq_pages = Page::query()->where('enable_faq', 1)->get();
            $view->with([
                'faq_pages' => $faq_pages,
                'all_pages' => $all_pages
            ]);
        });

        view()->composer(['back-end.layout.partials.sidebar', 'front_end.layouts.partials.header'], function ($view) {
            $logo = Setting::query()->where('key', 'logo')->first();
            $view->with([
                'logo' => $logo
            ]);
        });
        view()->composer(['back-end.layout.app', 'front_end.*'], function ($view) {
            $settings = Setting::all();

            $setting = new stdClass();
            foreach ($settings as $k => $v) {
                $setting->{$v['key']} = $v;
            }

            $setting = json_decode(json_encode($setting));

            $view->with([
                'setting' => $setting
            ]);
        });

    }
}
