<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class HealthBenefit extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];


    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_health_benefit', 'benefit_id', 'product_id');
    }

    public function getSlugFromNameAttribute()
    {
        return preg_replace('/\s+/u', '-', trim(Str::lower($this->name)));
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::title($value);
        $this->attributes['slug'] = preg_replace('/\s+/u', '-', trim(Str::lower($value)));
    }

}
