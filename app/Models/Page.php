<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Page extends Model
{
    use HasFactory, SoftDeletes;

    const HOME_PAGE = 1;
    const DRINK_PAGE = 2;
    const FIND_US_PAGE = 3;
    const VIBE_PAGE = 4;
    const WELL_BEING_PAGE = 5;
    const NEWS_PAGE = 6;
    const CONTACT_PAGE = 7;
    const ABOUT_PAGE = 8;
    const FRANCHISING_PAGE = 9;
    const TERM_AND_CONDITION_PAGE = 10;
    const PRIVACY_PAGE = 11;
    const JOB_PAGE = 12;

    protected $guarded = ['id'];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::title($value);
        $this->attributes['slug'] = preg_replace('/\s+/u', '-', trim(Str::lower($value)));
    }

    public function faqs()
    {
        return $this->hasMany(PageFaq::class);
    }

}
