<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
        'images' => 'array',
    ];

    protected $attributes = [
        "images" => [
            "main_image" => "2022-02-04-61fcaff531620.png",
            "thumbnail_image" => "2022-02-04-61fcaff531c1d.png",
            "cup_shadow_image" => "2022-02-04-61fcaff532176.png",
            "ingredient_image" => "2022-02-04-61fcaff532754.png"
        ]
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::title($value);
        $this->attributes['slug'] = preg_replace('/\s+/u', '-', trim(Str::lower($value)));
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'product_ingredient');
    }

    public function benefits()
    {
        return $this->belongsToMany(HealthBenefit::class, 'product_health_benefit', 'product_id', 'health_benefit_id');
    }

    public function getFullImageURLAttribute()
    {
        $images = [];
        foreach ($this->images as $key => $image) {
            $images[$key] = asset('images/' . $image);
        }
        return $images;
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
