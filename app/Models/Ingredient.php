<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Ingredient extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];


    public function getRouteKeyName()
    {
        return 'slug';
    }



    public function products()
    {
        return $this->belongsToMany(Product::class, 'ingredient_id', 'product_id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::title($value);
        $this->attributes['slug'] = preg_replace('/\s+/u', '-', trim(Str::lower($value)));
    }
}
