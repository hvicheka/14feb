<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Setting extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $table = 'settings';

    public function setKeyAttribute($value)
    {
        $this->attributes['key'] = Str::lower($value);
    }
}
