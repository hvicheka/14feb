<!-- Sidebar -->
<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon">
            <img src="{{ asset('images/' . $logo->image) }} ">
        </div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('admin.dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCategory"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Categories</span>
        </a>
        <div id="collapseCategory" class="collapse" aria-labelledby="headingCategory"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('admin.categories.index') }}">All Categories</a>
                <a class="collapse-item" href="{{ route('admin.categories.create') }}">Add New Category</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBenefit"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Health Benefits</span>
        </a>
        <div id="collapseBenefit" class="collapse" aria-labelledby="headingBenefit"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('admin.benefits.index') }}">All Health Benefit</a>
                <a class="collapse-item" href="{{ route('admin.benefits.create') }}">Add New Health Benefit</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseIngredient"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Ingredients</span>
        </a>
        <div id="collapseIngredient" class="collapse" aria-labelledby="headingIngredient"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('admin.ingredients.index') }}">All Ingredients</a>
                <a class="collapse-item" href="{{ route('admin.ingredients.create') }}">Add New Ingredients</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDrink"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Products</span>
        </a>
        <div id="collapseDrink" class="collapse" aria-labelledby="headingDrink"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('admin.products.index') }}">All Products</a>
                <a class="collapse-item" href="{{ route('admin.products.create') }}">Add New Product</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePost"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Posts</span>
        </a>
        <div id="collapsePost" class="collapse" aria-labelledby="headingDrink"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('admin.posts.index') }}">All Posts</a>
                <a class="collapse-item" href="{{ route('admin.posts.create') }}">Add New Post</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseWellbeing"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Wellbeing</span>
        </a>
        <div id="collapseWellbeing" class="collapse" aria-labelledby="headingDrink"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('admin.wellbeing.index') }}">All Wellbeing</a>
                <a class="collapse-item" href="{{ route('admin.wellbeing.create') }}">Add New Wellbeing</a>
            </div>
        </div>
    </li>


    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseContact"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>About Us</span>
        </a>
        <div id="collapseContact" class="collapse" aria-labelledby="headingDrink"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('admin.about.index') }}">All About Us</a>
                <a class="collapse-item" href="{{ route('admin.about.create') }}">Add About Us</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePage"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Pages</span>
        </a>
        <div id="collapsePage" class="collapse" aria-labelledby="headingDrink"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @forelse($all_pages as $page)
                    <a class="collapse-item" href="{{ route('admin.pages.index',$page) }}">{{ $page->name }}</a>
                @empty
                    <a class="collapse-item" href="#"></a>
                @endforelse
                <a class="collapse-item" href="{{ route('admin.pages.vibe') }}">Vibe</a>

            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFaq"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Page FAQ</span>
        </a>
        <div id="collapseFaq" class="collapse" aria-labelledby="headingDrink"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @forelse($faq_pages as $page)
                    <a class="collapse-item" href={{ route('admin.pages.faq.index', $page) }}>{{ $page->name }}</a>
                @empty
                    <a class="collapse-item" href="#">No page</a>
                @endforelse
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Users</span>
        </a>
        <div id="collapseUsers" class="collapse" aria-labelledby="headingDrink"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href={{ route('admin.users.index') }}>All Users</a>
                <a class="collapse-item" href={{ route('admin.users.create') }}>New User</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSetting"
           aria-expanded="true" aria-controls="collapseBootstrap">
            <i class="far fa-fw fa-window-maximize"></i>
            <span>Settings</span>
        </a>
        <div id="collapseSetting" class="collapse" aria-labelledby="headingDrink"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href={{ route('admin.settings.index') }}>All Settings</a>
            </div>
        </div>
    </li>

</ul>
<!-- Sidebar -->
