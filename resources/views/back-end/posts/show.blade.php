@extends('back-end.layout.app')


@section('title,14 Feb','Show Post')

@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Show Post</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.posts.index') }}" class="btn btn-primary">All Posts</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h4 class="m-0 font-weight-bold text-primary">Title: {{ $post->title }}</h4>
                        </div>
                        <div class="card-body">
                            {!! $post->description !!}
                        </div>
                    </div>

                </div>
            </div>
            <!--Row-->
        </div>
    </div>
@endsection

@push('js')

@endpush
