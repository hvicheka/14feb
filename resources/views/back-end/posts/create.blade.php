@extends('back-end.layout.app')


@section('title,14 Feb','Add Post')

@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">New Post</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.posts.index') }}" class="btn btn-primary">All Posts</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">New Post</h6>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.posts.store') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                @include('back-end.posts.form')
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <!--Row-->
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('admin/js/image-preview.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>

@endpush
