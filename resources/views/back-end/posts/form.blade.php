<div class="form-group">
    <label for="validationServer03">Name</label>
    <input type="text" name="title"
           class="form-control  @error('title') is-invalid @enderror" value="{{ old('title', $post->title) }}">
    @error('title')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="validationServer03">Short Description</label>
    <textarea type="text" name="short_description"
              class="form-control ckeditor  @error('short_description') is-invalid @enderror"
              value="{{ old('short_description', $post->description) }}">{{ old('short_description', $post->short_description) }}</textarea>
    @error('short_description')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="validationServer03">Description</label>
    <textarea type="text" name="description"
              class="form-control ckeditor  @error('description') is-invalid @enderror"
              value="{{ old('description', $post->description) }}">{{ old('description', $post->description) }}</textarea>
    @error('description')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-row form-group">
    <div class="col">
        <div class="custom-file">
            <input type="file" name="image" class="custom-file-input @error('image') is-invalid @enderror images"
                   id="image"
                   data-preview="#image_preview">
            <label class="custom-file-label" for="customFile">Image</label>
            @error('image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <img class="img-thumbnail mt-3" src="{{ asset('images/' . $post->image) }}" id="image_preview"
             alt="Image Preview">
    </div>
    <div class="col">
        <div class="custom-file">
            <input type="file" class="custom-file-input @error('home_image') is-invalid @enderror images"
                   name="home_image"
                   id="home_image" data-preview="#home_image_preview">
            <label class="custom-file-label" for="home_image">Home Image (PNG)</label>
            @error('home_image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <img class="img-thumbnail mt-3" src="{{ asset('images/' . $post->home_image ) }}" id="home_image_preview"
             alt="Image Preview">
    </div>
</div>

<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" name="status"
               {{ $post->status ==1 ? 'checked' : '' }} class="custom-control-input @error('status') is-invalid @enderror"
               id="status">
        <label class="custom-control-label" for="status">Status</label>
        @error('status')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

</div>


