@extends('back-end.layout.app')

@section('title,14 Feb','Add Drink')

@push('css')
    <link href="{{ asset('admin/vendor/select2/dist/css/select2.min.css')}} " rel="stylesheet">
    <style>
        .is-invalid {
            border: 2px solid #dc3545 !important;
        }
    </style>
@endpush

@section('content')
    <div id="content">

        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Add Product</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.products.index') }}" class="btn btn-primary">All Products</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Add Product</h6>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.products.store') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="validationServer03">Product Name</label>
                                            <input type="text" name="name" value="{{ old('name') }}"
                                                   class="form-control  @error('name') is-invalid @enderror">
                                            @error('name')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group form-select2">
                                            <label for="category">Category</label>
                                            <select
                                                class="select2-multiple form-control select2-category   @error('categories') is-invalid @enderror"
                                                name="categories[]"
                                                multiple="multiple"
                                                id="category">
                                                @foreach($categories as $category)
                                                    <option
                                                        {{ in_array((string)$category->id,old('categories',[]) ) ? 'selected' : ''  }}
                                                        value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('categories')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <br>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="ingredient">Ingredient</label>
                                            <select
                                                class="select2-multiple form-control select2-ingredient  @error('ingredients') is-invalid @enderror"
                                                name="ingredients[]"
                                                multiple="multiple"
                                                id="ingredient">
                                                @foreach($ingredients as $ingredient)
                                                    <option
                                                        {{ in_array((string)$ingredient->id,old('ingredients',[]) ) ? 'selected' : ''  }}
                                                        value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('ingredients')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="benefit">Benefit</label>
                                            <select
                                                class="select2-multiple form-control select2-benefit  @error('benefits') is-invalid @enderror"
                                                name="benefits[]"
                                                multiple="multiple"
                                                id="benefit">
                                                @foreach($benefits as $benefit)
                                                    <option
                                                        {{ in_array((string)$benefit->id,old('benefits',[]) ) ? 'selected' : ''  }}
                                                        value="{{ $benefit->id }}">{{ $benefit->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('benefits')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="validationServer03">Product Description</label>
                                    <textarea class="form-control @error('description') is-invalid @enderror"
                                              name="description" id="description" cols="30"
                                              rows="5">{{ old('description') }}</textarea>
                                    @error('description')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input images" name="main_image"
                                                       id="main_image" data-preview="#main_image_image">
                                                <label class="custom-file-label" for="main_image">Main Image</label>
                                            </div>
                                            <img class="img-thumbnail mt-3"
                                                 src="{{ asset('img/placeholders/product/main_image.png') }}"
                                                 id="main_image_image"
                                                 alt="Image Preview">
                                        </div>
                                        <div class="col">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input images"
                                                       name="thumbnail_image"
                                                       id="thumbnail_image" data-preview="#thumbnail_image_preview">
                                                <label class="custom-file-label" for="thumbnail_image">Thumbnail</label>
                                            </div>
                                            <img class="img-thumbnail mt-3"
                                                 src="{{ asset('img/placeholders/product/thumbnail_image.png') }}"
                                                 id="thumbnail_image_preview"
                                                 alt="Image Preview">

                                        </div>
                                        <div class="col">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input images"
                                                       name="cup_shadow_image"
                                                       id="cup_shadow_image" data-preview="#cup_shadow_image_preview">
                                                <label class="custom-file-label" for="cup_shadow_image">Cup Shadow
                                                    Image</label>
                                            </div>
                                            <img class="img-thumbnail mt-3"
                                                 src="{{ asset('img/placeholders/product/cup_shadow_image.png') }}"
                                                 id="cup_shadow_image_preview"
                                                 alt="Image Preview">

                                        </div>
                                        <div class="col">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input images"
                                                       name="ingredient_image"
                                                       data-preview="#ingredient_image_image"
                                                       id="ingredient_image">
                                                <label class="custom-file-label" for="ingredient_image">Ingredient
                                                    Image</label>
                                            </div>
                                            <div class="invalid-feedback">
                                                ffffff
                                            </div>
                                            <img class="img-thumbnail mt-3"
                                                 src="{{ asset('img/placeholders/product/ingredient_image.png') }}"
                                                 id="ingredient_image_image"
                                                 alt="Image Preview">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="status" checked class="custom-control-input"
                                               id="status">
                                        <label class="custom-control-label" for="status">Status</label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <!--Row-->

        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('admin/vendor/select2/dist/js/select2.min.js') }}"></script>
    <script>
        // Select2
        $('.select2-category').select2({
            allowClear: true,
            placeholder: "Select Category",
        });
        $('.select2-ingredient').select2({
            allowClear: true,
            placeholder: "Select Ingredient",
        });
        $('.select2-benefit').select2({
            allowClear: true,
            placeholder: "Select health benefits",
        });
    </script>
    <script src="{{ asset('admin/js/image-preview.js') }}"></script>
@endpush

