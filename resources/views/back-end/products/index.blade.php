@extends('back-end.layout.app')


@section('title,14 Feb','All Products')

@push('css')

@endpush


@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">All Products</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.products.create') }}" class="btn btn-primary">New Product</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12 mb-4">
                    <!-- Simple Tables -->
                    <div class="card">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">All Drinks</h6>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Main Image</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($products as $product)
                                    <tr>
                                        <td class="align-middle">{{ ++$loop->index }}</td>
                                        <td><img src="{{ $product->full_image_url['main_image'] }}" alt="Image"
                                                 class="img-thumbnail" width="70"></td>
                                        <td class="align-middle">{{ $product->name }}</td>
                                        <td class="align-middle">
                                            @if($product->status ==1 )
                                                <span class="badge badge-success">active</span>
                                            @else
                                                <span class="badge badge-danger">inactive</span>
                                            @endif

                                        </td>
                                        <td class="align-middle">
                                            <a href="{{ route('admin.products.edit', $product->id) }}"
                                               class="btn btn-sm btn-primary">Edit</a>
                                            <a onclick="if(confirm('Are you sure?')){
                                                document.getElementById('delete-{{$product->id}}').submit()
                                                }" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                        <form method="POST" id="delete-{{ $product->id }}" class="hidden"
                                              action="{{ route('admin.products.destroy',$product) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <th colspan="20">No data found</th>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            {{ $products->links('back-end.layout.partials.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
            <!--Row-->

            <!-- Modal Logout -->
            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to logout?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                            <a href="login.html" class="btn btn-primary">Logout</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
