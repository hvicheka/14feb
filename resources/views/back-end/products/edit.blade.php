@extends('back-end.layout.app')

@section('title,14 Feb','Add Product')

@push('css')
    <link href="{{ asset('admin/vendor/select2/dist/css/select2.min.css')}} " rel="stylesheet">
@endpush

@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Edit Product</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.products.index') }}" class="btn btn-primary">All Products</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Edit Product</h6>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.products.update',$product->id) }}"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                @include('back-end.products.form')
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <!--Row-->

        </div>
    </div>
@endsection

@push('js')

    <script src="{{ asset('admin/js/image-preview.js') }}"></script>


    <script src="{{ asset('admin/vendor/select2/dist/js/select2.min.js') }}"></script>
    <script>
        // Select2
        $('.select2-category').select2({
            allowClear: true,
            placeholder: "Select Category",
        });
        $('.select2-ingredient').select2({
            allowClear: true,
            placeholder: "Select Ingredient",
        });
        $('.select2-benefit').select2({
            allowClear: true,
            placeholder: "Select health benefits",
        });
    </script>
@endpush

