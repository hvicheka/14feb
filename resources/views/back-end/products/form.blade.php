<div class="form-row">
    <div class="col">
        <div class="form-group">
            <label for="validationServer03">Product Name</label>
            <input type="text" name="name" value="{{ $product->name }}"
                   class="form-control  @error('name') is-invalid @enderror">
            @error('name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="category">Category</label>
            <select class="select2-multiple form-control select2-category"
                    name="categories[]"
                    multiple="multiple"
                    id="category">
                @foreach($categories as $category)
                    <option
                        {{ in_array($category->id,$product->categories->pluck('id')->toArray() ) ? 'selected=select' : ''  }}
                        value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col">
        <div class="form-group">
            <label for="ingredient">Ingredient</label>
            <select class="select2-multiple form-control select2-ingredient"
                    name="ingredients[]"
                    multiple="multiple"
                    id="ingredient">
                @foreach($ingredients as $ingredient)
                    <option
                        {{ in_array($ingredient->id,$product->ingredients->pluck('id')->toArray() ) ? 'selected=select' : ''  }}
                        value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="benefit">Benefit</label>
            <select class="select2-multiple form-control select2-benefit"
                    name="benefits[]"
                    multiple="multiple"
                    id="benefit">
                @foreach($benefits as $benefit)
                    <option
                        {{ in_array($benefit->id,$product->benefits->pluck('id')->toArray() ) ? 'selected=select' : ''  }}
                        value="{{ $benefit->id }}">{{ $benefit->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="validationServer03">Product Description</label>
    <textarea class="form-control @error('description') is-invalid @enderror"
              name="description" id="description" cols="30"
              rows="5">{{ $product->description }}</textarea>
    @error('description')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
<div class="form-row form-group">
    <div class="col">
        <div class="custom-file">
            <input type="file" class="custom-file-input images" name="main_image"
                   id="main_image" data-preview="#main_image_image">
            <label class="custom-file-label" for="main_image">Main Image</label>
        </div>
        <img class="img-thumbnail mt-3" src="{{ $product->full_image_url['main_image'] }}" id="main_image_image"
             alt="Image Preview">
    </div>
    <div class="col">
        <div class="custom-file">
            <input type="file" class="custom-file-input images" name="thumbnail_image"
                   id="thumbnail_image" data-preview="#thumbnail_image_preview">
            <label class="custom-file-label" for="thumbnail_image">Thumbnail</label>
        </div>
        <img class="img-thumbnail mt-3" src="{{ $product->full_image_url['thumbnail_image'] }}"
             id="thumbnail_image_preview" alt="Image Preview">

    </div>
    <div class="col">
        <div class="custom-file">
            <input type="file" class="custom-file-input images" name="cup_shadow_image"
                   id="cup_shadow_image" data-preview="#cup_shadow_image_preview">
            <label class="custom-file-label" for="cup_shadow_image">Cup Shadow
                Image</label>
        </div>
        <img class="img-thumbnail mt-3" src="{{ $product->full_image_url['cup_shadow_image'] }}"
             id="cup_shadow_image_preview" alt="Image Preview">

    </div>
    <div class="col">
        <div class="custom-file">
            <input type="file" class="custom-file-input images" name="ingredient_image"
                   data-preview="#ingredient_image_image"
                   id="ingredient_image">
            <label class="custom-file-label" for="ingredient_image">Ingredient
                Image</label>
        </div>
        <img class="img-thumbnail mt-3" src="{{ $product->full_image_url['ingredient_image'] }}"
             id="ingredient_image_image" alt="Image Preview">
    </div>
</div>
<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" name="status" {{ $product->status ==1 ? 'checked' : '' }} class="custom-control-input"
               id="status">
        <label class="custom-control-label" for="status">Status</label>
    </div>
</div>
