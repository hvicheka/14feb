<div class="form-group">
    <label for="validationServer03">Name</label>
    <input type="text" name="name"
           class="form-control  @error('name') is-invalid @enderror" value="{{ old('name', $benefit->name) }}">
    @error('name')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <div class="custom-file">
        <input type="file" name="image" class="custom-file-input @error('image') is-invalid @enderror" id="image">
        <label class="custom-file-label" for="customFile">Image</label>
        @error('image')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror

    </div>

</div>

<div class="form-group">
    <img class="img-fluid bg-gradient-success mt-3 mb-3" width="80" src="{{ asset('images/' . $benefit->image) }}" alt="Image Preview"
         id="preview-image">
</div>

