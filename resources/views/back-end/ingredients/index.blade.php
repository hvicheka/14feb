@extends('back-end.layout.app')


@section('title,14 Feb','All Drinks')


@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">All Ingredients</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.ingredients.create') }}" class="btn btn-primary">New Ingredient</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12 mb-4">
                    <!-- Simple Tables -->
                    <div class="card">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">All Ingredients</h6>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
{{--                                    <th>Image</th>--}}
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($ingredients as $ingredient)
                                    <tr>
                                        <td class="align-middle">{{ ++$loop->index }}</td>
{{--                                        <td>--}}
{{--                                            <img class="img-thumbnail bg-gradient-success" width="60"--}}
{{--                                                 src="{{ asset('images/' . $ingredient->image) }}"--}}
{{--                                                 alt="Image">--}}
{{--                                        </td>--}}
                                        <td class="align-middle">{{ $ingredient->name}}</td>
                                        <td class="align-middle">
                                            <a href="{{ route('admin.ingredients.edit', $ingredient) }}"
                                               class="btn btn-sm btn-primary">Edit</a>
                                            <a onclick="if(confirm('Are you sure?')){
                                                document.getElementById('delete-{{$ingredient->id}}').submit()
                                                }" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                        <form method="POST" id="delete-{{ $ingredient->id }}" class="hidden"
                                              action="{{ route('admin.ingredients.destroy',$ingredient) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <th colspan="20">No data found</th>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            {{ $ingredients->links('back-end.layout.partials.pagination') }}
                        </div>

                    </div>
                </div>
            </div>
            <!--Row-->

        </div>
    </div>
@endsection
