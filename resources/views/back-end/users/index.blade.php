@extends('back-end.layout.app')


@section('title','All Users')


@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">All Users</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.users.create') }}" class="btn btn-primary">New User</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12 mb-4">
                    <!-- Simple Tables -->
                    <div class="card">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">All Users</h6>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $user)
                                    <tr>
                                        <td class="align-middle">{{ ++$loop->index }}</td>
                                        <td>
                                            <img class="img-thumbnail" width="80"
                                                 src="{{ asset('images/' . $user->image) }}"
                                                 alt="Image">
                                        </td>
                                        <td class="align-middle">{{ $user->name }}</td>
                                        <td class="align-middle">{{ $user->email }} </td>
                                        <td class="align-middle">
                                            <a href="{{ route('admin.users.edit', $user) }}"
                                               class="btn btn-sm btn-primary">Edit</a>
                                            <a onclick="if(confirm('Are you sure?')){
                                                document.getElementById('delete-{{$user->id}}').submit()
                                                }" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                        <form method="POST" id="delete-{{ $user->id }}" class="hidden"
                                              action="{{ route('admin.users.destroy',$user) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <th colspan="20">No data found</th>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            {{ $users->links('back-end.layout.partials.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
            <!--Row-->

        </div>
    </div>
@endsection
