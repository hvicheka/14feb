<div class="form-row form-group">
    <div class="col form-group">
        <div class="form-group">
            <label for="validationServer03">Name</label>
            <input type="text" name="name"
                   class="form-control @error('name') is-invalid @enderror "
                   value="{{ old('name', $user->name) }}">
            @error('name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="validationServer03">Email</label>
            <input type="text" name="email"
                   class="form-control @error('email') is-invalid @enderror"
                   value="{{ old('email', $user->email) }}">
            @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="validationServer03">Password</label>
            <input type="password" name="password"
                   class="form-control @error('password') is-invalid @enderror"
                   value="{{ old('password') }}">
            @error('password')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>

    </div>
    <div class="col">
        <label for="validationServer03">Image</label>
        <div class="custom-file">
            <input type="file" name="image"
                   class="custom-file-input images"
                   id="image"
                   data-preview="#image_preview">
            <label class="custom-file-label" for="customFile">Choose File</label>
            @error('image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror

        </div>
        <a target="_blank" href="{{ asset('images/' . $user->image ?? 'def.jpg') }}">
            <img width="120" class="img-thumbnail mt-3" src="{{ asset('images/' . $user->image) }}"
                 id="image_preview"
                 alt="Image Preview">
        </a>
    </div>

</div>
