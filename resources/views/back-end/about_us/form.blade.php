<div class="form-group">
    <label for="validationServer03">Name</label>
    <input type="text" name="title"
           class="form-control  @error('title') is-invalid @enderror" value="{{ old('title', $about->title) }}">
    @error('title')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="validationServer03">Description</label>
    <textarea type="text" name="description" cols="30" rows="10"
              class="form-control ckeditor  @error('description') is-invalid @enderror"
              value="{{ old('description', $about->description) }}">{{ old('description', $about->description) }}</textarea>

    @error('description')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <div class="custom-file">
        <input type="file" name="image" class="custom-file-input @error('image') is-invalid @enderror" id="image">
        <label class="custom-file-label" for="customFile">Image</label>
        @error('image')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror

    </div>

</div>

<div class="form-group">
    <img class="img-fluid mt-3 mb-3" width="200" src="{{ asset('images/' . $about->image) }}" alt="Image Preview"
         id="preview-image">
</div>

