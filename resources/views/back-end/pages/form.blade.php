<div class="form-group">
    <label for="validationServer03">Title</label>
    <input type="text" name="name"
           class="form-control  @error('name') is-invalid @enderror"
           value="{{ old('name', $page->name) }}">
    @error('name')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="validationServer03">Short Description</label>
    <textarea type="text" name="description"
              class="form-control ckeditor  @error('description') is-invalid @enderror"
              value="{{ old('description', $page->description) }}">{{ old('description', $page->description) }}</textarea>
    @error('description')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-row form-group">
    <div class="col">
        <div class="custom-file">
            <input type="file" name="image"
                   class="custom-file-input @error('image') is-invalid @enderror images"
                   id="image"
                   data-preview="#image_preview">
            <label class="custom-file-label" for="customFile">Image</label>
            @error('image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <a target="_blank" href="{{ asset('images/' . $page->image ?? 'def.jpg') }}">
            <img class="img-thumbnail mt-3" src="{{ asset('images/' . $page->image) }}"
                 id="image_preview"
                 alt="Image Preview">
        </a>
    </div>
    <div class="col">
        <div class="custom-file">
            <input type="file" name="title_image"
                   class="custom-file-input @error('image') is-invalid @enderror images"
                   id="title_image"
                   data-preview="#title_image_preview">
            <label class="custom-file-label" for="customFile">Title Image</label>
            @error('title_image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <a target="_blank" href="{{ asset('images/' . $page->title_image ?? 'def.jpg') }}">
            <img class="img-thumbnail mt-3" src="{{ asset('images/' . $page->title_image) }}"
                 id="title_image_preview"
                 alt="Image Preview">
        </a>
    </div>
</div>

<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" name="enable_faq"
               {{ $page->enable_faq == 1 ? 'checked' : '' }} class="custom-control-input @error('enable_faq') is-invalid @enderror"
               id="status">
        <label class="custom-control-label" for="status">Enable FAQ</label>
        @error('enable_faq')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

</div>
