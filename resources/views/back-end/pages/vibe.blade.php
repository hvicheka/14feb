@extends('back-end.layout.app')


@section('title','Add Wellbeing')

@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Vibe Page</h1>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Vibe Page</h6>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.pages.vibe.update') }}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                @include('back-end.pages.form')

                                <div class="form-group">
                                    <label for="validationServer03">Term and Condition</label>
                                    <textarea type="text" name="terms"
                                              class="form-control ckeditor  @error('terms') is-invalid @enderror"
                                              value="{{ old('terms', $page->terms) }}">{{ old('terms', $page->terms) }}</textarea>
                                    @error('terms')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <!--Row-->
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('admin/js/image-preview.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>

@endpush
