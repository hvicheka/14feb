<div class="form-group">
    <label for="key">Key</label>
    <input type="text" id="key" name="key" class="form-control  @error('key') is-invalid @enderror"
           value="{{ old('key', $setting->key) }}">
    @error('key')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="value">Value</label>
    <input type="text" id="value" name="value"
           class="form-control  @error('value') is-invalid @enderror" value="{{ old('value', $setting->value) }}">
    @error('value')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="url">URL</label>
    <input type="url" id="link" name="link"
           class="form-control  @error('link') is-invalid @enderror" value="{{ old('url', $setting->link) }}">
    @error('link')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>


<div class="form-row form-group">
    <div class="col">
        <div class="custom-file">
            <input type="file" name="image" class="custom-file-input @error('image') is-invalid @enderror images"
                   id="image"
                   data-preview="#image_preview">
            <label class="custom-file-label" for="customFile">Image</label>
            @error('image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <img class="img-thumbnail mt-3" width="200" src="{{ asset('images/' . $setting->image) }}" id="image_preview"
             alt="Image Preview">
    </div>
</div>


