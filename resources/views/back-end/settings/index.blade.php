@extends('back-end.layout.app')


@section('title','All Setting')


@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">All Setting</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.settings.create') }}" class="btn btn-primary">New Setting</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12 mb-4">
                    <!-- Simple Tables -->
                    <div class="card">
                        <div
                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between d-inline-flex">
                            <div class="d-flex">
                                <form action="{{ route('admin.settings.index') }}" class="d-flex">
                                    <input type="text" class="form-control" name="q" placeholder="Search!...."
                                           value="{{ old('q', request()->q) }}">
                                </form>
                                @if(request()->q)
                                    <a class="btn btn-primary ml-2 " href="{{ route('admin.settings.index') }}"><span
                                            class="mt-5">Clear</span></a>
                                @endif
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>key</th>
                                    <th>value</th>
                                    <th>link</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($settings as $setting)
                                    <tr>
                                        <td class="align-middle">{{ ++$loop->index }}</td>
                                        <td class="align-middle">{{ $setting->key }}</td>
                                        <td class="align-middle">{{ $setting->value }}</td>
                                        <td class="align-middle">
                                            <a target="_blank" href="{{ $setting->link }}">{{ $setting->link }}</a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="{{ asset('images/' . $setting->image) }}"><img
                                                    src="{{ asset('images/' . $setting->image) }}" class="img-thumbnail"
                                                    width="60" alt=""></a>
                                        </td>
                                        <td class="align-middle">
                                            <a href="{{ route('admin.settings.edit', $setting) }}"
                                               class="btn btn-sm btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <th colspan="20">No data found</th>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            {{ $settings->links('back-end.layout.partials.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
            <!--Row-->

        </div>
    </div>
@endsection
