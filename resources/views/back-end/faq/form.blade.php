<div class="form-group">
    <label for="validationServer03">Question</label>
    <input type="text" name="question"
           class="form-control  @error('question') is-invalid @enderror"
           value="{{ old('question', $faq->question) }}">
    @error('question')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="validationServer03">Answer</label>
    <textarea type="text" name="answer"
              class="form-control ckeditor  @error('answer') is-invalid @enderror"
              value="{{ old('answer', $faq->answer) }}">{{ old('answer', $faq->answer) }}</textarea>
    @error('answer')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
