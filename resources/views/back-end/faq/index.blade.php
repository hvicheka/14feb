@extends('back-end.layout.app')


@section('title','Add Wellbeing')

@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">FAQ Page</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.pages.faq.create', $page) }}" class="btn btn-primary">New FAQ</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Page: {{ $page->name }}</h6>
                        </div>
                        <div class="card-body">
                            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Question</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($faqs as $faq)
                                        <tr>
                                            <td class="align-middle">{{ ++$loop->index }}</td>
                                            <td class="align-middle">{{ $faq->question }}</td>

                                            <td class="align-middle">
                                                <a href="{{ route('admin.pages.faq.edit',[$page,$faq]) }}"
                                                   class="btn btn-sm btn-primary">Edit</a>
                                                <a onclick="if(confirm('Are you sure?')){
                                                    document.getElementById('delete-{{$faq->id}}').submit()
                                                    }" class="btn btn-sm btn-danger">Delete</a>
                                            </td>
                                            <form method="POST" id="delete-{{ $faq->id }}" class="hidden"
                                                  action="{{ route('admin.pages.faq.destroy',[$page,$faq]) }}">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <th colspan="20">No data found</th>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                                {{ $faqs->links('back-end.layout.partials.pagination') }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--Row-->
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('admin/js/image-preview.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>

@endpush
