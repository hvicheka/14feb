<div class="form-group">
    <label for="validationServer03">Old Password</label>
    <input type="password" name="current_password"
           class="form-control" required>
</div>

<div class="form-group">
    <label for="validationServer03">Password</label>
    <input type="password" name="new_password"
           class="form-control" required>
</div>

<div class="form-group">
    <label for="validationServer03">Confirm Password</label>
    <input type="password" name="new_confirm_password"
           class="form-control" required>
</div>
