
<div class="form-row form-group">
    <div class="col form-group">
        <div class="form-group">
            <label for="validationServer03">Name</label>
            <input type="text" name="name"
                   class="form-control "
                   value="{{ old('name', auth()->user()->name) }}">
        </div>

        <div class="form-group">
            <label for="validationServer03">Email</label>
            <input type="text" name="email"
                   class="form-control"
                   value="{{ old('email', auth()->user()->email) }}">
        </div>

    </div>
    <div class="col">
        <label for="validationServer03">Image</label>
        <div class="custom-file">
            <input type="file" name="image"
                   class="custom-file-input images"
                   id="image"
                   data-preview="#image_preview">
            <label class="custom-file-label" for="customFile">Choose File</label>

        </div>
        <a target="_blank" href="{{ asset('images/' . auth()->user()->image ?? 'def.jpg') }}">
            <img width="100" class="img-thumbnail mt-3" src="{{ asset('images/' . auth()->user()->image) }}"
                 id="image_preview"
                 alt="Image Preview">
        </a>
    </div>

</div>
