@extends('back-end.layout.app')


@section('title','Profile')

@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Update Profile</h1>
            </div>

            <div class="row">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li class="text-danger">{{ $error }}</li>
                    @endforeach
                </ul>

                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            Update Profile
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.profile.update') }}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                @include('back-end.profile.form')
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>

                </div>


                <div class="col-lg-12">
                    <!-- Form Basic -->
                    <div class="card mb-4">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            Change Password
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.profile.change_password') }}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                @include('back-end.profile.change_password_form')
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <!--Row-->
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('admin/js/image-preview.js') }}"></script>
@endpush
