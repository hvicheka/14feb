<div class="form-group">
    <label for="validationServer03">Name</label>
    <input type="text" name="title"
           class="form-control  @error('title') is-invalid @enderror" value="{{ old('title', $wellbeing->title) }}">
    @error('title')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="validationServer03">Short Description</label>
    <textarea type="text" name="short_description"
              class="form-control ckeditor  @error('short_description') is-invalid @enderror"
              value="{{ old('short_description', $wellbeing->description) }}">{{ old('short_description', $wellbeing->short_description) }}</textarea>
    @error('short_description')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-group">
    <label for="validationServer03">Description</label>
    <textarea type="text" name="description"
              class="form-control ckeditor  @error('description') is-invalid @enderror"
              value="{{ old('description', $wellbeing->description) }}">{{ old('description', $wellbeing->description) }}</textarea>
    @error('description')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>

<div class="form-row form-group">
    <div class="col">
        <div class="custom-file">
            <input type="file" name="image" class="custom-file-input @error('image') is-invalid @enderror images"
                   id="image"
                   data-preview="#image_preview">
            <label class="custom-file-label" for="customFile">Image</label>
            @error('image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <img class="img-thumbnail mt-3" src="{{ asset('images/' . $wellbeing->image) }}" id="image_preview"
             alt="Image Preview">
    </div>
</div>

<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" name="status"
               {{ $wellbeing->status == 1 ? 'checked' : '' }} class="custom-control-input @error('status') is-invalid @enderror"
               id="status">
        <label class="custom-control-label" for="status">Status</label>
        @error('status')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

</div>


