@extends('back-end.layout.app')


@section('title,14 Feb','All Posts')


@section('content')
    <div id="content">
        <div class="container-fluid" id="container-wrapper">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">All Wellbeing</h1>
                <ol class="breadcrumb">
                    <a href="{{ route('admin.wellbeing.create') }}" class="btn btn-primary">New Wellbeing</a>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12 mb-4">
                    <!-- Simple Tables -->
                    <div class="card">
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">All Wellbeing</h6>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($wellbeings as $wellbeing)
                                    <tr>
                                        <td class="align-middle">{{ ++$loop->index }}</td>
                                        <td>
                                            <img class="img-thumbnail" width="80"
                                                 src="{{ asset('images/' . $wellbeing->image) }}"
                                                 alt="Image">
                                        </td>
                                        <td class="align-middle">{{ $wellbeing->title }}</td>
                                        <td class="align-middle">
                                            @if($wellbeing->status ==1 )
                                                <span class="badge badge-success">active</span>
                                            @else
                                                <span class="badge badge-danger">inactive</span>
                                            @endif

                                        </td>
                                        <td class="align-middle">
                                            <a href="{{ route('admin.wellbeing.show', $wellbeing) }}"
                                               class="btn btn-sm btn-success">Show</a>
                                            <a href="{{ route('admin.wellbeing.edit', $wellbeing) }}"
                                               class="btn btn-sm btn-primary">Edit</a>
                                            <a onclick="if(confirm('Are you sure?')){
                                                document.getElementById('delete-{{$wellbeing->id}}').submit()
                                                }" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                        <form method="POST" id="delete-{{ $wellbeing->id }}" class="hidden"
                                              action="{{ route('admin.wellbeing.destroy',$wellbeing) }}">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <th colspan="20">No data found</th>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            {{ $wellbeings->links('back-end.layout.partials.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
            <!--Row-->

        </div>
    </div>
@endsection
