<!doctype html>
<html lang="en-AU">
<head>
    <title>Boost Juice</title>
    @include('front_end.layouts.partials.style')
</head>
<body class="boost-home">
@include('front_end.layouts.partials.header')
<div class="boost-main-content">
    <div class="main-fold welcome">
        <div class="container">
            <div class="row">
                <div class="six columns"><img src="img/placeholders/titles-love-life.png"/> <span
                        class="hide-mobile"> <a href="drinks.html" class="button">Juices & Smoothies</a> <a
                            href="index.html#boost-store-locator" class="button">Find Us</a> </span> <span
                        class="hide-desktop"> <a href="drinks.html" class="button">View Our Drinks</a> <a
                            href="index.html#boost-store-locator" class="button">Find Us</a> </span></div>
            </div>
        </div>
    </div>
    <div class="sub-container">
        <div class="block block-green promo half-angle-container grey">
            <div class="container">
                <div class="row">
                    <div class="six columns"><h2>Nothing But Passion!</h2>
                        <p>Our newest campaign ‘Nothing but Passion’ is here and ready to take you on a juicy date. One
                            sip is all it takes to fall in love with our three new drinks.</p>
                        <p><a href="news/new-nothing-but-passion.html" class="button">Learn More</a></p></div>
                    <div class="six columns half-angle"><img
                            src="wp-content/uploads/2020/11/RZBN-01221-Nothing-But-Passion_Homepage_550x450.png"/></div>
                    <style>.block.half-angle-container.grey .half-angle::after {
                            background-color: #f4c26f;
                        }</style>
                </div>
            </div>
        </div>
        <div class="block vibe-bkg">
            <div class="container">
                <div class="row">
                    <div class="columns text-center"><img src="img/placeholders/vibe-logo.png"/>
                        <p>This is it. This is the coolest, most exclusive club you’ll ever be a part of. It’s more
                            glamourous than Mariah Carey’s life, it’s got more hidden benefits than the Illuminati, and
                            if you download our Boost app then you’ll be swimming in free stuff and sweet deals for the
                            rest of your life… or until you lose your card or delete the app.</p>
                        <p><a href="https://www.boostjuice.com.au/vibe" class="button">Teach me more!</a></p></div>
                </div>
            </div>
        </div>
        <div id="boost-store-locator" class="hide-stores">
            <div class="preloader" id="boost-stores-loading">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
            <div class="boostjuice-search"><img src="img/placeholders/icon_boost_cup.png"/>
                <h2>Find a Boost Juice Store</h2>
                <p>How close am I to putting a Boost in my belly? Just search your address using the little search bar
                    thingy below. Easy.</p><a id="get-my-location" class="button">Use current location</a> <span
                    class="choice">OR</span> <span id="autocomplete-box"><input autocomplete="off"
                                                                                id="boostjuice-search-input" type="text"
                                                                                value="" name="boostjuice-search-input"
                                                                                placeholder="Postcode or suburb/town"/> <input
                        id="boostjuice-search-btn" type="submit" value=""></span></div>
            <div id="boost-stores">
                <div id="boostjuice-result-list">
                    <div id="boostjuice-stores"></div>
                </div>
                <div class="boostjuice-gmap-wrap">
                    <div id="boostjuice-gmap"></div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer" role="contentinfo"><a href="https://www.facebook.com/boostjuice" target="_blank"><img
                src="https://www.boostjuice.com.au/img/placeholders/icon_facebook.png"
                alt="Boost Juice - Facebook"/></a> <a href="https://twitter.com/boostjuiceoz" target="_blank"><img
                src="https://www.boostjuice.com.au/img/placeholders/icon_twitter.png" alt="Boost Juice - Twitter"/></a>
        <a href="https://www.instagram.com/boost_juice" target="_blank"><img
                src="https://www.boostjuice.com.au/img/placeholders/icon_instagram.png" alt="Boost Juice - Instagram"/></a>
        <nav class="footer-nav">
            <ul>
                <li><a href="about-boost-juice/index.html">About</a></li>
                <li><a href="franchising/index.html">Franchising</a></li>
                <li><a href="work-at-boost/index.html">Careers</a></li>
                <li><a href="https://www.boostjuice.com.au/vibe">VIBE</a></li>
                <li><a href="https://www.boostjuice.com.au/onlineshop/">Shop</a></li>
            </ul>
        </nav>
        <p class="copyright"> &copy; 2022 Copyright Boost Juice <span class="hide-mobile">|</span> <a
                href="https://www.boostjuice.com.au/terms-and-conditions">Terms & Conditions</a> <span
                class="hide-mobile">|</span> <a href="https://www.boostjuice.com.au/privacy-policy">Privacy</a> <span
                class="hide-mobile">|</span> <a href="https://www.boostjuice.com.au/secret-bits">Secret Bits</a></p>
    </footer>
</div>
<script type='text/javascript'
        src='https://maps.googleapis.com/maps/api/js?libraries=places&amp;v=3.40&amp;key=AIzaSyCBkGjTDClCnRg5YiMHYADLvZ2-8L1jSUI&amp;ver=5.6.1'
        id='wpsl-gmap-js'></script>
<script type='text/javascript' src='{{ asset('dist/js/jquery.geocomplete.min.js') }}'
        id='wpsl-gmap-autocomplete-js'></script>
<script type='text/javascript' src='{{ asset('dist/js/boostjuice-gmap.js') }}' id='wpsl-js-js'></script>
<script type="text/javascript" src="{{ asset('js/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-28404652-1', 'auto');
    ga('send', 'pageview');</script>
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '545650925586999');
    fbq('track', 'PageView');</script>
<noscript>
    <img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=545650925586999&ev=PageView&noscript=1"/>
</noscript>
<script>
    function onSubmitVibePasswordForm(token) {
        document.getElementById("vibe-password").submit();
    }
</script>
</body>
</html>
