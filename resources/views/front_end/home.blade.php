@extends('front_end.layouts.app')

@push('css')

    <style>
        .vibe-bkg {
            background-image: url("{{ asset('images/' . $vibe->image) }}");
            overflow: hidden;
            background-size: cover;
        }

        .vibe-bkg::before {
            background-image: url("{{ asset('images/' . $vibe->image) }}");
            background-size: cover;
        }

        @media (max-width: 1440px) {
            .vibe-bkg {
                background-image: url("{{ asset('images/' . $vibe->image) }}");
                min-height: 46vw;
                background-size: cover;
            }
        }

        .boostjuice-search img {
            transform: rotate(0deg);
        }

        .friends_side {
            background-image: url('{{ asset('images/' . $home_page->image) }}');
            background-size: cover;
        }

        .friends_backs {
            background-image: url('{{ asset('images/' . $home_page->image) }}');
            background-size: 100%;
        }
    </style>
@endpush

@section('title', $home_page->name)

@section('content')
    <div class="main-fold welcome">
        <div class="container">
            <div class="row">
                <div class="six columns">
                    <img src="{{ asset('images/' . $home_page->title_image) }}"/>
                    <span
                        class="hide-mobile">
                        <a href="{{ route('front_end.drinks') }}" class="button">Juices & Smoothies</a>
                        <a href="#boost-store-locator" class="button">Find Us</a>
                    </span>
                    <span class="hide-desktop">
                        <a href="{{ route('front_end.drinks') }}" class="button">View Our Drinks</a>
                        <a href="#boost-store-locator" class="button">Find Us</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-container">
        <div class="block block-green promo half-angle-container grey">
            <div class="container">
                <div class="row">
                    <div class="six columns">
                        <h2>{{ $post->title }}</h2>
                        {!! $post->short_description !!}
                        <p>
                            <a href="{{ route('front_end.news.detail', $post->slug) }}" class="button">Learn More</a>
                        </p>
                    </div>
                    <div class="six columns half-angle">
                        <img
                            src="{{ asset('images/' .  $post->home_image)  }}"/>
                    </div>
                    <style>.block.half-angle-container.grey .half-angle::after {
                            background-color: #f4c26f;
                        }</style>
                </div>
            </div>
        </div>
        <div class="block vibe-bkg">
            <div class="container">
                <div class="row">
                    <div class="columns text-center">
                        <img src="{{ asset('images/' . $vibe->title_image) }}"/>
                        <p>
                            {!! $vibe->description !!}
                        </p>
                        <p><a href="{{ route('front_end.vibe') }}" class="button">Teach me more!</a></p></div>
                </div>
            </div>
        </div>
        <div id="boost-store-locator" class="hide-stores">
            <div class="preloader" id="boost-stores-loading">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
            <div class="boostjuice-search"><img src="{{ asset('images/' . $find_us->title_image) }}"/>
                <h2>{{ $find_us->name }}</h2>
                <p>{!! $find_us->description !!}</p>
                {{--                <a id="get-my-location" class="button">Use current location</a>--}}
                {{--                <span--}}
                {{--                    class="choice">OR</span>--}}
                {{--                <span id="autocomplete-box">--}}
                {{--                    <input autocomplete="off" id="boostjuice-search-input" type="text" value="" name="boostjuice-search-input" placeholder="Postcode or suburb/town"/>--}}
                {{--                    <input id="boostjuice-search-btn" type="submit" value=""></span>--}}
            </div>
            <div id="boost-stores">
                <div id="boostjuice-result-list">
                    <div id="boostjuice-stores"></div>
                </div>
                <div class="boostjuice-gmap-wrap">
                    <div id="boostjuice-gmap"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush

