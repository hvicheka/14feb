@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .vibe-banner-bkg {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .vibe-banner-bkg {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }
    </style>
@endpush

@section('content')
    {{--    <div class="preloader">--}}
    {{--        <div class="spinner">--}}
    {{--            <div class="double-bounce1"></div>--}}
    {{--            <div class="double-bounce2"></div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    <div class="main-fold vibe-banner-bkg"></div>
    <div class="sub-fold fix-content-large" id="vibe-welcome">
{{--        <div class="get-app">--}}
{{--            <a href="https://apps.apple.com/au/app/boost-juice/id874205871">--}}
{{--                <img--}}
{{--                    src="{{ asset('img/placeholders/button_app_store.png') }}"/></a> --}}
{{--            <a--}}
{{--                href="https://play.google.com/store/apps/details?id=com.redcat.BoostApp">--}}
{{--                <img--}}
{{--                    src="{{ asset('img/placeholders/button_google_play.png') }}"/></a>--}}
{{--        </div>--}}
        <div class="container">
            <div class="row fix-content">
                <div class="columns text-center">
                    <img src="{{ asset('images/'. $page->title_image) }}" alt=""/>
                    {!! $page->description !!}
                    {{--                    <div class="row">--}}
                    {{--                        <div class="columns six"><a class="button vibe-login-trigger hide-mobile">Sign in</a> <a--}}
                    {{--                                class="button vibe-login-trigger-mobile hide-desktop">Sign in</a></div>--}}
                    {{--                        <div class="columns six"><a href="#vibe-sign-up"--}}
                    {{--                                                    class="button button-primary">Register</a></div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    <div id="my-vibe">
        <div class="sides-off" id="my-vibe-inner">
            <div class="text-center vibe-join-now">
                <div class="fix-content text-center"><h3>How do I join?</h3>
                    <p>So you want in hey? Well, it ain’t that easy. You can’t just magically download the app and
                        register your details and expect to be granted entry into our super exclusive Vibe Club. Wait.
                        Actually, that’s exactly all you need to do. So go and do it so you can enjoy free Boost stuff
                        and juicy deals!</p></div>
            </div>
            <div class="vibe-options">
                <div class="container">
                    <div class="row">
                        <div class="six columns">
                            <div><p class="sub-title">Option 1</p>
                                <h3>Download the<br> Boost Juice App</h3>
                                <p>Your phone just got smarter! From the convenience of your fingers (or toes if you’re
                                    talented like that) you can order and customise your Boost from the App and strut
                                    your stuff past any queues. If that’s not enough, we give so much stuff away you’d
                                    think we we’re having a closing down sale! Which we aren’t. We’re here forever.</p>
                                <p><a href="https://apps.apple.com/au/app/boost-juice/id874205871"><img
                                            src="img/placeholders/button_app_store.png" width="160"></a> <a
                                        href="https://play.google.com/store/apps/details?id=com.redcat.BoostApp"><img
                                            src="img/placeholders/button_google_play.png" width="160"></a></p></div>
                        </div>
                        <div class="six columns">
                            <div><p class="sub-title">Option 2</p>
                                <h3>Get yourself a Vibe Card</h3>
                                <p>Oh you little hipster thing you!<br>If tech is too cool at the moment, then scoot on
                                    over to your nearest Boost store, get yourself a Vibe card and register your
                                    details. Do it quickly before it becomes mainstream.</p> <a
                                    href="#vibe-sign-up" class="button">Register here</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="intro"><h3 class="text-center">Testimonials</h3>
                            <p><em id="vibe-testimonail"></em></p></div>
                    </div>
                    <div class="row vibe-registration" id="vibe-sign-up"><h3 class="text-center" id="i-want-in">I want
                            in! Sign me up!</h3>
                        <form class="boostform" id="vibe-register-form" autocomplete="off">
                            <ul id="progressbar">
                                <li class="active">Your contact details</li>
                                <li>Your membership</li>
                            </ul>
                            <fieldset class="form-step-1"><input type="text" id="reg-user-first-name"
                                                                 name="reg_user_first_name" placeholder="First name*">
                                <input type="text" id="reg-user-last-name" name="reg_user_last_name"
                                       placeholder="Last name*"> <label class="text-center">Date of birth - For a FREE
                                    birthday Boost*</label> <input type="number" id="reg-dob-date" name="reg_dob_date"
                                                                   placeholder="DD" maxlength="2"
                                                                   style="max-width: 80px; min-width: auto;"> <input
                                    type="number" id="reg-dob-month" name="reg_dob_month" placeholder="MM"
                                    maxlength="2" style="max-width: 80px; min-width: auto;"> <input type="number"
                                                                                                    id="reg-dob-year"
                                                                                                    name="reg_dob_year"
                                                                                                    placeholder="YYYY"
                                                                                                    maxlength="4"
                                                                                                    style="max-width: 100px; min-width: auto;">
                                <input type="email" id="reg-user-email" name="reg_user_email" placeholder="Email*">
                                <input type="button" name="next" class="next1 action-button" value="Next"/></fieldset>
                            <fieldset class="form-step-2">
                                <div class="custom-inputs"><input type="radio" name="vibe" id="vibe_yes" checked
                                                                  value=""> <label>I have a Vibe card</label> <input
                                        type="radio" name="vibe" id="vibe_no" value="No"> <label>Register Without a
                                        Card</label></div>
                                <input type="text" autocomplete="off" id="reg-boostie-number" name="reg_boostie_number"
                                       placeholder="Boostie Number*"> <input type="text" autocomplete="off"
                                                                             id="reg-verification-code"
                                                                             name="reg_verification_code"
                                                                             placeholder="Verification code*"> <input
                                    type="password" autocomplete="off" id="reg-user-pass" name="reg_user_pass"
                                    placeholder="Set password for your account*">
                                <div id="register-output"></div>
                                <label class="custom-input"> <input type="checkbox" id="terms" name="terms" value="1"> I
                                    have read and agree to the <a href="vibe-terms-and-conditions.html" target="_blank">Vibe
                                        Terms and Conditions*</a> </label>
                                <p>
                                <div class="g-recaptcha" data-sitekey="6Le0ZKcZAAAAAM3fOPJPMyvHht1MrHKgIoogiSqu"
                                     data-theme="dark"></div>
                                </p> <input type="button" name="previous" class="previous action-button"
                                            value="Previous"/> <input type="submit" name="submit"
                                                                      class="submit action-button" value="Register"
                                                                      id="register-trigger"/></fieldset>
                        </form>
                        <div id="after-success"><input type="hidden" id="user-email"> <input type="hidden"
                                                                                             id="user-pass">
                            <p>Congratulations!!! We just sent you an email to activate your account.</p>
                            <p>Still you can login to your new account.</p> <a class="button button-primary"
                                                                               id="trigger-login">Login to my
                                account</a></div>
                    </div>
                </div>
            </div>
            <div class="boost-carousel owl-carousel owl-theme boost-carousel-vibe">
                <div class="slide-1"><img src="img/placeholders/slider_shape_1.png" class="slider-icon-1"/></div>
                <div class="slide-2"><img src="img/placeholders/slider_shape_2.png" class="slider-icon-1"/></div>
                <div class="slide-3"><img src="img/placeholders/slider_shape_3.png" class="slider-icon-1"/></div>
                <div class="slide-4"><img src="img/placeholders/slider_shape_4.png" class="slider-icon-1"/></div>
                <div class="slide-5"><img src="img/placeholders/slider_shape_5.png" class="slider-icon-1"/></div>
            </div>
        </div>
    </div>
    <div id="vibe-learn-more">
        <div class="container">
            <div class="row">
                <ul id="vibe-learn-more-triggers">
                    <li id="trigget-faq">Frequently Asked Questions</li>
                    <li id="trigger-terms">Terms & Conditions</li>
                </ul>
                <div id="faq"><h3>Frequently Asked Questions</h3>
                    <ul class="expander">
                        @forelse($faqs as $faq)
                            <li> {{ $faq->question }}
                                <div><p>{!! $faq->answer !!}</p></div>
                            </li>
                        @empty
                            No data found
                        @endforelse
                    </ul>
                </div>
                <div id="vibe-terms">
                    <div>
                        <h3>Terms &amp; Conditions</h3>
                        {!! $page->terms !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('dist/js/jquery.form.js') }}"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript" src="{{ asset('dist/js/icheck.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/js/vibe.js') }}"></script>
@endpush
