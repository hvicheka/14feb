@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .banner-find-us {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .banner-find-us {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }

    </style>
@endpush

@section('content')
    <div class="main-fold banner-find-us"></div>
    <div class="boost-store-finder">
        <div id="boost-store-locator" class="hide-stores">
            <div class="preloader" id="boost-stores-loading">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
            <div class="boostjuice-search">
                <img src="{{ asset('images/' . $page->title_image) }}"/>
                <h2>{{ $page->name }}</h2>
                {!! $page->description !!}

                {{--                <a id="get-my-location" class="button">Use current location</a> <span--}}
                {{--                    class="choice">OR</span> <span id="autocomplete-box">--}}
                {{--                    <input autocomplete="off"--}}
                {{--                           id="boostjuice-search-input" type="text"--}}
                {{--                           value="" name="boostjuice-search-input"--}}
                {{--                           placeholder="Postcode or suburb/town"/>--}}
                {{--                    <input--}}
                {{--                        id="boostjuice-search-btn" type="submit" value=""></span></div>--}}
                <div id="boost-stores">
                    <div id="boostjuice-result-list">
                        <div id="boostjuice-stores"></div>
                    </div>
                    <div class="boostjuice-gmap-wrap">
                        <div id="boostjuice-gmap"></div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('js')

@endpush
