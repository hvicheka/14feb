@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .banner-news {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .banner-news {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }
    </style>
@endpush

@section('content')
    <div class="main-fold-angle banner-news"></div>
    <div class="container">
        <div class="row">
            <div id="blog-posts" class="eight columns inner"><h2> {{ $post->title }}</h2>
                <div class="date"> {{ $post->created_at->format('d-m-Y') }}</div>
                <a href="{{ route('front_end.news.detail', $post->slug) }}" title="NEW Nothing But Passion"> <img
                        src="{{ asset('images' . $post->image) }}"
                        class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" loading="lazy"
                        srcset="{{ asset('images/' . $post->image) }}"
                        sizes="(max-width: 720px) 100vw, 720px"/> </a>
                {!! $post->description !!}
            </div>
            <div class="four columns hide-mobile-small" id="popular-posts">

                <h2>Popular Posts</h2>
                @forelse($most_view_posts as $post)
                    <div class="row">
                        <div class="four columns"><a href="{{ route('front_end.news.detail', $post->slug) }}">
                                <img
                                    src="{{ asset('images/' . $post->image) }}"
                                    title="{{ $post->title }}" alt="{{ $post->title }}"
                                    class="blog-cover"/></a>
                        </div>
                        <div class="eight columns"><h3><a href="{{ route('front_end.news.detail', $post->slug) }}">
                                    {{ $post->title }}
                                </a>
                            </h3>
                            <p>{{ $post->created_at->format('d-m-Y') }}</p>
                        </div>
                        @empty
                            No data found
                        @endforelse
                    </div>
            </div>
        </div>
@endsection

@push('js')

@endpush
