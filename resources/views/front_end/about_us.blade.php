@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .banner-about-boost {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .banner-about-boost {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }
    </style>
@endpush

@section('content')
    <div class="main-fold banner-about-boost"></div>
    <div class="sub-container">
        <div class="sub-fold">
            <div class="container">
                <div class="row fix-content">
                    <div class="columns text-center"><img src="{{ asset('images/'. $page->title_image) }}">
                        {!! $page->description !!}</div>
                </div>
            </div>
        </div>
        <div class="container full about-us">
            @forelse($abouts as $about)
                @if($loop->even)
                    <div class="row">
                        <div class="six columns left-fix'"><h3>{{ $about->title }}</h3>
                            {!! $about->description !!}
                        </div>
                        <div class="six columns">
                            <img src="{{ asset('images/' . $about->image) }}" alt="placeholder"/>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="six columns hide-mobile">
                            <img src="{{ asset('images/' . $about->image) }}"
                                 alt="placeholder"/>
                        </div>
                        <div class="six columns right-fix"><h3>{{ $about->title }}</h3>
                            {!! $about->description !!}
                        </div>
                    </div>
                @endif
            @empty
                No data found
            @endforelse
        </div>
    </div>
@endsection

@push('js')

@endpush
