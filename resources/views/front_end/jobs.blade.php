@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .expander li {
            position: relative;
            cursor: pointer;
            padding-right: 25px;
            margin-bottom: 30px;
            color: black;
        }

        .work-for-boost {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .work-for-boost {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }
    </style>
@endpush

@section('content')
    <div class="main-fold work-for-boost"></div>
    <div class="sub-fold fix-content-large">
        <div class="container">
            <div class="row fix-content">
                <div class="columns text-center">
                    <img src="{{ asset('images/' . $page->title_image) }}">
                    {!! $page->description !!}
                </div>
            </div>
        </div>
        <div class="sub-container sides-off">
            <div class="jobs-faq">
                <div class="container narrow"><h2>Frequently Asked Questions</h2>
                    <ul class="expander">
                        @forelse($faqs as $faq)
                            <li class="text-primary">
                                {{ $faq->question }}
                                <div>
                                    <p>{!! $faq->answer !!}</p>
                                </div>
                            </li>
                        @empty
                            No data found
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>

@endsection

@push('js')

@endpush
