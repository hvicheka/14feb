<!doctype html>
<html lang="en">
<head>
    <title>{{ $page->name }}</title>
    @include('front_end.layouts.partials.style')
    <link rel="stylesheet" media="all" href="{{ asset('hc/application.css') }}" id="stylesheet"/>
    <link rel="stylesheet" media="all" href="{{ asset('hc/theming_v1_support.css') }}" id="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('hc/style.css') }}">
</head>
<body class="">
<header class="header">
    <div class="header_inner">
        <div class="logo">
            <a href="{{ route('front_end.home') }}">
                <img src="{{ asset('images/' .$setting->logo->image ) }}"
                     alt="{{ $setting->logo->value }}">
            </a>
        </div>
    </div>
</header>

<main role="main">
    <section class="section hero">
        <div class="hero-inner">
            <h1>{{ $page->name }}</h1>
            <h3 class="hero-greeting">{{ $page->descrption }}</h3>
        </div>
    </section>

    <div class="contactuspanel">
        <div class="contactus_inner">
            <h3 class="contactus_text">{{ $setting->contact_us_title->value }}</h3>
            <a href="mailto:{{ $setting->email->value }}"
               class="btn-primary backtosite">{{ $setting->send_mail_title->value }}</a>
            <div class="callusmaybe">
                <img width="50"
                     src="{{ asset('images/' . $setting->phone_call_icon->image) }}"><span>{{ $setting->phone_call_title->value }}</span>
            </div>
            <p>{{ $setting->workinghours->value }}</p>
            <div>
                <span>{{ $setting->phone->value }}</span>
            </div>
        </div>
    </div>

</main>

@include('front_end.layouts.partials.footer')


<!-- / -->

<script src="//static.zdassets.com/hc/assets/en-us.5821d45ace4ef9adbdb8.js"></script>
<script src="https://boostjuice.zendesk.com/auth/v2/host.js" data-brand-id="3784768"
        data-return-to="https://help.boostjuice.com.au/hc/en-us" data-theme="hc" data-locale="en-us"
        data-auth-origin="3784768,true,true"></script>

{{--<script type="text/javascript">--}}
{{--    HelpCenter.account = {"subdomain":"boostjuice","environment":"production","name":"Retail Zoo"};--}}
{{--    HelpCenter.user = {"identifier":"da39a3ee5e6b4b0d3255bfef95601890afd80709","email":null,"name":null,"role":"anonymous","avatar_url":"https://assets.zendesk.com/hc/assets/default_avatar.png","is_admin":false,"organizations":[],"groups":[]};--}}
{{--    HelpCenter.internal = {"asset_url":"//static.zdassets.com/hc/assets/","web_widget_asset_composer_url":"https://static.zdassets.com/ekr/snippet.js","current_session":{"locale":"en-us","csrf_token":null,"shared_csrf_token":null},"settings":{},"usage_tracking":{"event":"front_page_viewed","data":"BAh7BjoKX21ldGF7CzoPYWNjb3VudF9pZGkDB2kVOhNoZWxwX2NlbnRlcl9pZGwrCFgYrNFTADoNYnJhbmRfaWRpA0DAOToMdXNlcl9pZDA6E3VzZXJfcm9sZV9uYW1lSSIOQU5PTllNT1VTBjoGRVQ6C2xvY2FsZUkiCmVuLXVzBjsLVA==--9e714524b8c022db7c2ee13213ae4f79d67b6e9e","url":"https://help.boostjuice.com.au/hc/activity"},"current_record_id":null,"current_record_url":null,"current_record_title":null,"search_results_count":null,"current_text_direction":"ltr","current_brand":{"id":3784768,"account_id":1403143,"name":"Boost Juice","active":true,"deleted_at":null,"created_at":"2017-01-19T02:21:56Z","updated_at":"2020-07-01T05:27:45Z","route_id":1320478,"signature_template":"Kind regards, \r\n{{agent.signature}}\r\nBoost Juice Australia\r\nwww.boostjuice.com.au\r\n\r\nTel: 03 9508 4409 | Email: boost@boostjuice.com.au\r\n"},"current_brand_url":"https://boostjuice.zendesk.com","current_host_mapping":"help.boostjuice.com.au","current_path":null,"authentication_domain":"https://boostjuice.zendesk.com","show_autocomplete_breadcrumbs":true,"rollbar_config":{"enabled":true,"endpoint":"https://rollbar-us.zendesk.com/api/1/item/","accessToken":"731a5a953e9a4b7ab6cac9623f50c732","captureUncaught":true,"captureUnhandledRejections":true,"payload":{"environment":"production","client":{"javascript":{"source_map_enabled":true,"code_version":"cfd63a87658f6c47b780aae77ce502e42eae67d9","guess_uncaught_frames":true}}}},"user_info_changing_enabled":false,"has_user_profiles_enabled":true,"has_end_user_attachments":true,"user_aliases_enabled":false,"has_anonymous_kb_voting":true,"has_multi_language_help_center":true,"mobile_device":false,"mobile_site_enabled":false,"show_at_mentions":true,"embeddables_config":{"embeddables_web_widget":false,"embeddables_connect_ipms":false},"base_domain":"zendesk.com","answer_bot_subdomain":"static","manage_content_url":"https://help.boostjuice.com.au/hc/en-us","arrange_content_url":"https://help.boostjuice.com.au/hc/admin/arrange_contents?locale=en-us","general_settings_url":"https://help.boostjuice.com.au/hc/admin/general_settings?locale=en-us","user_segments_url":"https://boostjuice.zendesk.com/knowledge/user_segments?brand_id=3784768","has_gather":true,"has_community_enabled":false,"has_community_badges":false,"has_user_segments":true,"has_answer_bot_web_form_enabled":false,"has_answer_bot_web_form_modal_v2":false,"has_edit_user_profile_v2":true,"billing_url":"/access/return_to?return_to=https://boostjuice.zendesk.com/admin/billing/subscription","is_account_owner":false,"theming_cookie_key":"hc-da39a3ee5e6b4b0d3255bfef95601890afd807091-preview","is_preview":false,"has_guide_user_segments_search":true,"has_alternate_templates":true,"arrange_articles_url":"https://boostjuice.zendesk.com/knowledge/arrange?brand_id=3784768","article_verification_url":"https://boostjuice.zendesk.com/knowledge/verification?brand_id=3784768","has_article_verification":true,"guide_language_settings_url":"https://help.boostjuice.com.au/hc/admin/language_settings?locale=en-us","docs_importer_url":"https://boostjuice.zendesk.com/knowledge/import_articles?brand_id=3784768","community_badges_url":"https://boostjuice.zendesk.com/knowledge/community_badges?brand_id=3784768","community_settings_url":"https://boostjuice.zendesk.com/knowledge/community_settings?brand_id=3784768","gather_plan_state":"subscribed","search_settings_url":"https://boostjuice.zendesk.com/knowledge/search_settings?brand_id=3784768","has_multibrand_search_in_plan":true,"theming_api_version":1,"theming_settings":{"brand_color":"#FB7923","secondary_brand_color":"rgba(122, 193, 53, 1)","brand_text_color":"rgba(122, 193, 53, 1)","text_color":"rgba(89, 89, 89, 1)","link_color":"rgba(89, 89, 89, 1)","background_color":"#FFFFFF","btn_primary_bg_color":"#FB7923","btn_primary_text_color":"#FFFFFF","btn_primary_bg_color_hov":"#FB7923","btn_primary_text_color_hov":"#FFFFFF","btn_secondary_bg_color":"#fff","btn_secondary_text_color":"#133f6e","btn_secondary_bg_color_hov":"#133f6e","btn_secondary_text_color_hov":"#fff","generic_border_color":"#EAEAEA","category_tile_bg_color":"#fff","heading_font":"-apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif","text_font":"-apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif","logo":"//theme.zdassets.com/theme_assets/1403143/40cec215814aee4d75d241c8d3332426fe0800f8.png","favicon":"//theme.zdassets.com/theme_assets/1403143/ff172f3c378e1ff787c1c48c1236ee993f0b336b.png","header_button_text":"Customer Service","header_button_link":"https://boostjuice.zendesk.com/hc/en-us/requests/new","homepage_background_image":"//theme.zdassets.com/theme_assets/1403143/304b5c5fd1f3d45cd0da520ba2cff9cd4f4f9c62.png","community_background_image":"//theme.zdassets.com/theme_assets/1403143/ea0fc49e94c74884940bd530924916ea87035a5f.svg","community_image":"//theme.zdassets.com/theme_assets/1403143/e58c5e8c3ebf94cbbc4d3669def0adbd7d0ff479.svg","search_banner_greeting_text":"Let Us Know How We May Help You","searchbar_placeholder":"Search","scoped_kb_search":true,"scoped_community_search":true,"show_recent_activity":true,"promoted_articles_header_text":"Most Popular Questions","contactus_msg":"Still Couldn't Find What You Want? It's Ok, We Are Here To Help You","contact_us_btn_text":"Send Us An Email","call_us_text":"Call Us Maybe?","helpdesk_availability":"(Monday - Friday 9am-5pm EST)","helpdesk_contact_number":"Phone: (+61) 3 9508 4409 Fax:(+61) 3 9508 4499","show_articles_in_section":true,"show_article_author":false,"show_article_comments":false,"show_follow_article":false,"show_recently_viewed_articles":false,"show_related_articles":false,"show_article_sharing":false,"show_follow_section":true,"store_finder_link":"https://www.reece.com.au/storefinder","store_finder_label":"Online Store Finder","weekly_schedule_range":"Monday to Friday","daily_time_schedule":"8:30AM — 5:00PM","office_location_label":"Reception:","office_phone":"+61 3 9274 0000","office_fax":"+61 3 9274 0198","show_follow_post":false,"show_post_sharing":false,"show_follow_topic":true},"has_pci_credit_card_custom_field":true,"current_brand_id":3784768,"help_center_restricted":false,"current_brand_active":true,"is_assuming_someone_else":false,"flash_messages":[],"user_photo_editing_enabled":true,"has_end_user_apps":false,"has_docs_importer":true,"has_sunco_widget_kill_switch":false,"gather_user_image_upload_via_aus":false,"user_preferred_locale":"en-us"};--}}
{{--</script>--}}

<script src="{{ asset('hc/moment.js') }}"></script>
<script src="{{ asset('hc/hc_enduser.js') }}"></script>

<script type="text/javascript">(function () {
        window['__CF$cv$params'] = {
            r: '6d50a53999bcc70c',
            m: 'TcLeITcbyfLCbhvdYHDEmQbnlnpBZ0spRc.F7y4QWlw-1643440275-0-AQZhUYa7fwxkSQ5LKuUc5pcMKF+PgFyB6jE2G6zEiuQiWqLZ6JAHuRM8/IO3dKySNxG+X6Mi9sYbjyQK1cht5VUSwgcFV3h+MFdatxkVrGid6vZRtI8kSUtI+2DuhYYdkA==',
            s: [0x94fee84a21, 0x6452fb4a61],
        }
    })();</script>
<div>
    <div class="_63983a716933a264792c2d8aeb1396b7-css"></div>
</div>
</body>
</html>
