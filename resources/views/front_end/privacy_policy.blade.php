@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .vibe-banner-bkg {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .vibe-banner-bkg {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }
    </style>
@endpush

@section('content')
    <div class="main-fold-angle vibe-banner-bkg"></div>
    <div class="container simple-inner"><h2></h2>
        {!! $page->description !!}
    </div>
@endsection

@push('js')

@endpush
