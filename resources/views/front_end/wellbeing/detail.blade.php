@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .banner-health {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .banner-health {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }
    </style>

@endpush

@section('content')

    <div class="main-fold-angle banner-health"></div>
    <div class="container">
        <div class="row">
            <div id="blog-posts" class="eight columns inner">
                <h2>{{ $wellbeing->title }} </h2>
                <a href="{{ route('front_end.wellbeing.detail', $wellbeing->slug) }}" title="{{ $wellbeing->title }}">
                    <img src="{{ asset('images/' . $wellbeing->image) }}"
                         class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" loading="lazy"
                         srcset="{{ asset('images/' . $wellbeing->image) }}"
                         sizes="(max-width: 615px) 100vw, 615px"/>
                </a>

                {!! $wellbeing->description !!}

            </div>
            <div class="four columns hide-mobile-small" id="popular-posts"><h2>Popular Posts</h2>
                @forelse($popular as $post)
                    <div class="row">
                        <div class="four columns">
                            <a href="{{ route('front_end.wellbeing.detail', $post->slug) }}">
                                <img
                                    src="{{ asset('images/' . $post->image) }}"
                                    title="{{ $post->title }}"
                                    alt="{{ $post->title }}" class="blog-cover"/></a>
                        </div>
                        <div class="eight columns"><h3><a
                                    href="{{ route('front_end.wellbeing.detail', $post->slug) }}">{{ $post->title }}</a>
                            </h3></div>
                    </div>
                @empty
                    No data found
                @endforelse

            </div>
        </div>
    </div>

@endsection

@push('js')

@endpush
