@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')

    <style>
        .banner-health {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .banner-health {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }
    </style>

@endpush

@section('content')
    <div class="main-fold-angle banner-health"></div>
    <div id="blog-posts" class="container">
        <div id="blog-posts" class="container">
            <div class="preloader" style="display: none;">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
            @forelse($wellbeings as $wellbing)
                @if($loop->even)
                    <div class="row" style="display: block;">
                        <div class="six columns">
                            <p>
                                <a
                                    href="{{ route('front_end.wellbeing.detail', $wellbing->slug) }}">
                                    <img
                                        src="{{ asset('images/' . $wellbing->image) }}"
                                        title="{{ $wellbing->title }}" alt="{{ $wellbing->title }}" class="blog-cover">
                                </a>
                            </p>
                        </div>
                        <div class="six columns"><h2>{{ $wellbing->title }}</h2>
                            {!! $wellbing->short_description !!}
                            <a href="{{ route('front_end.wellbeing.detail', $wellbing->slug) }}">Read
                                more</a>
                        </div>
                    </div>
                @else
                    <div class="row" style="display: block;">
                        <div class="six columns"><h2>{{ $wellbing->title }}</h2>
                            {!! $wellbing->short_description !!}
                            <a
                                href="{{ route('front_end.wellbeing.detail', $wellbing->slug) }}">Read
                                more</a>
                        </div>
                        <div class="six columns">
                            <p>
                                <a
                                    href="{{ route('front_end.wellbeing.detail', $wellbing->slug) }}"><img
                                        src="{{ asset('images/' . $wellbing->image) }}"
                                        title="{{ $wellbing->title }}" alt="{{ $wellbing->title }}" class="blog-cover"></a>
                            </p>
                        </div>
                    </div>
                @endif
            @empty
                No data found
            @endforelse

            <div class="pagination">
                {{ $wellbeings->links('back-end.layout.partials.pagination') }}
            </div>

        </div>
    </div>

    <div id="end">This is the end of the posts page</div>
@endsection

@push('js')


@endpush
