<!doctype html>
<html lang="en-AU">
<head>
    <title>@yield('title',$setting->site_name->value){{  ' - ' .$setting->site_name->value }}</title>
    @include('front_end.layouts.partials.style')
    @stack('css')
</head>
<body class="boost-home">
@include('front_end.layouts.partials.header')

<div class="boost-main-content">
    @yield('content')
    @include('front_end.layouts.partials.footer')
</div>
@include('front_end.layouts.partials.scripts')
@stack('js')
</body>
</html>
