<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="description"
      content="{{ $setting->content_description->value }}">
<meta name="author" content="{{ asset('images/' . $setting->site_name->value) }}">
<link rel="apple-touch-icon" href="{{ asset('images/' . $setting->icon->image) }}">
<link rel="icon" href="{{ asset('images/' . $setting->icon->image) }}" type="image/x-icon">
<link rel="shortcut icon" href="{{ asset('images/' . $setting->icon->image) }}" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Assistant:300,400,600,700|Crete+Round:400,400i"
      rel="stylesheet">

<link rel="stylesheet" href="{{ asset('dist/css/boostjuice_v2.css') }}">
<style>
    .pagination {
        list-style-type: none;
    }

    .pagination .page-link {
        padding: 5px;
        text-decoration: none !important;
    }

    .pagination .active {
        text-decoration: underline !important;
        color: black !important;
    }

    @media (min-width: 1025px) {
        header.is-fixed .sub-navi {
            padding: 18px 20px;
            -webkit-transition: padding 0.5s ease-in-out;
            transition: padding 0.5s ease-in-out;
            top: -67px;
            background-color: rgba(255, 255, 255, 0.9);
        }
    }
</style>
<script type="text/javascript" src="{{ asset('dist/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/tempo.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/jquery.easing.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/jquery.tokeninput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/boost-v3.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/home.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/navigation.js') }}"></script>
<script src="{{ asset('dist/js/modernizr.custom.js') }}"></script>
<script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MGPNTZN');
</script>
