<footer class="footer" role="contentinfo">
    <div class="footer_inner">
        <span class="social_links">
            <a href="{{ $setting->facebook->link }}" target="_blank">
                <img width="50"
                     src="{{ asset('images/' . $setting->facebook->image) }}"
                     alt="Facebook"/>
            </a>
            <a href="{{ $setting->twitter->link }}" target="_blank">
                <img width="50"
                    src="{{ asset('images/' . $setting->twitter->image) }}"
                    alt="Twitter"/>
            </a>
            <a href="{{ $setting->instagram->link }}" target="_blank">
                <img width="50"
                    src="{{ asset('images/' . $setting->instagram->image) }}" alt="Instagram"/>
            </a>
        <span/>
        <nav class="footer-nav">
            <ul>
                <li><a href="{{ route('front_end.about_us') }}">About</a></li>
                <li><a href="{{ route('front_end.franchising.index') }}">Franchising</a></li>
                <li><a href="{{ route('front_end.jobs') }}">Careers</a></li>
                <li><a href="{{ route('front_end.vibe') }}">VIBE</a></li>
            </ul>
        </nav>
        <p class="copyright"> &copy; @php
                $dt = new DateTime('now', new DateTimezone('Asia/Phnom_Penh'));
                echo $dt->format('Y');
            @endphp Copyright {{ $setting->site_name->value }}
            <span class="hide-mobile">|</span>
            <a href="{{ route('front_end.term') }}">Terms & Conditions</a>
            <span class="hide-mobile">|</span>
            <a href="{{ route('front_end.privacy_policy') }}">Privacy</a>
        </p>
    </div>
</footer>
