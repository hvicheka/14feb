<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MGPNTZN" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<header class="boost-main">
    <nav class="boost-main-nav-wrapper">
        <ul class="boost-main-nav">
            <li class="first-item"><a class="boost-subnav-trigger-off" href="{{ route('front_end.about_us') }}">About
                    Us</a></li>
            <li><a class="boost-subnav-trigger-off" href="{{ route('front_end.franchising.index') }}">Franchising</a>
            </li>
            <li><a class="boost-subnav-trigger-off" href="{{ route('front_end.jobs') }}">Job</a></li>
            <li></li>
            <li></li>
        </ul>
    </nav>
    <a class="boost-nav-trigger"><span></span></a>
    <div class="boost-logo">
        <img src="{{ asset('images/' . $logo->image) }}" alt="Boost Juice" id="boost-logo">
    </div>
    <nav class="sub-navi">
        <ul>
            <li id="menu-item-500" class="remove-me active"><a href="{{ route('front_end.drinks') }}">Our Drinks</a>
            </li>
            <li id="menu-item-501" class="remove-me"><a href="{{ route('front_end.stores') }}">Find Us</a></li>
            <li id="menu-item-502" class="remove-me"><a href="{{ route('front_end.vibe') }}">VIBE</a></li>
            <li id="menu-item-503" class="remove-me"><a href="{{ route('front_end.wellbeing.index') }}">Wellbeing</a>
            </li>
            <li id="menu-item-504" class="remove-me"><a href="{{ route('front_end.news.index') }}">News</a></li>
            <li id="menu-item-505" class="remove-me"><a href="{{ route('front_end.contact') }}">Contact Us</a></li>
        </ul>
    </nav>
    <section id="vibe-login">
        <div class="preloader">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <div id="vibe-choice"></div>
        <div id="vibe-quick-view">
            <div id="vibe-quick-view-info"></div>
            <br> <a id="logout-trigger" class="button">Log out</a> <a href="{{ route('front_end.vibe') }}"
                                                                      class="button button-primary">My VIBE</a></div>
    </section>
</header>
