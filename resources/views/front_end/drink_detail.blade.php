@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .banner-drinks {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .banner-drinks {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }

        .title {
            color: #67b94c !important;
            font-size: 52px !important;
            line-height: 54px !important;
            margin-bottom: 15px !important;
            text-transform: uppercase !important;
            font-weight: 600 !important;
            position: relative;
            z-index: 30;
        }

        .main-fold-angle {
            background-image: url("{{ asset('images/' . $page->image) }}");
            background-repeat: no-repeat;
            background-position: center top;
            min-height: 25vw;
            position: relative;
            margin-bottom: 0px;
        }
    </style>
@endpush

@section('content')
    <div class="main-fold banner-drinks"></div>
    <div class="container">
        <section class="hide-mobile-small" style="padding-top: 150px"></section>
        <div class="drinks-container">
            <div class="row">
                <div class="four columns" id="popular-posts">
                    <div class="hide-mobile-small" style="margin: 100px"></div>
                    <div style="margin: 50px"></div>
                    <h2 class="title">{{ $product->name }}</h2>
                    <p>
                        {{ $product->ingredients->implode('name', ', ') }}
                    </p>
                    <ul>
                        <p>Health Benefits:</p>
                        @foreach($product->benefits as $benefit)
                            <li>
                                {{ $benefit->name}}
                            </li>
                        @endforeach
                    </ul>
                    {!! $product->description !!}
                </div>
                <div id="blog-posts" class="eight columns text-right">
                    <img
                        src="{{ $product->full_image_url['main_image'] }}" alt="Main Image">
                </div>

            </div>
            <div><h1 class="title" style="text-align: center">All Other Drinks</h1></div>
            <div class="main">
                <ul id="og-grid" class="og-grid">
                    @forelse($products as $product)
                        <li class="drink-block {{ $product->categories->implode('slug', ' ') }}   {{ $product->benefits->implode('slug', ' ') }}  ">
                            <a href="{{ route('front_end.drinks.detail', $product->slug) }}"
                               data-menu="{{ $product->slug }}" class="grid_item">
                                <img
                                    src="{{ $product->full_image_url['thumbnail_image'] }}"/>
                                <img
                                    src="{{ $product->full_image_url['cup_shadow_image'] }}"/>
                                <img src="{{ $product->full_image_url['ingredient_image'] }}" class="topleft"/>
                                <img
                                    src="{{ $product->full_image_url['main_image'] }}"/>
                                <ul>
                                    @foreach($product->benefits as $benefit)
                                        <li>
                                        <span>
                                            <img src="{{ asset('images/' . $benefit->image) }}">
                                        </span>
                                            {{ $benefit->name }}
                                        </li>
                                    @endforeach
                                </ul>
                                <span>{{ $product->name }}</span>
                            </a>

                        </li>
                    @empty
                        No Product
                    @endforelse

                </ul>
            </div>
        </div>
    </div>


@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('dist/js/grid.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/js/main-grid.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/js/jquery.tokeninput.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".og-grid a").on("click", function (e) {
                e.preventDefault()
                window.location.href = $(this).attr('href')
            })
        })
    </script>
@endpush
