@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')

    <style>
        .banner-drinks {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .banner-drinks {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }

        #ingredient {
            font-size: 16px;
            display: block;
            padding: 10px;
            border: 1px solid #CCC;
            min-width: 100%;
            margin: 25px auto;
            padding-right: 50px;

        }

        #searchFilters {
            display: block;
            width: 45px;
            background-color: #65b949;
            cursor: pointer;
            background-image: url('{{ asset('img/placeholders/icon_search.png') }}');
            background-repeat: no-repeat;
            background-position: center center;
            background-size: 20px;
            overflow: hidden;
            text-indent: -200px;
            bottom: 23px;
            right: 11px;
            margin: 0;
            padding: 0;
            position: absolute;
        }

        #search-filter {
            width: 410px;
            display: block;
            margin: 30px auto;
            position: relative;
            min-height: 0px;
        }

    </style>
@endpush

@section('content')
    <div class="main-fold banner-drinks"></div>
    <div id="drinks-overlay">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
        <div id="progstat"></div>
        <div id="progress"></div>
    </div>
    <ul id="main-filter-sections">
        <li id="section-type" class="active">Choose <br class="hide-desktop">drink type</li>
        <li id="section-benefits">Choose <br class="hide-desktop">health benefits</li>
    </ul>
    <section id="main-filter">
        <ul id="type" class="main-filter-set">

            @foreach($categories as $category)
                <li class="{{ $category->slug }}">
                <span>
                    <img src="{{ asset('images/' . $category->image)}}">
                </span>
                    {{ $category->name }}
                </li>
            @endforeach

        </ul>
        <ul id="benefits" class="main-filter-set">
            @foreach($benefits as $benefit)
                <li class="{{ $benefit->slug}}">
                    <span>
                       <img src="{{ asset('images/' . $benefit->image)}}">
                    </span>
                    {{ $benefit->name }}</li>
            @endforeach
        </ul>
    </section>
    <div id="view-all-sidebar">View all</div>
    <section id="search-filter">
        <input type="text" id="ingredient" name="ingredient" class="d-none" placeholder="Search Drinks"
               style="display: none"/>
        <input type="button" value="Search" id="searchFilters"/>
    </section>
    <div class="drinks-container">
        <div class="main">
            <ul id="og-grid" class="og-grid">
                @forelse($products as $product)
                    <li class="drink-block {{ $product->categories->implode('slug', ' ') }}   {{ $product->benefits->implode('slug', ' ') }}  ">
                        <a href="{{ route('front_end.drinks.detail', $product->slug) }}"
                           data-menu="{{ $product->slug }}" class="grid_item">
                            <img
                                src="{{ $product->full_image_url['thumbnail_image'] }}"/>
                            <img
                                src="{{ $product->full_image_url['cup_shadow_image'] }}"/>
                            <img src="{{ $product->full_image_url['ingredient_image'] }}" class="topleft"/>
                            <img
                                src="{{ $product->full_image_url['main_image'] }}"/>
                            <ul>
                                @foreach($product->benefits as $benefit)
                                    <li>
                                        <span>
                                            <img src="{{ asset('images/' . $benefit->image) }}">
                                        </span>
                                        {{ $benefit->name }}
                                    </li>
                                @endforeach
                            </ul>
                            <span>{{ $product->name }}</span>
                        </a>

                    </li>
                @empty
                    No Product
                @endforelse

            </ul>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('dist/js/grid.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/js/main-grid.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dist/js/jquery.tokeninput.min.js') }}"></script>
    <script>

        $(document).ready(function () {
            $("#ingredient").tokenInput([
                    @foreach($products as $product)
                {
                    id: ".{{ $product->slug }}", name: "{{ $product->name }}"
                },
                @endforeach
            ], {
                excludeCurrent: true,
                searchingText: 'Searching for drinks',
                resultsLimit: 5,
                noResultsText: 'No drinks found',
                // hintText: 'Search Drinks',
                tokenDelimiter: '',
                preventDuplicates: true,
                onAdd: function (item) {
                    triggetFilterSearch();
                },
                onDelete: function (item) {
                    resetGrid();
                }
            });

            $("#token-input-ingredient").attr("placeholder", "Search Drinks");
        });
        $(".og-grid a").on("click", function (e) {
            e.preventDefault()
            window.location.href = $(this).attr('href')
        })
    </script>
@endpush
