@extends('front_end.layouts.app')

@section('title', $page->name)

<style>
    .main-fold-angle {
        background-image: url("{{ asset('images/' . $page->image) }}") !important;
    }

    .vibe-banner-bkg {
        background-image: url("{{ asset('images/' . $page->image) }}") !important;
    }

    @media (max-width: 1024px) {
        .vibe-banner-bkg {
            background-image: url("{{ asset('images/' . $page->image) }}") !important;
            min-height: 46vw;
        }
    }
    @media (max-width: 1440px) {
        .vibe-banner-bkg {
            background-image: url("{{ asset('images/' . $page->image) }}") !important;
            min-height: 46vw;
        }
    }

</style>

@push('css')

@endpush

@section('content')
    <div class="main-fold-angle vibe-banner-bkg"></div>
    <div class="container simple-inner"><h2></h2>
        {!! $page->description !!}
    </div>
@endsection

@push('js')

@endpush
