@extends('front_end.layouts.app')

@section('title', $page->name)

@push('css')
    <style>
        .location-hint #location-hint {
            text-align: center;
            position: fixed;
            bottom: 0px;
            background-color: #fff;
            width: 100%;
            z-index: 999;
            height: 70px;
            border-top: 1px solid #eee;
        }

        .banner-own-a-boost {
            background-image: url("{{ asset('images/' . $page->image) }}");
        }

        @media (max-width: 1440px) {
            .banner-own-a-boost {
                background-image: url("{{ asset('images/' . $page->image) }}");
                min-height: 46vw;
            }
        }
    </style>
@endpush

@section('content')
    <div class="main-fold banner-own-a-boost"></div>
    <div class="sub-fold fix-content-large">
        <div class="container">
            <div class="row fix-content">
                <div class="columns text-center">
                    <img src="{{ asset('images/' . $page->title_image) }}">
                    <p>
                        {!! $page->description !!}
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="sub-container sides-off clear-top">
        <br><br>
        <div class="around-the-world-faq">
            <div class="container narrow"><h2>Frequently Asked Questions</h2>
                <ul class="expander">
                    @forelse($faqs as $faq)
                        <li> {{ $faq->question }}
                            <div><p>{!! $faq->answer !!}</p></div>
                        </li>
                    @empty
                        No data found
                    @endforelse

                </ul>
            </div>
        </div>
        <div class="block-green sub-fold-footer hide-mobile">
            <div class="container">
                <div class="row fix-content">
                    <div class="columns text-center"><h4>Talk to our Franchising team<br>for more information.</h4>
                        <p class="contact-info"><a href="mailto:boostinfo@retailzoo.com.au"> <img
                                    src="/img/placeholders/icon_email.png" alt="Email"/>boostinfo@retailzoo.com.au </a>
                            <a
                                href="tel:+61395084409"> <img src="/img/placeholders/icon_phone.png" alt="Phone"/>(03)
                                9508 4409 | option 2 </a></p></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="https://8298356.fls.doubleclick.net/activityi;src=8298356;type=001;cat=001;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
    </script>
@endpush
