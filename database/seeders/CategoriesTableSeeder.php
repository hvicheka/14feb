<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Smoothie',
                'slug' => 'smoothie',
                'image' => '2022-02-10-6204c4947fedd.png',
                'created_at' => '2022-02-03 02:30:50',
                'updated_at' => '2022-02-10 07:53:56',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Blended',
                'slug' => 'blended',
                'image' => '2022-02-10-6204c48c54356.png',
                'created_at' => '2022-02-04 02:18:25',
                'updated_at' => '2022-02-10 07:53:48',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Juiced',
                'slug' => 'juiced',
                'image' => '2022-02-10-6204c4806105e.png',
                'created_at' => '2022-02-04 02:18:45',
                'updated_at' => '2022-02-10 07:53:36',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Crushed',
                'slug' => 'crushed',
                'image' => '2022-02-10-6204c47804179.png',
                'created_at' => '2022-02-04 02:19:00',
                'updated_at' => '2022-02-10 07:53:28',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}