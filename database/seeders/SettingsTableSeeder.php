<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'logo',
                'value' => 'logo',
                'link' => NULL,
                'image' => '2022-02-09-6203918da8a0f.png',
                'created_at' => '2022-02-09 09:44:24',
                'updated_at' => '2022-02-09 10:03:57',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'icon',
                'value' => 'icon',
                'link' => NULL,
                'image' => '2022-02-10-620474619b5ec.png',
                'created_at' => '2022-02-09 09:49:42',
                'updated_at' => '2022-02-10 02:11:55',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'workinghours',
            'value' => '(Monday - Friday 9am-5pm EST)',
                'link' => NULL,
                'image' => 'def.png',
                'created_at' => '2022-02-10 02:10:51',
                'updated_at' => '2022-02-10 02:10:51',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'phone',
            'value' => '(+61) 3 9508 4409',
                'link' => NULL,
                'image' => 'def.png',
                'created_at' => '2022-02-10 02:12:34',
                'updated_at' => '2022-02-10 02:12:34',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'email',
                'value' => 'admin@gmail.com',
                'link' => NULL,
                'image' => 'def.png',
                'created_at' => '2022-02-10 02:15:05',
                'updated_at' => '2022-02-10 02:15:05',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'contact_us_title',
                'value' => 'Still Couldn\'t Find What You Want? It\'s Ok, We Are Here To Help You',
                'link' => NULL,
                'image' => 'def.png',
                'created_at' => '2022-02-10 02:53:54',
                'updated_at' => '2022-02-10 02:53:54',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'site_name',
                'value' => '14 Feb',
                'link' => NULL,
                'image' => 'def.png',
                'created_at' => '2022-02-10 03:30:17',
                'updated_at' => '2022-02-10 04:37:32',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'facebook',
                'value' => 'facebook',
                'link' => 'https://www.facebook.com/boostjuice',
                'image' => '2022-02-10-62048a5359ae9.png',
                'created_at' => '2022-02-10 03:42:55',
                'updated_at' => '2022-02-10 03:47:52',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'twitter',
                'value' => 'twitter',
                'link' => 'https://twitter.com/boostjuiceoz',
                'image' => '2022-02-10-62048b33b48de.png',
                'created_at' => '2022-02-10 03:43:18',
                'updated_at' => '2022-02-10 03:49:07',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'instagram',
                'value' => 'instagram',
                'link' => 'https://www.instagram.com/boost_juice/',
                'image' => '2022-02-10-62048b44b1bbb.png',
                'created_at' => '2022-02-10 03:43:55',
                'updated_at' => '2022-02-10 03:49:24',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'phone_call_icon',
                'value' => 'phone_call_icon',
                'link' => NULL,
                'image' => '2022-02-10-62048bcd7cfce.png',
                'created_at' => '2022-02-10 03:51:41',
                'updated_at' => '2022-02-10 03:51:41',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'phone_call_title',
                'value' => 'Call us maybe?',
                'link' => NULL,
                'image' => 'def.png',
                'created_at' => '2022-02-10 03:53:26',
                'updated_at' => '2022-02-10 03:53:26',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'send_mail_title',
                'value' => 'SEND US AN EMAIL',
                'link' => NULL,
                'image' => 'def.png',
                'created_at' => '2022-02-10 04:12:02',
                'updated_at' => '2022-02-10 04:12:02',
            ),
        ));
        
        
    }
}