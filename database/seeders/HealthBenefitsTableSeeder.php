<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HealthBenefitsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('health_benefits')->delete();
        
        \DB::table('health_benefits')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Dairy Free',
                'slug' => 'dairy-free',
                'image' => '2022-02-10-6204c5d88fd0f.png',
                'created_at' => '2022-02-03 02:34:23',
                'updated_at' => '2022-02-10 07:59:20',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Low Fat',
                'slug' => 'low-fat',
                'image' => '2022-02-10-6204c5ac3d190.png',
                'created_at' => '2022-02-04 02:19:48',
                'updated_at' => '2022-02-10 07:58:36',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Gluten Free',
                'slug' => 'gluten-free',
                'image' => '2022-02-10-6204c59e2393c.png',
                'created_at' => '2022-02-04 02:20:07',
                'updated_at' => '2022-02-10 07:58:22',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Ralph Bernard',
                'slug' => 'ralph-bernard',
                'image' => 'def.png',
                'created_at' => '2022-02-07 05:57:18',
                'updated_at' => '2022-02-07 05:57:22',
                'deleted_at' => '2022-02-07 05:57:22',
            ),
        ));
        
        
    }
}