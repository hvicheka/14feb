<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class IngredientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ingredients')->delete();
        
        \DB::table('ingredients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Low Fat Milk',
                'slug' => 'low-fat-milk',
                'created_at' => '2022-02-03 02:43:25',
                'updated_at' => '2022-02-07 02:10:21',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Banana',
                'slug' => 'banana',
                'created_at' => '2022-02-03 02:43:34',
                'updated_at' => '2022-02-07 02:10:17',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Honey',
                'slug' => 'honey',
                'created_at' => '2022-02-03 02:43:48',
                'updated_at' => '2022-02-07 02:10:13',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Tropical Juice',
                'slug' => 'tropical-juice',
                'created_at' => '2022-02-03 02:44:53',
                'updated_at' => '2022-02-07 02:10:09',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Mango',
                'slug' => 'mango',
                'created_at' => '2022-02-03 02:45:16',
                'updated_at' => '2022-02-07 02:10:05',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Hok Vicheka',
                'slug' => 'hok-vicheka',
                'created_at' => '2022-02-10 08:22:53',
                'updated_at' => '2022-02-10 08:22:56',
                'deleted_at' => '2022-02-10 08:22:56',
            ),
        ));
        
        
    }
}