<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('posts')->delete();
        
        \DB::table('posts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'title' => 'Architecto Cupiditatssss Ssss',
                'slug' => 'architecto-cupiditatssss-ssss',
                'image' => '2022-02-07-620093f335b9e.jpg',
                'home_image' => '',
                'description' => '<p>fdggggggggggxxxx</p>',
                'short_description' => NULL,
                'view' => 0,
                'created_at' => '2022-02-07 03:37:23',
                'updated_at' => '2022-02-08 02:28:03',
                'deleted_at' => NULL,
                'status' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'title' => 'New Pineapple Fever!',
                'slug' => 'new-pineapple-fever!',
                'image' => '2022-02-08-6201d0a0658ee.png',
                'home_image' => '2022-02-08-6201d8c96500c.png',
                'description' => '<p>Boost is throwing a juicy disco party and you&rsquo;re invited!<br />
Get your tastebuds ready to groove to our mighty fine&rsquo; pineapple drinks!</p>

<p>You can dance with&nbsp;<strong>Foxy Lychee</strong>&nbsp;&ndash; Pineapple, lychees, tropical juice, vanilla yoghurt, sorbet &amp; ice<br />
You can jive with&nbsp;<strong>Slammin&rsquo; Pandan</strong>&nbsp;&ndash; Pineapple, mango, coconut pandan powder, mango nectar, mango yoghurt, vanilla yoghurt &amp; ice<br />
And have the time of your life with&nbsp;<strong>Piney Limey Crush with Boogie Bobbles&nbsp;<img src="https://www.boostjuice.com.au/wp-content/uploads/2021/10/vegan_symbol_50x50.png" style="height:22px; width:21px" />&nbsp;</strong>&ndash; Freshly juiced pineapple, pineapple, lime juice, tropical juice,&nbsp;mango bobbles, sorbet &amp; ice</p>

<p>Better break out your bell bottoms and shuffle your dancing feet to your closest Boost to try one or all three today before they leave the dancefloor!</p>

<p><strong><em>T&amp;C&rsquo;s/Disclaimer:</em></strong>&nbsp;<em>Available at participating Boost stores for a limited time. Subject to availability in WA and NT. Check out boostjuice.com.au for full ingredients, allergens and nutritional information.</em></p>

<p><strong><em>&nbsp;</em></strong><em>Although products marked with this symbol&nbsp;<img src="https://www.boostjuice.com.au/wp-content/uploads/2021/10/vegan_symbol_50x50.png" style="height:23px; width:23px" />&nbsp;</em><em>are made using plant-based ingredients, there may be traces of animal products present due to potential cross contamination in store or during the external manufacturing process of the ingredients used in such products.</em><br />
<a href="https://www.boostjuice.com.au/wp-content/uploads/2022/02/foxylychee.png"><img alt="" src="https://www.boostjuice.com.au/wp-content/uploads/2022/02/foxylychee.png" style="height:269px; width:206px" /></a>&nbsp;<a href="https://www.boostjuice.com.au/wp-content/uploads/2022/02/PineyLimey.png"><img alt="" src="https://www.boostjuice.com.au/wp-content/uploads/2022/02/PineyLimey.png" style="height:269px; width:198px" /></a><img alt="" src="https://www.boostjuice.com.au/wp-content/uploads/2022/02/slamminPandan.png" style="height:270px; width:210px" /></p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2022/02/Allergens.png"><img alt="" src="https://www.boostjuice.com.au/wp-content/uploads/2022/02/Allergens-250x126.png" style="height:230px; width:457px" /></a></p>',
                'short_description' => '<p>Time To Get Groovy, Baby!</p>',
                'view' => 2,
                'created_at' => '2022-02-08 02:08:32',
                'updated_at' => '2022-02-10 04:51:35',
                'deleted_at' => NULL,
                'status' => 1,
            ),
        ));
        
        
    }
}