<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductCategoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_category')->delete();
        
        \DB::table('product_category')->insert(array (
            0 => 
            array (
                'product_id' => 1,
                'category_id' => 1,
            ),
            1 => 
            array (
                'product_id' => 2,
                'category_id' => 1,
            ),
            2 => 
            array (
                'product_id' => 1,
                'category_id' => 2,
            ),
        ));
        
        
    }
}