<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductHealthBenefitTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_health_benefit')->delete();
        
        \DB::table('product_health_benefit')->insert(array (
            0 => 
            array (
                'product_id' => 1,
                'health_benefit_id' => 1,
            ),
            1 => 
            array (
                'product_id' => 2,
                'health_benefit_id' => 1,
            ),
            2 => 
            array (
                'product_id' => 2,
                'health_benefit_id' => 3,
            ),
            3 => 
            array (
                'product_id' => 1,
                'health_benefit_id' => 2,
            ),
        ));
        
        
    }
}