<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'All Berry Bang',
                'slug' => 'all-berry-bang',
                'description' => 'Per hoc minui studium suum existimans paulus, ut erat in conplicandis negotiis artifex dirus,',
                'images' => '{"main_image":"2022-02-04-61fca5c004345.png","thumbnail_image":"2022-02-04-61fca5c0055b2.png","cup_shadow_image":"2022-02-04-61fca2d23a766.png","ingredient_image":"2022-02-04-61fca2d23b545.png"}',
                'status' => 1,
                'created_at' => '2022-02-03 03:07:45',
                'updated_at' => '2022-02-10 08:39:08',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Banana Buzz',
                'slug' => 'banana-buzz',
                'description' => 'Se natos ita amant ad quoddam.Hunc novimus esse textum. cum africam pro consule regeret carthaginiensibus victus inopia iam lassatis,',
                'images' => '{"main_image":"2022-02-04-61fcaff531620.png","thumbnail_image":"2022-02-04-61fcaff531c1d.png","cup_shadow_image":"2022-02-04-61fcaff532176.png","ingredient_image":"2022-02-04-61fcaff532754.png"}',
                'status' => 1,
                'created_at' => '2022-02-04 04:47:49',
                'updated_at' => '2022-02-10 08:24:37',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}