<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PageFaqsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('page_faqs')->delete();
        
        \DB::table('page_faqs')->insert(array (
            0 => 
            array (
                'id' => 2,
                'page_id' => 4,
                'question' => 'Why register?',
                'answer' => '<p>
Ah, cos it&rsquo;s awesome? Keep your alerts on and get the low down on random challenges, freebies and discounts (because we&rsquo;re a generous bunch of coconuts). 
You can also strut your stuff past the queue and grab your pre-ordered Boost drink with our &lsquo;Order Now&rsquo; feature. 
Other deliciously enticing things that may tickle your pickle:

&bull; My Boost Points &ndash; Collect points every time you buy a Boost. Once you reach 10 points, you will receive a freebie.
&bull; Personalise &ndash; You can create the juice or smoothie of your dreams because it&rsquo;s so easy to add or delete ingredients.
&bull; Find Us &ndash; If you&rsquo;re lost, sorry, we can&rsquo;t find you, but we can point you in the direction of a Boost.
&bull; Our Menu &ndash; What&rsquo;s in our drinks? Nothing artificial that&rsquo;s for sure. You can find out all the details.
&bull; FREE birthday drink just for being born! Yay for parents!</p>',
                'created_at' => '2022-02-09 04:13:06',
                'updated_at' => '2022-02-09 04:34:08',
            ),
            1 => 
            array (
                'id' => 3,
                'page_id' => 4,
                'question' => 'What are my Boost Points?',
                'answer' => '<p>
Buy one drink, get one point. Buy two drinks, get two points. Easy peezy lemon squeezy! This can be for Kids, Medium and Original drinks, so long as you buy a drink. Points only apply for full price drinks (so not when any offer or discount is used) and not where VIBE club is excluded.
</p>',
                'created_at' => '2022-02-09 04:13:27',
                'updated_at' => '2022-02-09 04:17:26',
            ),
            2 => 
            array (
                'id' => 4,
                'page_id' => 4,
                'question' => 'Can I get a free drink yet?',
                'answer' => '<p>Got 10 or more points? Then do a happy dance because you have a free Boost! You only need 10 points to get your freebie! Free Boost is for original size only and does not include soy, double fruit or any extras. !</p>',
                'created_at' => '2022-02-09 04:17:47',
                'updated_at' => '2022-02-09 04:17:47',
            ),
            3 => 
            array (
                'id' => 5,
                'page_id' => 4,
                'question' => 'Is there a limit on points and do they expire?',
                'answer' => '<p>You can collect as many points as you want to and your points have no expiry date, so collect away and eventually you could have enough to fill your bath with Mango Magic!</p>',
                'created_at' => '2022-02-09 04:19:26',
                'updated_at' => '2022-02-09 04:19:26',
            ),
            4 => 
            array (
                'id' => 6,
                'page_id' => 12,
                'question' => 'What is the recruitment process for store based roles?',
                'answer' => '<p>We have a pretty simple 5 step process that occurs when you apply for a role with us:</p>

<p>1. Apply for a job &ndash; Hand in your resume in store or online<br />
2. Boost gets in contact &ndash; So we can learn more about you<br />
3. Face to face interview with a manager in store<br />
4. Reference checks &ndash; The manager will contact your references<br />
5. You got the job! &ndash; If you are successful you will contacted and offered a role. If you are unsuccessful you will be called and informed.</p>',
                'created_at' => '2022-02-10 05:28:21',
                'updated_at' => '2022-02-10 05:28:21',
            ),
            5 => 
            array (
                'id' => 7,
                'page_id' => 12,
                'question' => 'What do Boost Juice look for?',
                'answer' => '<p>We are looking for fun, passionate, driven people who love working with people and customers alike.</p>

<p>For team member roles no previous experience is necessary, as we will teach you how to make our amazing products.</p>

<p>For our store management roles we do however require previous experience in a similar store management role.</p>',
                'created_at' => '2022-02-10 05:28:36',
                'updated_at' => '2022-02-10 05:28:36',
            ),
            6 => 
            array (
                'id' => 8,
                'page_id' => 12,
                'question' => 'What do we do with your information we collect?',
                'answer' => '<p>All information that is collected during the recruitment process will not be used for any other purpose or disclosed to any organisation during the recruitment process, unless required by or authorised by law.</p>',
                'created_at' => '2022-02-10 05:28:51',
                'updated_at' => '2022-02-10 05:28:51',
            ),
            7 => 
            array (
                'id' => 9,
                'page_id' => 9,
                'question' => 'Is the application deposit refundable?',
                'answer' => '<p>Yes, the application deposit is fully refundable at any stage in your recruitment process. Once approved as a Boost Juice franchise partner, your deposit will cover the legal costs incurred with the creation of your franchise documentation. Please note that the $2,200 application deposit is not included in the purchase price of your store.</p>',
                'created_at' => '2022-02-10 05:54:52',
                'updated_at' => '2022-02-10 05:54:52',
            ),
            8 => 
            array (
                'id' => 10,
                'page_id' => 9,
                'question' => 'How much does your franchise opportunity cost, what does this price include and what other costs will be incurred in addition to this price?',
                'answer' => '<p>A new Boost Juice store, on average, costs between $250,000 - $350,000 + GST. This contract covers the franchise fee, fit out and design of the store, all of the plant and equipment in the store and your training. In addition to this, you will need to budget for working capital, start-up stock and local area marketing.</p>

<p>If you are interested in buying an existing Boost Juice business, you will negotiate the sale price directly with the existing owner of the store or their nominated business broker. You will need to pay the $2,200 application deposit plus the training fee ($14,000 + GST) to Boost Juice, on top of the price you negotiate with the store owner.</p>

<p>Please note that you will also need to supply a bank guarantee to the landlord (similar to a &lsquo;rental bond&rsquo; paid to the landlord at the beginning of a lease term).</p>',
                'created_at' => '2022-02-10 05:55:04',
                'updated_at' => '2022-02-10 05:55:04',
            ),
            9 => 
            array (
                'id' => 11,
                'page_id' => 9,
                'question' => 'How much working capital do I need?',
                'answer' => '<p>We recommend you seek external financial advice on this figure, however as a rough number to work with is 10% of the purchase price.</p>',
                'created_at' => '2022-02-10 05:55:24',
                'updated_at' => '2022-02-10 05:55:24',
            ),
            10 => 
            array (
                'id' => 12,
                'page_id' => 9,
                'question' => 'Do I need cash or equity towards the purchase of a franchise?',
                'answer' => '<p>Yes, you will have access to at least 50% of the purchase price of the business. You may have this in actual cash, funding from family and friends, equity in your home/ investment properties or a combination of all of these. Please be aware that we will request proof that you have access to these funds throughout the franchise recruitment process.</p>',
                'created_at' => '2022-02-10 05:55:39',
                'updated_at' => '2022-02-10 05:55:39',
            ),
        ));
        
        
    }
}