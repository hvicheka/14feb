<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'eOcambo Technology',
                'email' => 'eocambo@gmail.com',
                'email_verified_at' => '2022-02-07 02:32:55',
                'password' => '$2y$10$MlI5H3N/2NFhoX395JRYWOPbnSlvPTND/3vZxyefDsnusKfGDRwCi',
                'role' => 'admin',
                'remember_token' => 'JYygErqNGI',
                'created_at' => '2022-02-07 02:32:55',
                'updated_at' => '2022-02-07 02:32:55',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'email_verified_at' => '2022-02-07 02:32:55',
                'password' => '$2y$10$91/b1ZjJn7kf6K0awbqYsOd7yLLEqow7567FGzBvfOqEicGapvtoe',
                'role' => 'admin',
                'remember_token' => 'EwywGRJqiy',
                'created_at' => '2022-02-07 02:32:55',
                'updated_at' => '2022-02-07 02:32:55',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'User',
                'email' => 'user@gmail.com',
                'email_verified_at' => '2022-02-07 02:32:55',
                'password' => '$2y$10$JZ/U9GjKw7DDs955dDj5tOa9f150FV12urvZ0vXMKP4izdNOF3Rua',
                'role' => 'user',
                'remember_token' => 'o6henYthe3',
                'created_at' => '2022-02-07 02:32:55',
                'updated_at' => '2022-02-07 02:32:55',
            ),
        ));
        
        
    }
}