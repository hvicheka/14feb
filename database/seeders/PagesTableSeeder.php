<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Home',
                'title_image' => '2022-02-09-620367c1ed5ec.png',
                'slug' => 'home',
                'image' => '2022-02-09-62036b97e3fc2.jpg',
                'description' => '<p>description</p>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:18:43',
                'updated_at' => '2022-02-09 07:21:59',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Drink',
                'title_image' => '2022-02-08-620238092bea7.jpg',
                'slug' => 'drink',
                'image' => '2022-02-08-620238092a452.jpg',
                'description' => '<p>dddddddd</p>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:18:46',
                'updated_at' => '2022-02-10 01:33:34',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Find Us',
                'title_image' => '2022-02-09-62036be23501a.png',
                'slug' => 'find-us',
                'image' => '2022-02-09-62036be232e25.jpg',
                'description' => '<p>Do you hate waiting? Do you like things really fast? Are you becoming impatient waiting for us to make our point? Well wait no more! When you become a Vibe member, you get to cut the queue forever when you order through the App! Vibe members get a ton of free stuff and all you have to do is register.</p>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:18:49',
                'updated_at' => '2022-02-09 07:23:14',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Vibe',
                'title_image' => '2022-02-09-62032bbb4e893.png',
                'slug' => 'vibe',
                'image' => '2022-02-09-62032bbb4c8d2.jpg',
                'description' => '<p>Do you hate waiting? Do you like things really fast? Are you becoming impatient waiting for us to make our point? Well wait no more! When you become a Vibe member, you get to cut the queue forever when you order through the App! Vibe members get a ton of free stuff and all you have to do is register.</p>',
                'terms' => '<ol>
<li>By registering and/or using your Boost VIBE Card, you consent to the terms of this agreement.</li>
<li>VIBE Points will be awarded to customers for full price smoothies, crushes or freshly squeezed juice products only, unless expressly specified. Points will not be awarded in association with any other offer or discount. Point balances are not transferable or redeemable for cash.</li>
<li>While every effort will be made in store to ensure VIBE Points are correctly awarded, Boost Juice Bars takes no responsibility for technical errors associated with crediting points onto the VIBE Card.</li>
<li>The Boost VIBE Card should be treated like cash. Unless otherwise required by law or permitted by this agreement, any points or cash balance on your VIBE Card is non-refundable and may not be redeemed for cash. No interest or any other earnings on funds deposited to a VIBE card will accrue, be paid or be credited to you by Boost Juice Bars.</li>
<li>In the case of a lost or stolen Vibe Card; Vibe Members should visit a Boost Juice Bar and request a new Vibe Card. They can log onto the registered members section of the Boost website (boostjuice.com.au/vibelogin) where they can Transfer their existing card information including Vibe Points and Cash Balances, to their new card by submitting their details. Every endeavour will be made to ensure that the full points balance is also transferred to the new card; however Boost Juice Bars will not be responsible for any points accrued that are lost in the transfer. Cards that are reported as lost or stolen will immediately be cancelled and will be no longer able to be used. If a Vibe Card is lost or stolen, the Vibe Member is responsible for logging onto the website (boostjuice.com.au/vibelogin) to perform a lost card process and transfer all existing Vibe Points or Vibe Cash credit to a new Vibe Card. A new card can be acquired from any Boost Juice Outlet in Australia.</li>
<li>We reserve the right not to accept a VIBE Card should we believe that the use is unauthorised, fraudulent or otherwise unlawful.</li>
<li>VIBE Members are entitled to a free Birthday Boost Juice on the day of their birthday upon presentation of their VIBE Card confirming their birth date registered on the card matches the date of visit to the store. This free Birthday Boost Juice cannot be redeemed on any other day regardless of external circumstances including but not limited to public holidays, store closures etc. This free Birthday Boost is non-transferable and cannot be exchanged for cash. Boost Juice do not take responsibility for incorrectly entered birth dates during the registration process.</li>
<li>The VIBE Card is issued to you by Boost Juice Bars. It allows you to load a dollar value between $10 and $500 to use in participating Boost Juice Bars. We reserve the right to change these amounts at any time by notifying you by email, or by changing the terms and conditions on our website.</li>
<li>No fees will be charged by Boost Juice Bars for the issue, registration, activation or use of the VIBE Card.</li>
<li>Should a password be lost or forgotten, VIBE members should go to the registered members section of the Boost website (boostjuice.com.au/vibelogin) where they can enter their email address and click on &#39;lost password&#39;. Their password will then be sent to their nominated email address.</li>
<li>Boost Juice Bars reserve the right to alter the terms and conditions of the VIBE Member program at any time by notifying members through email or website.</li>
<li>Information collected from members at the time of signing up to the VIBE Member program is subject to the privacy policy listed at boostjuice.com.au/privacy</li>
</ol>',
                'enable_faq' => 1,
                'created_at' => '2022-02-10 08:18:52',
                'updated_at' => '2022-02-09 07:03:30',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Wellbeing',
                'title_image' => '2022-02-09-62032509e3968.jpg',
                'slug' => 'wellbeing',
                'image' => '2022-02-09-62032509e1102.jpg',
                'description' => '<p>sss</p>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:18:54',
                'updated_at' => '2022-02-09 02:20:57',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'News',
                'title_image' => '2022-02-09-6203258d70764.jpg',
                'slug' => 'news',
                'image' => '2022-02-09-6203258d6eb2e.jpg',
                'description' => '<p>news</p>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:18:55',
                'updated_at' => '2022-02-09 02:23:09',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Contact Us',
                'title_image' => '2022-02-10-6204888ba2a88.png',
                'slug' => 'contact-us',
                'image' => '2022-02-10-6204888b8a2cb.png',
                'description' => '<h3>Let Us Know How We May Help You</h3>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:18:56',
                'updated_at' => '2022-02-10 03:37:47',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'About Us',
                'title_image' => '2022-02-08-62023fd310c99.png',
                'slug' => 'about-us',
                'image' => '2022-02-08-62023fc799a2a.jpg',
                'description' => '<p>How close am I to putting a Boost in my belly? Just search your address using the little search bar thingy below. Easy.</p>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:18:56',
                'updated_at' => '2022-02-08 10:02:59',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Franchising',
                'title_image' => '2022-02-10-62049bb03dbc5.png',
                'slug' => 'franchising',
                'image' => '2022-02-10-62049bb03c496.jpg',
                'description' => '<p>Boost Juice is seeking enthusiastic individuals who are passionate about tasty, healthy food options and who live and breathe a &lsquo;Love LIfe&rsquo; attitude! As a Franchise Partner at Boost, you will be supported by a dedicated team of experienced professionals and proven systems and processes to help you realise your business goals. Take the steps to join one of Australia&rsquo;s most loved brands today!</p>

<p>Talk to our Franchising team<br />
for more information.</p>

<p><a href="tel:+61395084409">(03) 9508 4409</a></p>',
                'terms' => NULL,
                'enable_faq' => 1,
                'created_at' => '2022-02-10 08:18:58',
                'updated_at' => '2022-02-10 04:59:28',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Term And Condition',
                'title_image' => '2022-02-10-62046a7768a2a.png',
                'slug' => 'term-and-condition',
                'image' => '2022-02-10-620471a61b2b6.jpg',
                'description' => '<h3>Encyclopedia of Fruit Disclaimer</h3>

<p>The information on this website was gathered collaboratively and voluntarily by monkeys wearing large top hats.</p>

<p>If you would like to make a contribution, or believe an article to be in error, you may contact one of our chimps at&nbsp;<a href="http://14feb.test/info@eocambo.com">info@eocambo.com</a></p>

<p>Be advised that nothing found here has necessarily been reviewed by humans with the expertise required to provide you with complete, accurate or reliable information. Boost Juice Bars cannot guarantee the validity of the information found here.</p>

<p>Any of the trademarks, service marks, collective marks, design rights, personality rights or similar rights that are mentioned, used or cited in the articles of the Encyclopedia of Fruit are the property of their respective owners.</p>

<p>Unless otherwise stated, Boost Juice Bars are neither endorsed nor affiliated with any of the holders of any such rights and as such Boost cannot grant any rights to use any otherwise protected materials. Your use of any such or similar incorporeal property is at your own risk.</p>

<h3>not professional advice</h3>

<p>Our facts and claims should not be deemed as professional advice. If you seek professional advice, please seek the advises of a professional<br />
who is licensed or knowledgeable in the area you seek.</p>

<h3>website disclaimer</h3>

<p>1.The materials displayed on this website, including without limitation all editorial materials, information, photographs, illustrations, artwork and other graphic materials, and names, logos and trade marks, are unless otherwise stated, the property of Boost Juice Bars and are protected by copyright, trade mark and other intellectual property laws. Any such content may be displayed and printed solely for your personal, non-commercial use within your organisation only. You agree not to reproduce, retransmit, distribute, disseminate, sell, publish, broadcast or circulate any such material to any third party without the express prior written consent of Boost Juice Bars.</p>

<p>Save for the above, Boost Juice Bars does not grant any license or right in, or assign all or part of, its intellectual property rights in the content or applications incorporated into the website or in the user interface of the website.</p>

<p>2. You acknowledge and accept that the information contained on this website (the website information) may include technical inaccuracies. You acknowledge and accept that the website information is subject to change at any time and may not necessarily be up to date or accurate at the time you view it. You should enquire with us directly to ensure the accuracy and currency of the material you seek to rely upon.</p>

<p>3. To the extent permitted by law Boost Juice Bars disclaims all warranties with regard to the information and applications contained on the website and your use of the website. In no event shall Boost Juice Bars, its contractors, agents, franchisees or employees be liable for any damages whatsoever (including special, indirect or consequential) arising out of or in connection with the use or performance of the website whether in contract, at common law or in equity, or on any other basis. These Terms and Conditions do not exclude warranties or conditions which Boost Juice Bars cannot, by law, exclude. These Terms and Conditions do not exclude any liability which any law requires Boost Juice Bars to accept.</p>

<p>4. If you wish to establish a&nbsp;<a href="http://www.boostjuicebars.com.au/" target="_blank">link to this website</a>&nbsp;you must, in the first instance, use the Contact Us Link at the bottom of the page and provide the following information:</p>

<ul>
<li>(i) the URL of the website that you seek to establish a link from</li>
<li>(ii) a brief description of your website</li>
<li>(iii) the reason that you wish to establish a link.</li>
</ul>

<p>If Boost Juice Bars agrees to your proposed link, you must comply with any terms and conditions imposed by Boost Juice Bars as a condition of such agreement.&nbsp; If the nature and/or content of your website changes in any significant way, you must contact Boost Juice Bars and provide a new description of your website.</p>

<p>5. This website may not be accessible from time to time, including inability to obtain or shortage of necessary materials, equipment facilities, power or telecommunications, lack of telecommunications equipment or facilities and failure of information technology or telecommunications equipment or facilities.</p>

<p>6. You must not attempt to change, add to, remove, deface, hack or otherwise interfere with this website or any material or content displayed on this website.</p>

<p>7. These Terms and Conditions are governed by and construed in accordance with the State of Victoria, Australia. In the event that a dispute arises from these terms and Conditions, you agree to submit to the non-exclusive jurisdiction of the courts of Victoria, Australia.</p>

<p>For more information,&nbsp;<a href="http://14feb.test/info@eocambo.com" target="_blank">contact us</a>.</p>

<p><em>Are you seriously reading this? We don&rsquo;t know whether to congratulate you for your efforts, or make fun of you for having nothing better to do&hellip; either way you&rsquo;re here so good for you.</em></p>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:18:59',
                'updated_at' => '2022-02-10 02:00:06',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Privacy',
                'title_image' => '2022-02-10-62046a563d9d4.png',
                'slug' => 'privacy',
                'image' => '2022-02-10-620471d84912b.jpg',
                'description' => '<p><em>Boost Juice Privacy Policy Last Updated: 20 July 2018</em></p>

<h3>1. Why do we have a Privacy Policy?</h3>

<p><strong>Summary:</strong><br />
When you interact with Boost Juice, you trust us with your information and expect us to protect and use it appropriately. We take privacy seriously and are committed to keeping that trust. That starts with helping you understand our privacy practices.<br />
This policy describes the information we collect, how it is used and shared, and your choices regarding this information.</p>

<p>Boost Juice Pty Ltd ACN 092 165 681 and its related entities (collectively referred to as &ldquo;we&rdquo;, &ldquo;us&rdquo;, &ldquo;our&rdquo; or &ldquo;Boost Juice&rdquo;) are committed to protecting the privacy of Personal Information in accordance with the Australian Privacy Principles contained in the Privacy Act 1988 (Cth) (Act) or, where applicable, other data protection laws (such as those in force in the European Union (EU). This Privacy Policy sets out the way we handle Personal Information and includes information on how we collect, use, disclose, keep secure and provide you with access to your Personal Information, whether provided to us via our stores, head office, the Boost Juice website, the Boost Juice smart device application including the VIBE club or other means. This policy applies to users of Boost Juice&rsquo;s services anywhere in the world, unless covered by a separate privacy policy.</p>

<h3>2. What Personal Information do we collect?</h3>

<p>Examples of Personal Information we may collect from you includes your name, address, date of birth (age), gender, home address, email address, telephone number, occupation, menu preferences, previous order information, IP address, credit card details, individual interests, preferences, tastes, purchasing behaviour, education details, employment history and other information relating to your work experience. We may also collect an opinion (including information or an opinion forming part of a database, whether true or not, and whether recorded in a material form or not, about an individual whose identity is apparent, or can reasonably be ascertained from the information or opinion). Those are just a few examples so please review this Privacy Policy closely and let us know if you have any queries.</p>

<p>We do not collect Sensitive Personal Information, which includes information about racial or ethnic groups or religious beliefs, without your express consent or as otherwise required by law. The exception to this is when you apply for a position, or to become a franchisee, in which case refer to the sections below titled &ldquo;Employment Applications&rdquo; and &ldquo;Franchise Applications&rdquo;. We will not use Sensitive Personal Information for marketing purposes.</p>

<h3>3. How do we collect Personal Information?</h3>

<p><strong>Summary:</strong><br />
The term &ldquo;Personal Information&rdquo; means any information that can be used to personally identify you.</p>

<p><strong>Summary:</strong><br />
We collect your Personal Information in a number of ways. We may collect your Personal Information directly from you or in the course of our dealings with you, for example:</p>

<ul>
<li>
<p>Information that you provide to us, such as your name</p>
</li>
<li>
<p>Information when you use our services, such as your purchase history</p>
</li>
</ul>

<p>We will only collect Personal Information from you that we reasonably require for one or more of our business functions or activities and will do so in accordance with the Australian Privacy Principles.</p>

<p>The kinds of Personal Information we may collect from you will depend on what type of interaction you have with us. We may collect Personal Information from you in the following ways:</p>

<ul>
<li>
<p>When you participate in a promotion, competition, promotional activity, survey, market research or customer behavioural activity</p>
</li>
<li>
<p>When you subscribe to our mailing list or interact or follow our social media pages including Facebook, Twitter and Instagram</p>
</li>
<li>
<p>When you download or use a Boost Juice App such as VIBE</p>
</li>
<li>
<p>We collect information about how you interact with our services. This includes information such as access dates and times, app features or pages viewed, app crashes and other system activity</p>
</li>
<li>
<p>When you provide us with Personal Information via our stores or head office or when you place an order in store, by telephone or via our mobile or electronic ordering system</p>
</li>
<li>
<p>If you use social media, any information that you allow the social media site to share with us</p>
</li>
<li>
<p>If you provide feedback and opinions about our products and services through any channel</p>
</li>
<li>
<p>If you contact our Support Centre we may record calls in and out of our Support Centre and so we will collect any Personal Information you provide during the call</p>
</li>
<li>
<p>If you are applying for a position with Boost Juice, we will collect information as outlined in the section below titled &ldquo;Employment Applications&rdquo;</p>
</li>
<li>
<p>If you are applying to become a Boost Juice franchisee, we will collect information as outlined in the section below titled &ldquo;Franchise Applications&rdquo;</p>
</li>
<li>
<p>By accessing a website or mobile application via links in an email we have sent and/or by accessing a website or mobile application where you have identified yourself, you consent to the collection of such information where it is Personal Information. We may combine your anonymous or personal visitor session information or other information collected through tracking technologies with other Personal Information collected from you from time to time in order to understand and measure your online experiences and to determine what products, promotions and services are likely to be of interest to you.</p>
</li>
<li>
<p>Depending on the Boost Juice services that you use, and your app settings or device permissions, we may collect your precise or approximate location information as determined through data such as GPS, IP address and WiFi.</p>
</li>
<li>
<p>We may collect transaction details related to your use of our services, including your order details, date and time the service was provided, amount charged, and payment method</p>
</li>
<li>
<p>We may also collect Personal Information through cookies and other identification technologies, please refer to &ldquo;Cookies and Web Beacons&rdquo; section below.</p>
</li>
</ul>

<ul>
<li>
<p>Public databases</p>
</li>
<li>
<p>If you use our &lsquo;store locator&rsquo; feature, you can agree to share your location so that you can easily find the nearest store. If you use the store locator feature on our website(s) the data is cached (temporarily stored) and then trashed.</p>
</li>
<li>
<p>If you use the Boost VIBE app and have location services enabled, your most recent location each time you open the app will be cached. Previous locations are trashed, but each app open is stored.</p>
</li>
<li>
<p>We may use CCTV in or around Boost Juice businesses to investigate any type of incident and protect our personnel. We will restrict access to any footage.</p>
</li>
</ul>

<p>Boost Juice may combine the information collected from these sources with other information in its possession.</p>

<p>In some circumstances, we may collect Personal Information from third parties such as credit reporting agencies or marketing agencies. Where we do, we will ensure that we act in accordance with relevant Australian laws.</p>

<h3>4. Cookies and web beacons</h3>

<p>As you navigate through our websites or smart device applications, certain information can be passively collected (by us or our vendors) using various technologies such as cookies, internet tags or web beacons, and navigational data collection (log files, server logs and clickstream data). Using &ldquo;cookies&rdquo; enables us to collect data regarding your personal preferences, including what products you have ordered, at what times and in what amounts, the pages you have visited in what order and so forth. This enables us to recognise you as a specific customer and to help us and our third-party service providers present targeted and customised advertising to you. For example, we can keep track of the drinks you like such that if you consent, we can send you information about new products and promotions that you might be interested in.</p>

<p>If you do not wish to receive cookies, you can change your browser settings such that your device does not accept cookies or please contact us. However, please note that certain areas of the Boost Juice website or Boost Juice app may only be accessed in conjunction with cookies.</p>

<h3>5. Third-party links</h3>

<p>The websites may include links to third-party websites, plug-ins and applications. Clicking on those links or enabling those connections may allow third parties to collect or share data about you. We do not control, endorse or make representations about these third-party sites and are not responsible for their privacy statements. When you leave our websites, we encourage you to read the privacy notice of every website you visit. If you choose to give Personal Information to unrelated third parties it will not be covered by this Privacy Policy.</p>

<p><strong>Summary:</strong><br />
Boost Juice and its partners may use cookies and other identification technologies on our apps, websites, emails, and online ads for purposes described in this policy.<br />
A &ldquo;cookie&rdquo; is a small text file that is stored on your browser or device that can identify you as a unique customer and store your personal preferences.</p>

<p>On our online services, we may also have providers of other services like apps, tools, widgets and plug-ins (for example Instagram &ldquo;Like&rdquo; features), which may also use automated methods to collect information about how you use these features. These organisations may use your information in line with their own policies.</p>

<h3>6. Employment applications</h3>

<p>We collect Personal Information when you interact with us with regards to employment.</p>

<p>Examples of when we collect Personal Information includes when you apply for a position with us, complete an employment form for us, contact our Support Centre in respect to an employment opportunity, visit our website, social media channels or mobile applications.</p>

<p>In considering your application, we may also obtain Personal Information about you from third parties. For example, from your previous employers, recruitment agencies, nominated referees, government agencies and regulatory authorities. Subject to your consent, we may also collect Sensitive Personal Information about you such as information about your health (including any disability), any criminal record, if it is relevant to the role that you are applying for, and whether you identify as Aboriginal or Torres Strait Islander.</p>

<p>We collect Personal Information for a variety of purposes, including but not limited to any one or more of the following purposes: verifying your identity, communicating with you, assessing you for a position with us or one of our related entities and storing your information for future employment opportunities.</p>

<p>If you do not provide us with the information we request, we will be unable to do one or more of the above.</p>

<p>We may disclose your information to; referees or previous employers, recruitment agencies or agencies or contractors acting on our behalf, our related entities, law enforcement agencies to verify whether you have a criminal record or educational organisations to the extent necessary to verify your qualifications.</p>

<p>If we engage third party contractors to perform services for us which involves handling Personal Information, we will take reasonable steps to prevent the contractor from using the Personal Information except for the purpose for which it was supplied.</p>

<p>We will hold your Personal Information for future job opportunities with us, unless you tell us not to.</p>

<h3>7. Franchise applications</h3>

<p>We collect Personal Information about franchise applicants. This may include Personal information about residency status, employment history, financial capacity (including bank account information) and other information relating to your qualifications and experience.</p>

<p>Examples of when we collect Personal Information includes when you apply to be a franchisee with us, complete a form for us (Boost Juice Franchise Application Form), contact our Support Centre, visit our website, social media channels or mobile applications or otherwise provide Personal Information to us.</p>

<p>We may combine and compare Personal Information that we hold about you with other information collected from, or held by, others.</p>

<p>In considering your application, we may also obtain Personal Information about you from third parties, for example, from your previous employers, business contacts or nominated<br />
referees. Subject to your consent, we may also collect Sensitive Personal Information about you such as information about any criminal record you may have or your membership of any professional or trade association.</p>

<p>We collect Personal Information for any one or more of the following purposes:</p>

<ul>
<li>
<p>assessing your application to become a Boost Juice franchisee;</p>
</li>
<li>
<p>assessing whether you are suitable to progress to each stage of the Boost Juice franchise recruitment process; and/or</p>
</li>
<li>
<p>storing your information for future Boost Juice franchise opportunities.</p>
</li>
</ul>

<p>If you do not provide us with the information we request, we will be unable to do one or more of the above.</p>

<p>We may disclose your information to; referees or previous employers, recruitment agencies or agencies or contractors acting on our behalf, our related entities, law enforcement agencies to verify whether you have a criminal record or educational organisations to the extent necessary to verify your qualifications.</p>

<p>If we engage third party contractors to perform services for us which involves handling Personal Information, we will take reasonable steps to prevent the contractor from using the Personal Information except for the purpose for which it was supplied.</p>

<h3>8. Franchisees</h3>

<p>A number of Boost Juice businesses are owned and operated by franchisees. Boost Juice&rsquo;s franchisees are required to follow this privacy policy. If you are concerned that there may have been a breach of this privacy policy by a franchisee, please contact the relevant franchisee entity or Boost Juice directly.</p>

<p>This privacy policy does not apply to our franchisee&rsquo;s conduct on websites or mobile apps they operate.</p>

<h3>9. How we store your Personal Information</h3>

<p>Where we store your Personal Information depends on what interaction you have had with us. However, some areas may include databases for processing customer enquiries, feedback or for general data processing or databases for email or SMS marketing communications.</p>

<h3>10. Information retention and deletion</h3>

<p><strong>Summary:</strong><br />
Boost Juice retains user profile and other information for as long as you maintain your VIBE club membership or other Boost Juice account.<br />
If you withdraw consent to the collection or use of Personal Information, Boost Juice deletes such Personal Information.</p>

<p>Users may request deletion of their accounts at any time. Following such request, Boost Juice deletes the information.</p>

<p>We only keep your Personal Information for as long as it is required for the purpose for which it was collected or as otherwise required by law. We will take appropriate measures to destroy or permanently de-identify your Personal Information if we no longer need to retain it. These measures may vary depending on the type of information concerned, the way it was collected and how it was stored.</p>

<p>There may be circumstances relating to employment or franchise applications or inappropriate or legal conduct which prevent Boost Juice from permanently deleting Personal Information. For example, in the case of fraud or where litigation has been threatened.</p>

<h3>11. How your Personal Information will be used</h3>

<p>We collect your Personal Information so that we can use it for our functions and activities and provide products and services to you, which include, among other things:</p>

<p>Providing services and features</p>

<p>Boost Juice uses the information we collect to provide, personalise, maintain and improve our products and services. This includes using the information to:</p>

<ul>
<li>
<p>Create and update your account</p>
</li>
<li>
<p>Verify your identity</p>
</li>
<li>
<p>To assist us in providing goods and services to you</p>
</li>
<li>
<p>processing orders you place with us in our stores or via our electronic or mobile ordering system, providing you with email or SMS confirmation of your orders, providing you with our products and processing refunds where applicable;</p>
</li>
<li>
<p>Perform internal operations necessary to provide our goods and services, including to troubleshoot software bugs and operational problems, to conduct data analysis, testing, and research, and to monitor and analyse usage and activity trends</p>
</li>
<li>
<p>features to personalise your experience on our Boost Juice platforms</p>
</li>
<li>
<p>to identify services and products that may be of interest to you</p>
</li>
<li>
<p>assisting you with remembering and re-ordering from our menu in future</p>
</li>
<li>
<p>administering and conducting traineeships</p>
</li>
<li>
<p>facilitating our internal business operations, including fulfilment of any legal and regulatory requirements</p>
</li>
<li>
<p>to facilitate the &ldquo;5 star&rdquo; feedback program</p>
</li>
<li>
<p>processing and considering your employment application and conducting reference checks (refer to Employment Application section above)</p>
</li>
<li>
<p>providing information that you request about our franchises and processing and considering your franchise application and conducting record checks (refer to Franchise Application section above)</p>
</li>
</ul>

<p><strong>Summary:</strong><br />
Boost Juice does not sell your Personal Information to third parties.</p>

<p>Facebook Custom Audiences: If you use Facebook, we may use your email address in an encrypted format to match with your Facebook profile so that we can provide you with personalised advertising on Facebook. This is subject to the privacy settings you have chosen on Facebook.</p>

<p>Facebook Lookalike Audiences: If you use Facebook, we may use your email address in an encrypted format to enable Facebook to find other registered users of their services that share similar interests to you based on how you interact with Boost Juice and what information Facebook has about you. This is subject to the privacy settings you have chosen on Facebook.</p>

<p>Customer support<br />
Boost Juice uses the information we collect to provide customer support services, including to:</p>

<ul>
<li>
<p>Direct your questions to the appropriate customer support person</p>
</li>
<li>
<p>Investigate and address your concerns</p>
</li>
<li>
<p>Monitor and improve our customer support responses</p>
</li>
<li>
<p>administering and responding to your enquiry or feedback about our products</p>
</li>
<li>
<p>to process and respond to any feedback provided by you</p>
</li>
</ul>

<p>Legal proceedings and requirements</p>

<p>Boost Juice may share your information if we believe it is required by applicable law, regulation, operating agreement, legal process or governmental request, or where the disclosure is otherwise appropriate due to safety or similar concerns:</p>

<p>Communications from Boost Juice<br />
Boost Juice may use the information we collect to communicate with you about:</p>

<ul>
<li>
<p>promoting and marketing current and future products, services, promotions, studies, surveys, news, updates and events</p>
</li>
<li>
<p>conducting, and allowing you to participate in, a promotion, competition, promotional activity, survey or market research; fulfil any related awards, and serve you relevant ads and content about our goods or services and those of our business partners</p>
</li>
<li>
<p>special promotions and offers and analysing our products and services so as to improve and develop new products and services.</p>
</li>
</ul>

<p>This communication (including direct marketing materials) may take place via: email, text messages, other digital services (such as through our VIBE app), phone, internet-based marketing including targeted online advertising and online behavioural marketing.</p>

<p>You consent to us sending you those direct marketing communications by any of those methods.</p>

<p>You may receive some of these communications based on your profile as a Boost Juice Vibe user. All users (including those in the EU) have the right to object to this type of processing. You can contact us at any time if you no longer wish to receive marketing materials from us or our related entities.</p>

<p><strong>With your consent</strong>&nbsp;: Boost Juice may share your information other than as described in this policy if we notify you and you consent to the sharing.</p>

<p><strong>Vendors</strong></p>

<p>We may share your Personal Information with vendors who provide services to us, such as fulfilling orders (e.g. Deliveroo), carrying out research (data processing), managing promotions, personalising customer experiences and other information technology services. We do not allow these vendors to use this information or to share it for any purpose other than to provide services on our behalf.</p>

<h3>12. Marketing Opt-Outs</h3>

<p>At any time, you may opt-out of receiving direct marketing communications from us or our related entities. We will then ensure your details are removed from our mailing lists and stop sending direct marketing communications to you within a reasonable period after your request is made (usually within 5 business days from receiving your request). To opt-out follow the instructions in the marketing communications we send you or contact us.</p>

<p>Please note that if you opt out, we may still send you non-promotional messages, such as receipts or information about your account.</p>

<h3>13. Disclosure of Personal Information to third parties</h3>

<p>We may disclose Personal Information we collect from you:</p>

<ul>
<li>
<p>to our related entities, suppliers, consultants, contractors or agents for the purposes set out in the section above or for other purposes directly related to the purpose for which the Personal Information is collected</p>
</li>
<li>
<p>to relevant federal, state and territory authorities for the purpose of investigating a food safety or health issue</p>
</li>
<li>
<p>to Boost Juice franchisee partners if you have made an enquiry or complaint relating to a store that is owned and operated by one of our franchisees</p>
</li>
<li>
<p>where the law requires or authorises us to do so (whether in Australia or overseas)</p>
</li>
<li>
<p>to others that you have been informed of at the time any Personal Information is collected from you</p>
</li>
<li>
<p>with your consent (express or implied) to others</p>
</li>
</ul>

<h3>14. Overseas disclosures</h3>

<p>We are based in Australia, so your Personal Information will be processed in Australia. However, some of our related entities and third-party goods and services providers are located overseas. We may need to share your Personal Information with organisations or persons located outside of Australia.</p>

<p>The countries in which these organisations or persons are located will depend on the circumstances, but in the course of our ordinary operations, we may disclose Personal Information to our third- party service providers located in the United States, United Kingdom and Italy.&nbsp;</p>

<p>If we disclose Personal Information to a third party in a country which does not have equivalent privacy laws to Australia, we will take steps to ensure that you are provided with appropriate safeguards in respect of your Personal Information. This might include entering into contractual clauses with the third-party that place obligations on the third-party in relation to their handling your Personal Information.</p>

<p>Whilst Boost Juice will take all reasonable care to ensure that overseas providers will handle your Personal Information securely, you acknowledge that by agreeing to the disclosure of your Personal Information to these overseas entities for the purposes stated above, Boost Juice will not be held accountable for any breaches of the Australian Privacy Principles by an overseas recipient.</p>

<h3>15. Security</h3>

<p>We take protecting your Personal Information seriously and are continuously developing our security systems and processes. We take reasonable steps to protect your Personal Information from misuse and loss and unauthorised access, modification or disclosure. We do this by having physical and electronic security systems and by limiting who can access your Personal Information. We also have online and network security systems in place for our websites, so that the information you provide us online is protected and secure. Your Personal Information may be held in electronic or hard copy form. We use third party service providers to store some personal information.</p>

<p>As our website and smart device application are linked to the internet which is inherently insecure, Boost Juice cannot provide any assurance regarding the security of the information you communicate to us online. We cannot guarantee that such information will not be intercepted so you transmit such information at your own risk. Please contact us if you become aware of, or suspect, any breach of security.</p>

<p>If we become aware of a data breach that is likely to result in serious harm (for example, your Personal Information was shared with unauthorised persons) we will notify you as soon as is practicable and the Office of the Australian Information Commissioner about that actual or possible breach.</p>

<h3>16. Access to Personal Information and Correction</h3>

<p>We aim to ensure that any information we obtain from you is accurate. You can request access to your Personal Information or advise us of any required correction and information which you believe to be inaccurate by forwarding a request to privacy@boostjuicebars.com.</p>

<p>We will require you to verify your identity and to specify what information you require. We may charge a fee to cover the cost of verifying the application and locating, retrieving, reviewing and copying any material requested. If the information sought is extensive, we will advise the likely cost in advance and can help to refine your request if required.</p>

<p>The exceptions under the Australian Privacy Principles which affect your right to access your Personal Information we hold about you include the following:</p>

<ul>
<li>
<p>access would pose a serious threat to the life or health of any individual</p>
</li>
<li>
<p>access would have an unreasonable impact on the privacy of others</p>
</li>
<li>
<p>a frivolous or vexatious request</p>
</li>
<li>
<p>the information relates to a commercially sensitive decision-making process</p>
</li>
<li>
<p>access would be unlawful or denying access is required or authorised by law</p>
</li>
<li>
<p>access would prejudice enforcement activities relating to criminal activities and other breaches of law, public revenue, a security function, or negotiations with the individual</p>
</li>
<li>
<p>legal dispute resolution proceedings.</p>
</li>
</ul>

<h3>17. Contact Us</h3>

<p>Although we are committed to ensuring that at all times there is strict adherence to the Australian Privacy Principles, in the event that you are dissatisfied in any way with the collection, use, storage and/or disclosure of your Personal Information or believe that there has been a breach of<br />
the Australian Privacy Principles, we invite you to contact us as soon as possible. We have appointed an internal Privacy Complaints Manager (who is also our Privacy Officer) to whom your complaint or any queries may be directed by:</p>

<p>E-mail at privacy@boostjuicebars.com Calling us on (03) 9508 4400<br />
Writing to us at the below address:</p>

<p>Attention: Privacy Officer<br />
Retail Zoo Legal Department Level 1, Tower 2, Chadstone Place 1341 Dandenong Road Chadstone, VIC 3148</p>

<p>If our Privacy Policies do not answer a question that you have about how we manage your Personal Information, please call or email us.</p>

<p>All complaints are to be made in writing to Boost Juice and Boost Juice is then allowed a reasonable time (within 30 days) to respond to complaints. We may ask you to participate in a dispute resolution scheme (such as mediation) in order to resolve your complaint.</p>

<p>If you are not satisfied with our response, you can write to &ldquo;Privacy Complaints Manager, Retail Zoo Legal Department, Level 1, Tower 2, Chadstone Place, 1341 Dandenong Road, Chadstone, Victoria, 3148&prime;&prime;. If you are not satisfied with the manner in which we deal with your complaint, you may refer it to the Office of the Australian Information Commissioner (&ldquo;OAIC&ldquo;). You can contact OAIC by:</p>

<ul>
<li>
<p>visiting www.oaic.gov.au;</p>
</li>
<li>
<p>forwarding an email to enquiries@oaic.gov.au;</p>
</li>
<li>
<p>telephoning 1300 363 992; or</p>
</li>
<li>
<p>writing to OAIC at GPO Box 5218, Sydney NSW 2001.</p>
</li>
</ul>

<h3>18. Changes to this Privacy Policy</h3>

<p>We may occasionally make changes to this Privacy Policy. We will post any changes to this privacy policy on our websites and applications. When we make material changes to this Privacy Policy, we will provide you with notice pursuant to applicable laws.</p>

<p>To the extent permitted under applicable law, by using our services after such notice, you consent to our updates to this policy.&nbsp;</p>

<p>We encourage you to periodically review this policy for the latest information on our privacy practices.</p>

<h3>19. Boost Juice subsidiaries and affiliates</h3>

<p>We share information with our subsidiaries and affiliates to help us provide our services or conduct data processing on their behalf. For example, Boost Juice Australia processes and stores Personal Information of EU customers in Australia on behalf of its master franchisee partner(s) in the EU.</p>

<p>Our Boost Juice has master franchisees who are owner by independent business owners to Boost Juice Australia and are data controllers. As a result, each Boost Juice master franchisee are responsible for their own data protection compliance.</p>

<p>If you live in the EU (including England) and interact with Boost Juice, please review our &ldquo;special information for European users&rdquo; below or to visit your countries Boost Juice website.</p>

<h3>20. Special information for European Union users</h3>

<p><strong>Summary:</strong><br />
The European Union General Data Protection Regulation (the GDPR) contains data protection requirements that apply from 25 May 2018.</p>

<p>The following section sets out Boost Juice&rsquo;s policies for processing Personal Information (referred to as &ldquo;personal data&rdquo; in the GDPR) of customers in the EU as a data processor.</p>

<p>If you are a resident of the EU and use any Boost Juice services in the EU, then in addition to the Boost Juice Privacy Policy, the following applies to you.</p>

<p>Data controller</p>

<p>The data controller for the information you provide or that is collected by Boost Juice or its affiliates is the Boost Juice entity in the jurisdiction where your personal information is collected.</p>

<p>In the United Kingdom the data controller is:</p>

<p>TD4 LTD Company Registration 05761251<br />
Boost Juice Bars (UK) Ltd.<br />
Privacy Officer<br />
Email: boost@boostjuicebars.co.uk<br />
Address: Abbots Moss Hall, The Pavilion, Oakmere, CW8 2ES, United Kingdom</p>

<p>In Estonia the data controller is:</p>

<p>Indrek Sei<br />
ID 37207260220<br />
Privacy Officer<br />
Megamahlad Holding OÜ<br />
Address: Külma 19 Tallinn 10618 Estonia E-mail: indrek.sei@boostjuicebars.ee</p>

<p>Phone: +3725011116 In Latvia the data controller is:</p>

<p>Indrek Sei<br />
ID 37207260220<br />
Privacy Officer<br />
Megamahlad Holding OÜ<br />
Address: Külma 19 Tallinn 10618 Estonia E-mail: indrek.sei@boostjuicebars.ee Phone: +3725011116</p>

<p>Questions, comments and complaints about Boost Juice&rsquo;s data practices with EU Personal Information can be submitted to Boost Juice&rsquo;s data controller.</p>

<p>What EU Boost Juice Personal Information is collected?</p>

<p>The kinds of Personal Information the Boost Juice EU master franchisee(s) may collect from EU customers/ users will depend on what type of interaction they have with Boost Juice in the EU. The information Boost Juice Australia in turns received from its EU master franchisees however, may include, among other things:</p>

<p>Fixed or automatic fields of collected data:</p>

<ul>
<li>
<p>Name</p>
</li>
<li>
<p>Email address</p>
</li>
<li>
<p>Gender</p>
</li>
<li>
<p>Date of Birth</p>
</li>
<li>
<p>Loyalty program member number</p>
</li>
<li>
<p>Registration date</p>
</li>
<li>
<p>Last update</p>
</li>
<li>
<p>Verification date</p>
</li>
<li>
<p>Issuing Store ID</p>
</li>
<li>
<p>Card type</p>
</li>
<li>
<p>Last item Option data collected:</p>
</li>
</ul>

<ul>
<li>
<ul>
<li>
<p>Mobile number</p>
</li>
<li>
<p>Physical address</p>
</li>
<li>
<p>Favourite item</p>
</li>
</ul>
</li>
</ul>

<p>How that data is collected</p>

<p>EU Personal Information (including England) is collected by Boost Juice master franchisee(s) and their associates in accordance with their own Privacy Policy located at:</p>

<ul>
<li>
<p>United Kingdom: https://www.boostjuicebars.co.uk/privacy-policy/</p>
</li>
<li>
<p>Estonia: http://boostjuicebars.ee/</p>
</li>
<li>
<p>Latvia: http://boostjuicebars.lv/</p>
</li>
</ul>

<p>Boost Juice master franchisees may engage third party contractors to perform services for them and us which involves handling Personal Information, in which case we take reasonable steps to prevent the contractor from using the Personal Information except for the purpose for which it was supplied.</p>

<p><strong>Why is the data collected and how do we use the Personal Information?</strong></p>

<p>Boost Juice collects EU Personal Information as a processor on behalf of its EU master franchisee(s). Boost Juice uses the data to run reports solely for the purpose of providing these reports to its master franchisee(s). These reports profile customers (amongst other things) in terms of sales data, registrations, redemptions, engagements/ loyalty, age and gender. Boost Juice does not use the reports or Personal Information for any purpose independent of providing the reports to its master franchisee(s).</p>

<p><strong>EU Boost Juice customer rights</strong></p>

<p>If you are a Boost Juice VIBE user or customer in the EU, you have additional rights with respect to Boost Juice&rsquo;s handling of your Personal Information which are set out below. Please contact us if you would like to exercise any of these rights or other rights available to you under the GDPR. Users outside the EU may also request explanation, correction, deletion or copies of their Personal Information.</p>

<p>In order to comply with the request, we may ask you to verify your identity.</p>

<p>We will fulfil the request by sending your copy electronically, unless the request specified a different method.</p>

<p>i. Explanation and copies of your data</p>

<ul>
<li>You have the right to request an explanation of the information that Boost Juice has about you and how Boost Juice uses that information.</li>
<li>You also have the right to receive a copy of the information that Boost Juice collects about you</li>
<li>you have the right to have the data we hold about you transferred to another organisation</li>
</ul>

<p>ii. Correction</p>

<ul>
<li>If Boost Juice has information about you that you believe is inaccurate, you are welcome to contact us so we can update it and keep your data accurate.</li>
</ul>

<p>iii. Deletion</p>

<ul>
<li>Any data that is no longer needed for the purposes specified will be deleted and/or deidentified.</li>
<li>Users may request deletion of their Boost VIBE accounts and other Personal Information at any time. Users may request deletion via email or over the phone.</li>
</ul>

<p>iv. Objections and complaint</p>

<ul>
<li>Users in the EU have the right to object to Boost Juice&rsquo;s processing of Personal Information, including for marketing purposes based on profiling and/or automated decision making.</li>
<li>You have the right to lodge a complaint with the Information Commissioner&rsquo;s Office. Further information, including contact details, is available at https://ico.org.uk.</li>
</ul>

<p>You may also submit questions, comments or complaints to Boost Juice&rsquo;s data controller.</p>

<p>Boost Juice will make every effort to provide these right but there may be circumstances where these rights may be limited. For example, if fulfilling your request would reveal personal data about another person, present a security risk or where the information contains legal privilege.</p>

<p><strong>Grounds for processing</strong></p>

<ul>
<li>When EU customers register and use the Boost Juice VIBE platform (and other associated Boost Juice platforms), customers provide information detailed above which in turn, is provided to Boost Juice in Australia.</li>
</ul>

<ul>
<li>
<p>The GDPR requires whenever we or our master franchisee(s) process your Personal Information we have to have something called a &ldquo;legal basis&rdquo; for what we do.</p>

<p>The different grounds Boost Juice relies on for processing Personal Information are (amongst other things):</p>
</li>
</ul>

<p>i. Consent<br />
You have told us you are happy for us to process your Personal Information for a specific purpose.</p>

<p>Where processing your personal information is based on your consent you may withdraw this consent at any time (the withdrawal of the consent will not affect the lawfulness of processing based on consent before its withdrawal).</p>

<p>ii. Legitimate interests</p>

<p>The processing is necessary for us to conduct our business, but not where our interests are overridden by your interests or rights.</p>

<p>Boost Juice must collect, process and use certain information in order to provide its services to its master franchisees. This includes:</p>

<ul>
<li>
<p>To provide customer support</p>
</li>
<li>
<p>For research and analytical purposes. This includes, for example, analysing usage trends to</p>

<p>improve user experience</p>
</li>
<li>
<p>For direct marketing purposes. This includes, for example, analysing data to identify trends</p>

<p>and tailor marketing messages to user needs.</p>
</li>
</ul>

<p>iii. Performance of a contract</p>

<p>We must process your Personal Information in order to be able to provide you with one of our products or services.</p>

<p><strong>Children&rsquo;s Privacy Notice</strong></p>

<p>Our policy is not to collect Personal Information from any person under 13-years-old unless collection is for a specific activity and that person&rsquo;s parent or guardian has provided us with written consent to collect their Personal Information for that specific activity.</p>

<p>Whilst some of our Boost Juice products (e.g. smoothies) are targeted towards children, we do not intend for our online services to be used by anyone under the age of 13. If you are a child under the age of 13, PLEASE DO NOT provide any Personal Information to us. If you are a parent or guardian and believe we may have collected information about your child, please contact us.</p>

<p>Data protection officer</p>

<p>Boost Juice has appointed a Data Protection Officer to ensure we protect the Personal Information of our customers (and others) and comply with data protection legislation.</p>

<p>If you have any questions about how Boost Juice uses your Personal Information that are not answered here, or if you want to exercise your rights regarding your Personal Information, please contact our Data Protection Officer:</p>

<p>E-mail at privacy@boostjuicebars.com</p>

<p>Calling us on (03) 9508 4400<br />
Writing to us at the below address:</p>

<p>Attention: Data Protection Officer</p>

<p>Retail Zoo Legal Department Level 1, Tower 2, Chadstone Place</p>

<p>1341 Dandenong Road</p>

<p>Chadstone, VIC 3148</p>

<p><strong>International transfers of Personal Information</strong></p>

<p>EU customers data (including Personal Information) will transfer between Australia and the EU. Please refer to your countries specific privacy policy for further details about other international data transfers.</p>

<p><strong>Cookies and Other Technologies</strong><br />
A copy of Boost Juice&rsquo;s UK cookies policy can be found here.</p>

<p><strong>How long will we keep EU Personal Information for?</strong></p>

<p>We will keep your Personal Information for the purposes set out in this privacy policy and in accordance with the law and relevant regulations. We will only retain your Personal Information for as long as we need it for our legitimate interest in accordance with applicable laws. After such time, we will either delete or deidentify your Personal Information. We will your Personal Information upon request.</p>

<h3>21. Overseas</h3>

<p>If you interact with Boost Juice outside Australia and would like to contact us or for further information please do so via the below:</p>

<ul>
<li>
<p>United Kingdom at http://www.boostjuicebars.co.uk/ or on boost@boostjuicebars.co.uk</p>
</li>
<li>
<p>Estonia at http://boostjuicebars.ee/ or on info@boostjuicebars.ee</p>
</li>
<li>
<p>Latvia at http://boostjuicebars.lv/ or on info@boostjuicebars.lv</p>
</li>
<li>
<p>Brunei at http://www.boostjuicebars.com.my/ or on</p>
</li>
<li>
<p>Chile at http://www.boostjuicebars.cl/ or on</p>
</li>
<li>
<p>India at http://www.joostjuicebars.com/about-joost-juice/ or on</p>
</li>
<li>
<p>Malaysia at http://www.boostjuicebars.com.my/ or on</p>
</li>
<li>
<p>Namibia at http://boostjuice.co.za/ or on</p>
</li>
<li>
<p>New Zealand at https://www.boostjuice.co.nz/ or on queensgate@boostjuice.co.nz</p>
</li>
<li>
<p>Singapore at http://boostjuicebars.com.sg/ or on barry@boostjuicebars.com.sg</p>
</li>
<li>
<p>South Africa at http://www.boostjuice.co.za/ or on info@boostjuice.co.za</p>
</li>
<li>
<p>Taiwan at http://www.boostjuice.com.tw/ or on boostjuice@landistpe.com.tw</p>
</li>
</ul>',
                'terms' => NULL,
                'enable_faq' => 0,
                'created_at' => '2022-02-10 08:19:01',
                'updated_at' => '2022-02-10 02:00:56',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Job',
                'title_image' => '2022-02-10-62049ccbc29d3.png',
                'slug' => 'job',
                'image' => '2022-02-10-62049ccbc02de.jpg',
                'description' => '<p>We currently have over 580 stores across 13 different countries. That&rsquo;s a lot of stores, in a lot of countries, which means a lot of jobs.</p>',
                'terms' => NULL,
                'enable_faq' => 1,
                'created_at' => NULL,
                'updated_at' => '2022-02-10 05:04:11',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}