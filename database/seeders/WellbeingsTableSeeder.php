<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WellbeingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('wellbeings')->delete();
        
        \DB::table('wellbeings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Nutrition Facts 2',
                'slug' => 'nutrition-facts-2',
                'image' => '2022-02-08-6201e7bed4759.jpg',
                'description' => '<h3>Our Drinks</h3>

<p>By offering fresh, fast and healthy choices, we aim to help you feel and live better!</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2016/08/RZBM-01444-Boost-NIPS-Chia-Seeds-Edit_WEBSITE.pdf">Nutrition Information and Allergens</a><br />
<a href="https://www.boostjuice.com.au/wp-content/uploads/2016/08/RZBM-00930-Boost-Product_Secret-Smoothies-NIPs_SEPT21_FA.pdf" target="_blank">Secret Smoothies</a></p>

<h3>Boosters</h3>

<p>Give your juice or smoothie an extra kick!</p>

<p><strong>Powerpack</strong><br />
Choose any three boosters</p>

<p><strong>All greens</strong><br />
A blend of wheatgrass, alfalfa, barley grass, spinach, spirulina and broccoli plant&nbsp;powders</p>

<p><strong>Chia seeds</strong><br />
An awesome plant-based source of omega 3</p>

<p><strong>Energiser^&dagger;</strong><br />
Refresh and energise with guarana extract, ginseng extract, taurine and vitamin E (vitamin&nbsp;E&nbsp;&ndash;&nbsp;50% RDI)</p>

<p><strong>Immunity^&Dagger;&dagger; (vit A &amp; C)</strong><br />
Supports a healthy immune system with green tea extract, echinacea extract, vitamins A and C</p>

<p><strong>Protein&dagger;</strong><br />
Whey protein to support muscle growth as part of a healthy nutritious diet</p>

<p><strong>Plant Based Protein&dagger;</strong><br />
A vegan source of protein made from peas</p>

<p>&nbsp;</p>

<p><strong>Superfruit</strong><br />
With cranberry, acai berry extract, pomegranate, goji berries, it&rsquo;s packed with antioxidants!</p>

<p><strong>Vita&dagger;</strong><br />
At least 10% of your RDI of essential vitamins and minerals (vitamin A, B12, C, D, E, niacin, riboflavin, pyridoxine, pantothenic acid, thiamine, folic acid, biotin; minerals sodium phosphate, magnesium oxide, ferrous sulphate, zinc sulphate, copper gluconate, manganese sulphate and potassium&nbsp;iodide)</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2019/09/RZPSB-10718-Boost-Product_Boosters-NIPs.pdf">Boosters Fact Sheet</a></p>

<p><em>The average adult daily energy intake is 8700 kJ</em><br />
<em>Please note that sometimes some products may not be available in all stores. RDI: Recommended daily intake</em><br />
<em>^ Contains caffeine.</em><br />
<em>* For original size.</em><br />
<em>&dagger; Boosters not suitable for children under 15 years or pregnant women, except for chia seeds, all greens and superfruit.</em><br />
<em>&Dagger; Contains vitamin A &amp; C necessary for normal immune function as part of a healthy nutritious diet.</em></p>

<h3>Ingredients</h3>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2021/09/1._RZBP-01216_Boost_Product_Ingredient_NIPs_Update_AUGUST2021.pdf" target="_blank">Boost Ingredients</a></p>

<p><strong>Boost Yoghurt</strong><br />
Our smoothies contain our very own exclusive frozen yoghurt, sorbet and frozen coconut cream. Our low-fat frozen yoghurt contains the live probiotic cultures Streptococcus Thermophilus, Lactobacillus Acidophilus and Bifidobacterium.</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2016/08/RZBM-00832_Boost_Product_Frozen_Yoghurt_NIPs_2021_FEB.pdf" target="_blank">Boost Yoghurt Fact Sheet</a></p>

<p><strong>Individually Quick Frozen</strong><br />
Some of the fruit used in our Smoothies is IQF (Individually Quick Frozen). This ensures we get the fruit at its absolute optimum quality as it is snap frozen to lock in all the goodness. It also ensures we deliver a consistent product each time.</p>

<p><strong>Milk Products</strong><br />
Everyone&rsquo;s bodies (and tastebuds) are different, which is why we have a range of milk products to choose from so you don&rsquo;t miss out on your favourite milk-based smoothie. Choose from Low Fat Milk, Soy Milk, Oat Milk or Almond Milk as the base of your smoothie. Coconut Milk is also used as an ingredient in several of our drinks to add a refreshing coconut flavour.</p>

<p>Please note that the full alternative milk range may not be available in all stores.</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2019/09/RZPSB-10718-Boost-Product_Milk-Products-NIPs.pdf" target="_blank">Milk Products Fact Sheet</a></p>

<p><strong>Ambient Range</strong><br />
Just like there is no Batman without Robin, there is no smoothie without a base. The ambient range is used as the base for some of our most popular smoothies. Made using no GMO, an ambient base is just the beginning of our delicious smoothies.&nbsp;</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2021/08/RZBJ-00432_Boost_Product_Ambient_Range_NIPs_Updated_Sept_2020_2.pdf">Ambient Range Fact Sheet</a></p>

<p><strong>Protein Balls</strong><br />
Small balls packed full of yummy goodness. A delicious energy boost that is the perfect &lsquo;anytime&rsquo; snack!</p>

<p><strong>Popcorn</strong><br />
We stock popcorn for your enjoyment. Choose between &lsquo;Sea Salt&lsquo; and &lsquo;Lightly Salted, Slightly Sweet&rsquo;</p>

<p><strong>Banana Bread</strong><br />
Boost Banana Breads is amazing when toasted on a cold Winter&rsquo;s day, or is the perfect companion to a smoothie or juice to help satisfy your hunger craving when out and about or during your next lunch break. Also perfect for brekkie with a smoothie!</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2021/09/2._RZBP-01241_Boost_Snacks_NIPs_AUGUST_2021.pdf">Boost Snack Range Fact Sheet</a></p>

<p><em>Please note that the full snack range may not be available in all stores.</em></p>

<p>&nbsp;</p>

<h3>Retail Range</h3>

<p>Can&rsquo;t get to a Boost Juice Bar for the great taste of freshly squeezed juice? Well now you have no excuse &lsquo;cause Boost&rsquo;s Natural Born Juice range is here! Thick and luscious fresh based juices, high in puree.</p>

<p>Available in both 350ml and 1 Litre sizes, and you can find them at selected Coles and Woolworths,. The range comes in 4 splendid flavours.</p>

<p><strong>Energy Lift</strong><br />
pear, orange, mango, pineapple &amp; apple</p>

<p><strong>All Berry Bang</strong><br />
apple, banana, blueberry, strawberry, raspberry and mango</p>

<p><strong>Berry Happy</strong><br />
apple, pineapple, blueberry, strawberry &amp; raspberry</p>

<p><strong>Mango Tango</strong><br />
apple, mango, orange and passionfruit</p>

<p>No artificial anything<br />
No juice concentrates<br />
No preservatives<br />
No artificial colours or flavours<br />
Direct from the fruit to you!</p>',
                'short_description' => '<p>If you want to know all the facts, figures and allergy advice on our products, you&rsquo;ll find all the info you need right here.</p>',
                'view' => 4,
                'status' => 1,
                'created_at' => '2022-02-08 03:47:10',
                'updated_at' => '2022-02-08 04:46:47',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Nutrition Facts',
                'slug' => 'nutrition-facts',
                'image' => '2022-02-08-6201e7bed4759.jpg',
                'description' => '<h3>Our Drinks</h3>

<p>By offering fresh, fast and healthy choices, we aim to help you feel and live better!</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2016/08/RZBM-01444-Boost-NIPS-Chia-Seeds-Edit_WEBSITE.pdf">Nutrition Information and Allergens</a><br />
<a href="https://www.boostjuice.com.au/wp-content/uploads/2016/08/RZBM-00930-Boost-Product_Secret-Smoothies-NIPs_SEPT21_FA.pdf" target="_blank">Secret Smoothies</a></p>

<h3>Boosters</h3>

<p>Give your juice or smoothie an extra kick!</p>

<p><strong>Powerpack</strong><br />
Choose any three boosters</p>

<p><strong>All greens</strong><br />
A blend of wheatgrass, alfalfa, barley grass, spinach, spirulina and broccoli plant&nbsp;powders</p>

<p><strong>Chia seeds</strong><br />
An awesome plant-based source of omega 3</p>

<p><strong>Energiser^&dagger;</strong><br />
Refresh and energise with guarana extract, ginseng extract, taurine and vitamin E (vitamin&nbsp;E&nbsp;&ndash;&nbsp;50% RDI)</p>

<p><strong>Immunity^&Dagger;&dagger; (vit A &amp; C)</strong><br />
Supports a healthy immune system with green tea extract, echinacea extract, vitamins A and C</p>

<p><strong>Protein&dagger;</strong><br />
Whey protein to support muscle growth as part of a healthy nutritious diet</p>

<p><strong>Plant Based Protein&dagger;</strong><br />
A vegan source of protein made from peas</p>

<p>&nbsp;</p>

<p><strong>Superfruit</strong><br />
With cranberry, acai berry extract, pomegranate, goji berries, it&rsquo;s packed with antioxidants!</p>

<p><strong>Vita&dagger;</strong><br />
At least 10% of your RDI of essential vitamins and minerals (vitamin A, B12, C, D, E, niacin, riboflavin, pyridoxine, pantothenic acid, thiamine, folic acid, biotin; minerals sodium phosphate, magnesium oxide, ferrous sulphate, zinc sulphate, copper gluconate, manganese sulphate and potassium&nbsp;iodide)</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2019/09/RZPSB-10718-Boost-Product_Boosters-NIPs.pdf">Boosters Fact Sheet</a></p>

<p><em>The average adult daily energy intake is 8700 kJ</em><br />
<em>Please note that sometimes some products may not be available in all stores. RDI: Recommended daily intake</em><br />
<em>^ Contains caffeine.</em><br />
<em>* For original size.</em><br />
<em>&dagger; Boosters not suitable for children under 15 years or pregnant women, except for chia seeds, all greens and superfruit.</em><br />
<em>&Dagger; Contains vitamin A &amp; C necessary for normal immune function as part of a healthy nutritious diet.</em></p>

<h3>Ingredients</h3>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2021/09/1._RZBP-01216_Boost_Product_Ingredient_NIPs_Update_AUGUST2021.pdf" target="_blank">Boost Ingredients</a></p>

<p><strong>Boost Yoghurt</strong><br />
Our smoothies contain our very own exclusive frozen yoghurt, sorbet and frozen coconut cream. Our low-fat frozen yoghurt contains the live probiotic cultures Streptococcus Thermophilus, Lactobacillus Acidophilus and Bifidobacterium.</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2016/08/RZBM-00832_Boost_Product_Frozen_Yoghurt_NIPs_2021_FEB.pdf" target="_blank">Boost Yoghurt Fact Sheet</a></p>

<p><strong>Individually Quick Frozen</strong><br />
Some of the fruit used in our Smoothies is IQF (Individually Quick Frozen). This ensures we get the fruit at its absolute optimum quality as it is snap frozen to lock in all the goodness. It also ensures we deliver a consistent product each time.</p>

<p><strong>Milk Products</strong><br />
Everyone&rsquo;s bodies (and tastebuds) are different, which is why we have a range of milk products to choose from so you don&rsquo;t miss out on your favourite milk-based smoothie. Choose from Low Fat Milk, Soy Milk, Oat Milk or Almond Milk as the base of your smoothie. Coconut Milk is also used as an ingredient in several of our drinks to add a refreshing coconut flavour.</p>

<p>Please note that the full alternative milk range may not be available in all stores.</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2019/09/RZPSB-10718-Boost-Product_Milk-Products-NIPs.pdf" target="_blank">Milk Products Fact Sheet</a></p>

<p><strong>Ambient Range</strong><br />
Just like there is no Batman without Robin, there is no smoothie without a base. The ambient range is used as the base for some of our most popular smoothies. Made using no GMO, an ambient base is just the beginning of our delicious smoothies.&nbsp;</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2021/08/RZBJ-00432_Boost_Product_Ambient_Range_NIPs_Updated_Sept_2020_2.pdf">Ambient Range Fact Sheet</a></p>

<p><strong>Protein Balls</strong><br />
Small balls packed full of yummy goodness. A delicious energy boost that is the perfect &lsquo;anytime&rsquo; snack!</p>

<p><strong>Popcorn</strong><br />
We stock popcorn for your enjoyment. Choose between &lsquo;Sea Salt&lsquo; and &lsquo;Lightly Salted, Slightly Sweet&rsquo;</p>

<p><strong>Banana Bread</strong><br />
Boost Banana Breads is amazing when toasted on a cold Winter&rsquo;s day, or is the perfect companion to a smoothie or juice to help satisfy your hunger craving when out and about or during your next lunch break. Also perfect for brekkie with a smoothie!</p>

<p><a href="https://www.boostjuice.com.au/wp-content/uploads/2021/09/2._RZBP-01241_Boost_Snacks_NIPs_AUGUST_2021.pdf">Boost Snack Range Fact Sheet</a></p>

<p><em>Please note that the full snack range may not be available in all stores.</em></p>

<p>&nbsp;</p>

<h3>Retail Range</h3>

<p>Can&rsquo;t get to a Boost Juice Bar for the great taste of freshly squeezed juice? Well now you have no excuse &lsquo;cause Boost&rsquo;s Natural Born Juice range is here! Thick and luscious fresh based juices, high in puree.</p>

<p>Available in both 350ml and 1 Litre sizes, and you can find them at selected Coles and Woolworths,. The range comes in 4 splendid flavours.</p>

<p><strong>Energy Lift</strong><br />
pear, orange, mango, pineapple &amp; apple</p>

<p><strong>All Berry Bang</strong><br />
apple, banana, blueberry, strawberry, raspberry and mango</p>

<p><strong>Berry Happy</strong><br />
apple, pineapple, blueberry, strawberry &amp; raspberry</p>

<p><strong>Mango Tango</strong><br />
apple, mango, orange and passionfruit</p>

<p>No artificial anything<br />
No juice concentrates<br />
No preservatives<br />
No artificial colours or flavours<br />
Direct from the fruit to you!</p>',
                'short_description' => '<p>If you want to know all the facts, figures and allergy advice on our products, you&rsquo;ll find all the info you need right here.</p>',
                'view' => 6,
                'status' => 1,
                'created_at' => '2022-02-08 03:47:10',
                'updated_at' => '2022-02-10 04:54:30',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}