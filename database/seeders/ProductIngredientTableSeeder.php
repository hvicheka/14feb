<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductIngredientTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_ingredient')->delete();
        
        \DB::table('product_ingredient')->insert(array (
            0 => 
            array (
                'product_id' => 1,
                'ingredient_id' => 2,
            ),
            1 => 
            array (
                'product_id' => 1,
                'ingredient_id' => 3,
            ),
            2 => 
            array (
                'product_id' => 1,
                'ingredient_id' => 4,
            ),
            3 => 
            array (
                'product_id' => 2,
                'ingredient_id' => 1,
            ),
            4 => 
            array (
                'product_id' => 2,
                'ingredient_id' => 3,
            ),
            5 => 
            array (
                'product_id' => 2,
                'ingredient_id' => 5,
            ),
        ));
        
        
    }
}