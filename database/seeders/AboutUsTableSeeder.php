<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AboutUsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('about_us')->delete();
        
        \DB::table('about_us')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'The Boost Juice Story',
                'image' => '2022-02-07-6200c091bd2a1.jpg',
                'description' => '<p>Back in 2000, a woman with no business experience &ndash; but unlimited passion and family support &ndash; opened her first juice bar in Adelaide.</p>

<p>The idea was brilliantly simple: make healthy living tasty and fun.</p>

<p>The woman was Australian adventurer and entrepreneur Janine Allis. The bar was Boost Juice &ndash; now one of the most popular and loved juice and smoothie brands across the globe, with over 580 stores across 13 different countries and counting.</p>',
                'style' => NULL,
                'created_at' => '2022-02-07 06:47:45',
                'updated_at' => '2022-02-07 06:47:45',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Boost from the beginning',
                'image' => '2022-02-07-6200c1838cc3f.jpg',
                'description' => '<p>Today, the world can&rsquo;t get enough of Boost Juice. The brand&rsquo;s winning combination of fresh fruit and veg, tasty products and innovative marketing campaigns has been a remarkable success story.</p>

<p>But there&rsquo;s no elevator to success. You have to take the stairs.</p>

<p>On a trip to the United States back in 1999, something caught Janine&#39;s attention. Everyone was into healthy smoothies and fresh juices - while in Australia, the fast food market was uninspiring and unhealthy. Janine had just had her third son and, like many young mothers, wanted more flexibility to run her own race..</p>',
                'style' => NULL,
                'created_at' => '2022-02-07 06:51:47',
                'updated_at' => '2022-02-07 06:51:47',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Growing Strong',
                'image' => '2022-02-07-6200c1bb3a964.jpg',
                'description' => '<p>They say success is 10% inspiration and 90% perspiration. And who&rsquo;s going to argue with that? Little did Janine know that her plan to not work a 40-hour week would actually mean working a 100-hour week, with no money coming in for a good three years.</p>

<p>But hard work is often rewarded, and the success of the first King William St store allowed Janine and Jeff to open three new sites &ndash; including two in one day.</p>

<p>Sometimes, the best way to learn is to jump in at the deep end, and that&rsquo;s exactly what happened next. Janine and Jeff signed on for 28 leases at Westfield Shopping Centres. It meant a personal liability of over $5 million and a deadline to open every store in just 18 months. Thanks to sheer determination and amazing franchise partners, it happened on time and on budget.</p>

<p>&nbsp;</p>

<p>Just three years later, Janine purchased rival chain Viva Juice and sold the rebranded stores to franchise partners.</p>',
                'style' => NULL,
                'created_at' => '2022-02-07 06:52:43',
                'updated_at' => '2022-02-07 06:52:43',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'A love for life infuses everything we do',
                'image' => '2022-02-07-6200c1ec2b26e.jpg',
                'description' => '<p>It&rsquo;s a philosophy that has taken us far. Building one of Australia&rsquo;s most well-loved brands was never going to be easy. But thanks to a positive outlook, hard work and the help of extraordinary people, Boost has grown to over 580 stores worldwide.</p>

<p>Boost Juice has come a long way from its humble beginnings. But one thing hasn&rsquo;t changed &ndash; our commitment to health, fun and loving life.</p>',
                'style' => NULL,
                'created_at' => '2022-02-07 06:53:32',
                'updated_at' => '2022-02-07 06:53:32',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}