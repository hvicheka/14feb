<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
//        $this->call([
//            UserSeeder::class
//        ]);
        $this->call(AboutUsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(HealthBenefitsTableSeeder::class);
        $this->call(IngredientsTableSeeder::class);
//        $this->call(PageFaqsTableSeeder::class);
        $this->call(PageSlidesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
//        $this->call(ProductCategoryTableSeeder::class);
//        $this->call(ProductHealthBenefitTableSeeder::class);
//        $this->call(ProductIngredientTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(SizesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(WellbeingsTableSeeder::class);
    }
}
