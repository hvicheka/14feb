<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\AboutUsController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\HealthBenefitController;
use App\Http\Controllers\Admin\IngredientController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\WellbeingController;
use App\Http\Controllers\CKeditorController;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test', function () {
    return view('back-end.dashboard');
});

Route::group(['as' => 'front_end.'], function () {
    Route::get('/', [FrontendController::class, 'home'])->name('home');
    Route::get('/drinks', [FrontendController::class, 'products'])->name('drinks');
    Route::get('/drink/{slug}', [FrontendController::class, 'product_detail'])->name('drinks.detail');
    Route::get('/stores', [FrontendController::class, 'store'])->name('stores');
    Route::get('/vibe', [FrontendController::class, 'vibe'])->name('vibe');
    Route::get('/about-us', [FrontendController::class, 'about_us'])->name('about_us');
    Route::get('/contact-us', [FrontendController::class, 'contact'])->name('contact');
    Route::get('/jobs', [FrontendController::class, 'jobs'])->name('jobs');

    Route::get('/privacy-policy', [FrontendController::class, 'privacy'])->name('privacy_policy');
    Route::get('/terms-and-conditions', [FrontendController::class, 'term_and_condition'])->name('term');

    Route::group(['prefix' => 'news'], function () {
        Route::get('/', [FrontendController::class, 'posts'])->name('news.index');
        Route::get('/{slug}', [FrontendController::class, 'postShow'])->name('news.detail');
    });

    Route::group(['prefix' => 'wellbeing'], function () {
        Route::get('/', [FrontendController::class, 'wellbeing'])->name('wellbeing.index');
        Route::get('/{slug}', [FrontendController::class, 'wellbeing_detail'])->name('wellbeing.detail');
    });

    Route::group(['prefix' => 'franchising', 'as' => 'franchising.'], function () {
        Route::get('/', [FrontendController::class, 'franchising'])->name('index');
    });
});


Route::group(['middleware' => ['auth'], 'prefix' => 'dashboard', 'as' => 'admin.'], function () {
    Route::get('/', function () {
        return view('back-end.dashboard');
    })->name('dashboard');
    Route::resource('categories', CategoryController::class);
    Route::resource('benefits', HealthBenefitController::class);
    Route::resource('ingredients', IngredientController::class);
    Route::resource('products', ProductController::class);
    Route::resource('posts', PostController::class);
    Route::resource('wellbeing', WellbeingController::class);
    Route::resource('about', AboutUsController::class);
    Route::resource('users', UserController::class);

    Route::get('profile', [ProfileController::class, 'profile'])->name('profile');
    Route::put('profile', [ProfileController::class, 'update_profile'])->name('profile.update');
    Route::post('password', [ProfileController::class, 'change_password'])->name('profile.change_password');

    Route::resource('settings', SettingController::class)->except(['delete']);

    Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {

        Route::get('{page}/setting', [PageController::class, 'index'])->name('index');
        Route::put('{page}/setting', [PageController::class, 'update'])->name('update');

        Route::get('/vibe', [PageController::class, 'vibe_page'])->name('vibe');
        Route::put('/vibe', [PageController::class, 'update_vibe_page'])->name('vibe.update');

        Route::resource('{page}/faq', FaqController::class);
    });
});

Route::post('ckeditor/upload', [CKeditorController::class, 'upload'])->name('ckeditor.image-upload');


Auth::routes([
    'login' => true,
    'register' => false,
    'reset' => false
]);

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
