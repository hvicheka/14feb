function whichTransitionEvent() {
    var i, e = document.createElement("fakeelement"), t = {
        transition: "transitionend",
        OTransition: "oTransitionEnd",
        MozTransition: "transitionend",
        WebkitTransition: "webkitTransitionEnd"
    };
    for (i in t) if (void 0 !== e.style[i]) return t[i]
}

function nutritionInfo() {
    $(".tabs-nav a").on("click", function (i) {
        var e = $(".tabs-nav a").index(this);
        console.log(e), $(".tabs-nav a").removeClass("active"), $(this).addClass("active"), $(".nutrition-info .info-table").hide(), $(".nutrition-info .info-table:eq( " + e + " )").fadeIn(800), i.preventDefault()
    })
}

function triggerAnimation(i) {
    i = "" === i ? "" : i, loadNewContent(i)
}

function loadNewContent(i) {
    console.log('load new')
}

function resetGridInitial() {
    $(".og-close").click(), "" === filters && "" === $("#ingredient").tokenInput("get") || setTimeout(function () {
        $("#benefits li, #type li").removeClass("active"), filters = [], filterAnimation(), resetGrid(), "" != $("#ingredient").tokenInput("get") && $("#ingredient").tokenInput("clear")
    }, 100)
}

function hintHits(i, e) {
    $("#benefits li").removeClass("active"), $("#ingredient").tokenInput("clear"), $("#ingredient").tokenInput("add", {
        id: "." + i,
        name: e
    }), filters = [], drinks = "ul#og-grid > li:not(." + i + ")", filterAnimation(drinks)
}

function filterAnimation(i) {
    $("ul#og-grid > li a").addClass("show-ing"), setFilter = i, $("ul#og-grid > li").addClass("hide"), setTimeout(function () {
        $("ul#og-grid > li").removeAttr("style"), $("ul#og-grid > li").hide(), $("ul#og-grid > li").addClass("hide"), $("ul#og-grid > li").show(), $(setFilter).hide(), setTimeout(function () {
            $("ul#og-grid > li").removeClass("hide")
        }, 20), console.log($("ul#og-grid > li:visible").length), 0 == $("ul#og-grid > li:visible").length ? $("#hints").fadeIn() : $("#hints").hide(), console.log("showing: " + $("ul#og-grid > li:visible").length)
    }, 220)
}

function resetGrid() {
    $(".token-input-list li").length > 1 && console.log("more than 2"), 1 == $(".token-input-list li").length ? (triggetFilterSearch(), filterAnimation(), $("#hints").hide()) : triggetFilterSearch()
}

function closeWindow() {
    $(".og-expander").is("div") && $(".og-close").click()
}

function triggetFilterSearch() {
    "" != filters || "" != $("#ingredient").tokenInput("get") ? $("#view-all-sidebar").addClass("active") : $("#view-all-sidebar").removeClass("active"), gridDrinks && ($(gridDrinks).appendTo("ul#og-grid").each(function () {
        var i = $(this).data("drinkIndex");
        $("ul#og-grid > li.drink-block").eq(i).before(this)
    }), gridDrinks = null, $("ul#og-grid > li.drink-block").removeClass("og-expandedfd")), finalFilters && $("#ingredient").val() ? (drinks = "ul#og-grid > li:not(." + finalFilters + $("#ingredient").val() + ")", filterAnimation(drinks)) : finalFilters ? (drinks = "ul#og-grid > li:not(." + finalFilters + ")", filterAnimation(drinks)) : $("#ingredient").val() ? (drinks = "ul#og-grid > li:not(" + $("#ingredient").val() + ")", filterAnimation(drinks)) : ($("ul#og-grid > li").addClass("hide"), $(this).one(transitionEvent, function (i) {
        $("ul#og-grid > li").removeAttr("style"), $("ul#og-grid > li").show(), setTimeout(function () {
            $("ul#og-grid > li").removeClass("hide")
        }, 20)
    })), setTimeout(function () {
        gridDrinks || (gridDrinks = $('ul#og-grid > li.drink-block[style*="display: none"]').detach()), Grid.refreshDrinksGrid(), 0 != $("ul#og-grid > li:visible").length && $("#hints").hide()
    }, 500), closeWindow()
}

$(".preloader").show();
var transitionEvent = whichTransitionEvent();
window.directEnter = !1;
var newPageArray = location.pathname.split("/"), newPage = newPageArray[newPageArray.length - 2], isAnimating = !1,
    firstLoad = !1, naviControl = !1, newScaleValue = 1, filters = [], finalFilters = "", imgLoadCount = 0;
categories = 0;
var dashboard = $(".og-grid"), mainContent = $(".cd-main"), gridDrinks;
$("ul#og-grid > li.drink-block").each(function (i) {
    $(this).data("drinkIndex", i)
}), mainContent.on("click", ".cd-scroll", function (i) {
    var e = $(this.hash);
    $(e).velocity("scroll", {container: $(".cd-section")}, 200), i.preventDefault()
}), $("#main-filter-sections li").on("click", function (i) {
    $("#main-filter-sections li, #view-all-sidebar").removeClass("active"), $(this).addClass("active"), "section-benefits" == i.target.id && ($("#type").hide(), $("#benefits").show()), "section-type" == i.target.id && ($("#benefits").hide(), $("#type").show()), resetGridInitial()
}), $("#view-all-sidebar").on("click", function (i) {
    resetGridInitial()
}), $("#type li").on("click", function () {
    categories = 1
}), $("#benefits li").on("click", function () {
    categories = 0
}), $("#benefits li, #type li").on("click", function () {
    if (1 == categories && (filters = [], $("#type li").removeClass("active")), /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? $("html, body").stop().animate({scrollTop: $("#search-filter").position().top}, 0, "swing") : $("html, body").stop().animate({scrollTop: $("#search-filter").position().top}, 800, "swing"), $(this).toggleClass("active"), $(this).hasClass("active")) {
        var i = $.inArray(this.className, filters);
        i >= 0 ? filters.splice(i, 1) : filters.push(this.className)
    } else {
        var e = $.inArray(this.className + " active", filters);
        filters.splice(e, 1)
    }
    finalFilters = filters.toString().replace(/ active/g, ""), finalFilters = finalFilters.replace(/,/g, "."), console.log(finalFilters), triggetFilterSearch(), event.preventDefault()
}), $(window).load(function () {
    $("#og-grid li > a").each(function (i) {
        var e = $(window).scrollTop(), t = ($(window).height(), $(this).offset().top);
        $(this).height()
    }), $(window).on("scroll", function () {
        $("#og-grid li > a").each(function (i) {
            var e = $(window).scrollTop(), t = e + $(window).height() + 400, n = $(this).offset().top;
            n + $(this).height() <= t && n >= e && $(this).addClass("show-ing")
        })
    }), $(function () {
        Grid.init()
    }), window.tutorialMode = !0, console.log(window.tutorialMode);
    var i = $("#main-filter").offset().top - 460, e = $(window);
    e.scroll(function () {
        e.scrollTop() >= i && 1 == window.tutorialMode && (window.tutorialMode = !1)
    }), $("#main-filter #tutorial span").on("click", function (i) {
        $("#main-filter #tutorial").fadeOut(), window.tutorialMode = !1
    });
    var i = $("#view-all-sidebar").offset().top - 100, e = $(window);
    e.scroll(function () {
        e.scrollTop() >= i ? $("#view-all-sidebar").addClass("fixed") : $("#view-all-sidebar").removeClass("fixed")
    })
}), $(window).on("load", function () {
    $(".preloader").hide(), setTimeout(function () {
        console.log(newPage), $("#" + newPage).click(), window.directEnter = !0
    }, 1e3)
});
