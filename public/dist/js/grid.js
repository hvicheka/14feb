!function () {
    function i(i) {
        return document.getElementById(i)
    }

    function e() {
        function e() {
            a += 1;
            var i = (100 / tot * a << 0) + "%";
            if (s.style.width = i, o.innerHTML = "Blending " + i, a === tot) return t()
        }

        function t() {
            setTimeout(function () {
                n.style.display = "none"
            }, 1200)
        }

        var n = i("drinks-overlay"), s = i("progress"), o = i("progstat"), r = document.images, a = 0;
        tot = r.length;
        for (var h = 0; h < tot; h++) {
            var d = new Image;
            d.onload = e, d.onerror = e, d.src = r[h].src
        }
    }

    document.addEventListener("DOMContentLoaded", e, !1)
}();
var $event = $.event, $special, resizeTimeout;
window.closeStatus = !0, $special = $event.special.debouncedresize = {
    setup: function () {
        $(this).on("resize", $special.handler)
    }, teardown: function () {
        $(this).off("resize", $special.handler)
    }, handler: function (i, e) {
        var t = this, n = arguments, s = function () {
            i.type = "debouncedresize", $event.dispatch.apply(t, n)
        };
        resizeTimeout && clearTimeout(resizeTimeout), e ? s() : resizeTimeout = setTimeout(s, $special.threshold)
    }, threshold: 250
};
var liHeight = $("#og-grid li").height(), Grid = function () {
    function i(i) {
        E = $.extend(!0, {}, E, i), e(!0), o(), n()
    }

    function e(i) {
        g.each(function () {
            var e = $(this);
            e.data("offsetTop", e.offset().top), i && e.data("height", e.height())
        })
    }

    function t() {
        console.log("refreshDrinksGrid()"), p = 0, u = -1, e(), o(), void 0 !== $.data(this, "preview") && h()
    }

    function n() {
        s(g), /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || f.on("debouncedresize", function () {
            p = 0, u = -1, e(), o(), void 0 !== $.data(this, "preview") && h()
        })
    }

    function s(i) {
        i.on("click", "span.og-close", function () {
            return h(), !1
        }).children("a").on("click", function (i) {
            var e = $(this).parent();
            return l === e.index() ? h() : r(e), !1
        })
    }

    function o() {
        d = {width: f.width(), height: f.height()}
    }

    function r(i) {
        var e = $.data(this, "preview"), t = i.data("offsetTop");
        if (p = 0, void 0 !== e) {
            if (u === t) return e.update(i), !1;
            t > u && (p = e.height), h()
        }
        u = t, e = $.data(this, "preview", new a(i)), e.open()
    }

    function a(i) {
        this.$item = i, this.expandedIdx = this.$item.index(), this.create(), this.update()
    }

    function h() {
        l = -1, $.data(this, "preview").close(), $.removeData(this, "preview"), window.closeStatus = !0, setTimeout(function () {
        }, 600)
    }

    var d, c = $("#og-grid"), g = c.children("li"), l = -1, u = -1, p = 0, f = $(window), v = $("html, body"), m = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd",
            msTransition: "MSTransitionEnd",
            transition: "transitionend"
        }, w = m[Modernizr.prefixed("transition")], T = Modernizr.csstransitions,
        E = {minHeight: 1140, speed: 600, easing: "ease"};
    return a.prototype = {
        create: function () {
            this.$details = $('<div class="og-details"></div>'), this.$previewEl = $('<div class="og-expander"></div>'), this.$item.append(this.getEl()), T && this.setTransition()
        }, update: function (i) {
            if (i && (this.$item = i), -1 !== l) {
                g.eq(l).removeClass("og-expanded"), this.$item.addClass("og-expanded"), this.positionPreview()
            }
            l = this.$item.index();
            var e = this;
            void 0 !== e.$largeImg && e.$largeImg.remove()
        }, open: function () {
            setTimeout($.proxy(function () {
                this.setHeights(), this.positionPreview()
            }, this), 25)
        }, close: function () {
            $("#og-grid > li").removeClass("og-expanded"), $("#og-grid > li").css("height", liHeight), $(".og-expander").css("height", 0);
            var i = this, e = function () {
                T && $(this).off(w), i.$item.removeClass("og-expanded"), i.$previewEl.remove()
            };
            return setTimeout($.proxy(function () {
                void 0 !== this.$largeImg && this.$largeImg.fadeOut("fast"), this.$previewEl.css("height", 0), this.$previewEl.remove();
                var i = g.eq(this.expandedIdx);
                i.css("height", i.data("height")).on(w, e), T || e.call()
            }, this), 100), !1
        }, calcHeight: function () {
            var i = d.height - this.$item.data("height") - 10, e = d.height;
            i < E.minHeight && (i = E.minHeight, e = E.minHeight + this.$item.data("height") + 10), this.height = i, this.itemHeight = e
        }, setHeights: function () {
            var i = this, e = function () {
                T && i.$item.off(w), i.$item.addClass("og-expanded")
            };
            this.calcHeight(), this.$previewEl.css("height", this.height), this.$item.css("height", this.itemHeight).on(w, e), T || e.call()
        }, positionPreview: function () {
            var i = this.$item.data("offsetTop"), e = this.$previewEl.offset().top - p,
                t = this.height + this.$item.data("height") + 10 <= d.height ? i : this.height < d.height ? e - (d.height - this.height) : e;
            t -= 340;
            var n = this.$item.data("offsetTop");
            /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? $("html,body").animate({scrollTop: n}, 0) : v.animate({scrollTop: t}, E.speed)
        }, setTransition: function () {
            /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? (this.$previewEl.css("transition", "height 100ms " + E.easing), this.$item.css("transition", "height 100ms " + E.easing)) : (this.$previewEl.css("transition", "height " + E.speed + "ms " + E.easing), this.$item.css("transition", "height " + E.speed + "ms " + E.easing))
        }, getEl: function () {
            return this.$previewEl
        }
    }, {init: i, refreshDrinksGrid: t, Preview: a}
}();
