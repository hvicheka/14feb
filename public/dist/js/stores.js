jQuery(document).ready(function (e) {
    e(".boostform").css("height", e(".step-1").height() + 200), e("#storesinput-error").hide(), e(function () {
        function t() {
            e("input, textarea, select").removeClass("highlight"), e("input.error, textarea.error, select.error").first().addClass("highlight"), e("html, body").animate({scrollTop: e(".highlight").offset().top - 100}, 500)
        }

        function i() {
            e("fieldset").removeAttr("style"), e("#progressbar li + li").removeClass("active"), e(".thank-you").hide(), e(".form-elements").show(), e(".boostform").css("height", e(".step-1").height() + 200)
        }

        function o() {
            e(window).width() < 1024 ? e("html, body").animate({scrollTop: e("#progressbar").offset().top - 20}, 0) : e("html, body").animate({scrollTop: e("#progressbar").offset().top - 100}, 0)
        }

        function s() {
            e("#applynow").ajaxForm({
                beforeSubmit: function () {
                    e(".step-3 .preloader").show()
                }, success: function (t) {
                    e(".form-elements").hide(), e(".thank-you").fadeIn(), e("#resetForm").click(function () {
                        i()
                    }), e(".boostform").css("height", e(".step-3").height() + 300), e(".step-3 .preloader").hide(), e("html, body").animate({scrollTop: e("#progressbar").offset().top - 20}, 300)
                }, complete: function (t) {
                    e("form").trigger("reset")
                }
            })
        }

        function r() {
            if (d) return !1;
            d = !0, e("#progressbar li").eq(e("fieldset").index(l)).addClass("active"), l.show(), n.animate({opacity: 0}, {
                step: function (e, t) {
                    h = 1 - .2 * (1 - e), c = 50 * e + "%", u = 1 - e, n.css({transform: "scale(" + h + ")"}), l.css({
                        left: c,
                        opacity: u
                    })
                }, duration: 800, complete: function () {
                    n.hide(), d = !1
                }, easing: "easeInOutBack"
            }), o()
        }

        function a() {
            if (d) return !1;
            d = !0, e("#progressbar li").eq(e("fieldset").index(n)).removeClass("active"), p.show(), n.animate({opacity: 0}, {
                step: function (e, t) {
                    h = .8 + .2 * (1 - e), c = 50 * (1 - e) + "%", u = 1 - e, n.css({left: c}), p.css({
                        transform: "scale(" + h + ")",
                        opacity: u
                    })
                }, duration: 800, complete: function () {
                    n.hide(), d = !1
                }, easing: "easeInOutBack"
            }), o()
        }

        var n, l, p, c, u, h, d;
        e.validator.addMethod("filesize", function (e, t, i) {
            return this.optional(t) || t.files[0].size <= i
        }, "File size must be less than {0}KB"), e("#applynow").validate({
            rules: {
                position: {required: !0},
                availabilityMonday: {required: !0},
                availabilityTuesday: {required: !0},
                availabilityWednesday: {required: !0},
                availabilityThursday: {required: !0},
                availabilityFriday: {required: !0},
                availabilitySaturday: {required: !0},
                availabilitySunday: {required: !0},
                firstname: {required: !0, minlength: 2},
                lastname: {required: !0, minlength: 2},
                email: {required: !0, email: !0},
                emailconfirm: {equalTo: "#email"},
                mobile: {required: !0, minlength: 10},
                attachment1: {required: !0, extension: "pdf|doc|docx", filesize: 2097152},
                attachment2: {required: !0, extension: "pdf|doc|docx", filesize: 2097152}
            },
            messages: {
                attachment1: {required: "Please upload your cover letter (only word documents or PDFs accepted)"},
                attachment2: {required: "Please upload your resume (only word documents or PDFs accepted)"}
            }
        }), e("#applynow .next1").click(function () {
            e("#applynow").valid(), n = e(this).parent(), l = e(this).parent().next(), e("#applynow").valid() && "" != e("#stores-input").tokenInput("get") ? (r(), e(".boostform").css("height", e(".step-2").height() + 200)) : "" == e("#stores-input").tokenInput("get") ? (e("#token-input-stores-input").addClass("error"), e("#storesinput-error").show(), t()) : t(), e("#token-input-stores-input").click(function () {
                e(this).removeClass("error")
            })
        }), e("#applynow .next2").click(function () {
            e("#applynow").valid(), n = e(this).parent(), l = e(this).parent().next(), e("#applynow").valid() ? (r(), e(".boostform").css("height", e(".step-3").height() + 300)) : (t(), e(".boostform").css("height", e(".step-2").height() + 200))
        }), e(".previous").click(function () {
            n = e(this).parent(), p = e(this).parent().prev(), a(), setTimeout(function () {
                "1" == e(".step-1").css("opacity") && e(".boostform").css("height", e(".step-1").height() + 200), "1" == e(".step-2").css("opacity") && e(".boostform").css("height", e(".step-2").height() + 200), e(".step-3").css("opacity")
            }, 810)
        }), e("#applynow").on("keyup keypress", function (e) {
            if (13 === (e.keyCode || e.which)) return e.preventDefault(), !1
        }), e("#applynow .submit").click(function () {
            if (e("#applynow").valid(), n = e(this).parent(), l = e(this).parent().next(), !e("#applynow").valid()) return t(), !1;
            s()
        })
    })
});
