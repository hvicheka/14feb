jQuery(document).ready(function (o) {
    !function () {
        o(".owl-carousel").owlCarousel({
            items: 1,
            nav: !0,
            autoplay: !0,
            autoplayTimeout: 7e3,
            smartSpeed: 1e3,
            dots: !1,
            loop: !0
        })
    }()
});
