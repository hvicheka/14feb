function triggerJobsGrid() {
    var i, t, e = $.event;
    i = e.special.debouncedresize = {
        setup: function () {
            $(this).on("resize", i.handler)
        }, teardown: function () {
            $(this).off("resize", i.handler)
        }, handler: function (n, s) {
            var o = this, h = arguments, a = function () {
                n.type = "debouncedresize", e.dispatch.apply(o, h)
            };
            t && clearTimeout(t), s ? a() : t = setTimeout(a, i.threshold)
        }, threshold: 250
    };
    var n = function () {
        function i(i) {
            j = $.extend(!0, {}, j, i), e(!0), o(), n()
        }

        function t(i) {
            p = p.add(i), i.each(function () {
                var i = $(this);
                i.data({offsetTop: i.offset().top, height: i.height()})
            }), s(i)
        }

        function e(i) {
            p.each(function () {
                var t = $(this);
                t.data("offsetTop", t.offset().top), i && t.data("height", t.height())
            })
        }

        function n() {
            s(p), u.on("debouncedresize", function () {
                g = 0, f = -1, e(), o(), void 0 !== $.data(this, "preview") && a()
            })
        }

        function s(i) {
            /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || i.on("click", "span.og-close", function () {
                return a(), !1
            }).children("a").on("click", function (i) {
                var t = $(this).parent();
                return l === t.index() ? a() : h(t), !1
            })
        }

        function o() {
            d = {width: u.width(), height: u.height()}
        }

        function h(i) {
            var t = $.data(this, "preview"), e = i.data("offsetTop");
            if (g = 0, void 0 !== t) {
                if (f === e) return t.update(i), !1;
                e > f && (g = t.height), a()
            }
            f = e, t = $.data(this, "preview", new r(i)), t.open()
        }

        function a() {
            l = -1, $.data(this, "preview").close(), $.removeData(this, "preview")
        }

        function r(i) {
            this.$item = i, this.expandedIdx = this.$item.index(), this.create(), this.update()
        }

        var d, c = $("#boost-jobs"), p = c.children("li"), l = -1, f = -1, g = 0, u = $(window), v = $("html, body"),
            m = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd",
                msTransition: "MSTransitionEnd",
                transition: "transitionend"
            }, b = m[Modernizr.prefixed("transition")], w = Modernizr.csstransitions,
            j = {minHeight: 960, speed: 350, easing: "ease"};
        return r.prototype = {
            create: function () {
                this.$applynow = $("<p></p>"), this.$job = $("<p></p>"), this.$reference = $("<p></p>"), this.$location = $("<p></p>"), this.$description = $("<div></div>"), this.$jobdetails = $('<div class="eight columns"></div>').append(this.$description), this.$jobinfo = $('<div class="four columns"></div>').append(this.$job, this.$reference, this.$location, this.$applynow), this.$closePreview = $('<span class="og-close"></span>'), this.$previewInner = $('<div class="boost-jobs-details-inner row"></div>').append(this.$closePreview, this.$jobdetails, this.$jobinfo), this.$previewEl = $('<div class="boost-jobs-details"></div>').append(this.$previewInner), this.$item.append(this.getEl()), w && this.setTransition()
            }, update: function (i) {
                if (i && (this.$item = i), -1 !== l) {
                    p.eq(l).removeClass("boost-jobs-expanded"), this.$item.addClass("boost-jobs-expanded"), this.positionPreview()
                }
                l = this.$item.index();
                var t = this.$item.children("a"), e = {
                    applynow: t.data("applynow"),
                    job: t.data("job"),
                    reference: t.data("reference"),
                    location: t.data("location"),
                    description: $(t).find(".job-description").html()
                };
                this.$job.html("<strong>Job: </strong>" + e.job), this.$reference.html("<strong>Reference: </strong>" + e.reference), this.$location.html("<strong>Location: </strong>" + e.location), this.$applynow.html('<a href="apply-now/?id=' + e.applynow + '" class="button">Apply for this job</a>'), this.$description.html(e.description)
            }, open: function () {
                setTimeout($.proxy(function () {
                    this.setHeights(), this.positionPreview()
                }, this), 25)
            }, close: function () {
                var i = this, t = function () {
                    w && $(this).off(b), i.$item.removeClass("boost-jobs-expanded"), i.$previewEl.remove()
                };
                return setTimeout($.proxy(function () {
                    void 0 !== this.$largeImg && this.$largeImg.fadeOut("fast"), this.$previewEl.css("height", 0);
                    var i = p.eq(this.expandedIdx);
                    i.css("height", i.data("height")).on(b, t), w || t.call()
                }, this), 25), !1
            }, calcHeight: function () {
                var i = d.height - this.$item.data("height") - 10, t = d.height;
                i < j.minHeight && (i = j.minHeight, t = j.minHeight + this.$item.data("height") + 10), this.height = i, this.itemHeight = t
            }, setHeights: function () {
                var i = this, t = function () {
                    w && i.$item.off(b), i.$item.addClass("boost-jobs-expanded")
                };
                this.calcHeight(), this.$previewEl.css("height", this.height), this.$item.css("height", this.itemHeight).on(b, t), w || t.call()
            }, positionPreview: function () {
                var i = this.$item.data("offsetTop"), t = this.$previewEl.offset().top - g,
                    e = this.height + this.$item.data("height") + 10 <= d.height ? i : this.height < d.height ? t - (d.height - this.height) : t;
                v.animate({scrollTop: e}, j.speed)
            }, setTransition: function () {
                this.$previewEl.css("transition", "height " + j.speed + "ms " + j.easing), this.$item.css("transition", "height " + j.speed + "ms " + j.easing)
            }, getEl: function () {
                return this.$previewEl
            }
        }, {init: i, addItems: t}
    }();
    $(function () {
        n.init()
    })
}

jQuery(document).ready(function (i) {
    triggerJobsGrid()
});
