jQuery(document).ready(function (e) {
    function i() {
        e(".owl-carousel").owlCarousel({
            items: 1,
            nav: !0,
            autoplay: !0,
            autoplayTimeout: 5e3,
            smartSpeed: 800,
            dots: !1,
            loop: !0
        })
    }

    function o() {
        e("#vibe-login").removeClass("show"), e("#vibe-register-form input#reg-viber-card-trigger").each(function () {
            var i = e(this), o = i.next(), t = o.text();
            o.remove(), i.iCheck({
                checkboxClass: "icheckbox_lineblock-green",
                radioClass: "iradio_lineblock-green",
                insert: '<div class="icheck_line-icon"></div>' + t
            })
        })
    }

    function t() {
        var i = 0;
        e("#terms").is(":checked") && (i = 1);
        var o = {
            boostienumber: e("#reg-boostie-number").val(),
            verificationcode: e("#reg-verification-code").val(),
            firstname: e("#reg-user-first-name").val(),
            lastname: e("#reg-user-last-name").val(),
            email: e("#reg-user-email").val(),
            password: e("#reg-user-pass").val(),
            dobdate: e("#reg-dob-date").val(),
            dobmonth: e("#reg-dob-month").val(),
            dobyear: e("#reg-dob-year").val(),
            terms: i,
            captcha: grecaptcha.getResponse()
        };
        e.ajax({
            type: "POST",
            url: "/api/vibe/register.php",
            headers: o,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                e(".preloader").show()
            },
            success: function (i) {
                e("#register-output").html(""), e(".preloader").hide(), 1 === i.Success ? (e("#vibe-register-form").hide(), e("html, body").animate({scrollTop: e("#i-want-in").offset().top - 20}, 300), e("#user-email").val(e("#reg-user-email").val()), e("#user-pass").val(e("#reg-user-pass").val()), e("#after-success").show(), e("#trigger-login").on("click", function (i) {
                    e("#after-success").hide(), r(), i.preventDefault()
                })) : e.each(i, function (i, o) {
                    e("#register-output").fadeIn(), null !== o && e("#register-output").append(o), e(".boostform").css("height", e(".form-step-2").height() + 100)
                })
            },
            error: function (e) {
                console.log("error - Boost-04")
            }
        })
    }

    function r() {
        var i = {email: e("#user-email").val(), pass: e("#user-pass").val()};
        window.Gemail = e("#user-email").val(), e.ajax({
            type: "POST",
            url: "/api/vibe/user.php",
            headers: i,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                e("#vibe-login .preloader").show()
            },
            success: function (i) {
                e("#vibe-login .preloader").hide(), 1 === i.Success ? (window.location.href = "/vibe/", e("#vibe-quick-view-info").html("<p>Welcome " + i.GivenNames + " - " + i.MemberNo + "</p><p>Vibe cash: " + i.Money + " / Points: " + i.Points + "</p>"), e("#vibe-login-form").addClass("hide"), e("#vibe-login-form em").removeClass("show"), e("#vibe-quick-view").addClass("show")) : e("#vibe-login-form em").addClass("show"), window.Gtoken = i.AuthToken
            },
            error: function (e) {
                console.log("error - Boost-04")
            }
        })
    }

    e(".boostform").css("height", e(".form-step-1").height() + 440);
    var n = "";
    n = e(".boost-carousel").detach(), e.ajax({
        type: "GET",
        url: "/api/vibe/check-user.php",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            e(".preloader").show()
        },
        success: function (t) {
            1 === t.Success ? e("#my-vibe").load("/wp-content/themes/boostjuice/vibe-overview.php", function (o, r, s) {
                if ("success" == r) {
                    e("#vibe-welcome").remove(), n.appendTo("#my-vibe"), e(".preloader").fadeOut();
                    var a = e(".owl-carousel");
                    i(), a.on("changed.owl.carousel", function (e) {
                    }), Tempo.prepare(e("#viber-member-full-view")).render(t), e("#barcode").attr("src", e("#barcode").attr("data-src")), e("#barcode").removeAttr("data-src"), null == t.Orders ? (e("#transactions").hide(), e("#no-transactions").show()) : (e("#transactions").show(), e("#no-transactions").hide()), Tempo.prepare("recent-transactions", {escape: !1}).when(TempoEvent.Types.RENDER_STARTING, function (e) {
                    }).when(TempoEvent.Types.RENDER_COMPLETE, function (i) {
                        p = e("#recent-transactions").detach(), p.appendTo("#quick-history"), p = null, e("#quick-list li").on("click", function () {
                            e(this).toggleClass("close"), e(this).find("ul").toggleClass("show-details"), e("#quick-list li").not(this).each(function () {
                                e(this).removeClass("close"), e(this).find("ul").removeClass("show-details")
                            })
                        }), e("#my-vibe").show(), e("#resend-activation").on("click", function (i) {
                            e.ajax({
                                type: "GET",
                                url: "/api/vibe/resend-activation.php",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                beforeSend: function () {
                                    console.log("loading"), e(".preloader").show()
                                },
                                success: function (i) {
                                    e(".preloader").hide(), swal("Good news!", "We just emailed you the activaion link!", "success")
                                },
                                error: function (e) {
                                    console.log("error - Boost-04")
                                }
                            }), i.preventDefault()
                        })
                    }).render(t.Orders)
                }
            }) : (o(), e("#vibe-login-main").show(), e("#my-vibe").show(), e(".preloader").fadeOut(), n.prependTo("#my-vibe-inner"), i())
        },
        error: function (i) {
            e("#loading-image").hide()
        }
    }), e("#trigger-header-login").on("click", function (i) {
        e(".my-vibe").click()
    }), e(function () {
        function i() {
            e(window).width() < 1024 ? e("html, body").animate({scrollTop: e("#progressbar").offset().top - 20}, 0) : e("html, body").animate({scrollTop: e("#progressbar").offset().top - 100}, 0)
        }

        function o() {
            e("input, textarea, select").removeClass("highlight"), e("input.error, textarea.error, select.error").first().addClass("highlight"), e("html, body").animate({scrollTop: e(".highlight").offset().top - 100}, 500)
        }

        function r() {
            if (u) return !1;
            u = !0, e("#progressbar li").eq(e("fieldset").index(a)).addClass("active"), a.show(), s.animate({opacity: 0}, {
                step: function (e, i) {
                    h = 1 - .2 * (1 - e), c = 50 * e + "%", d = 1 - e, s.css({transform: "scale(" + h + ")"}), a.css({
                        left: c,
                        opacity: d
                    })
                }, duration: 800, complete: function () {
                    s.hide(), u = !1
                }, easing: "easeInOutBack"
            }), i()
        }

        function n() {
            if (u) return !1;
            u = !0, e("#progressbar li").eq(e("fieldset").index(s)).removeClass("active"), l.show(), s.animate({opacity: 0}, {
                step: function (e, i) {
                    h = .8 + .2 * (1 - e), c = 50 * (1 - e) + "%", d = 1 - e, s.css({left: c}), l.css({
                        transform: "scale(" + h + ")",
                        opacity: d
                    })
                }, duration: 800, complete: function () {
                    s.hide(), u = !1
                }, easing: "easeInOutBack"
            }), i()
        }

        var s, a, l, c, d, h, u;
        e("#vibe-register-form").validate({
            rules: {
                reg_user_first_name: {required: !0, minlength: 2},
                reg_user_last_name: {required: !0, minlength: 2},
                reg_dob_date: {required: !0, minlength: 2, number: !0},
                reg_dob_month: {required: !0, minlength: 2, number: !0},
                reg_dob_year: {required: !0, minlength: 4, number: !0},
                reg_user_email: {required: !0, email: !0},
                reg_boostie_number: {required: !0, minlength: 10},
                reg_verification_code: {required: !0, minlength: 5},
                reg_user_pass: {required: !0},
                terms: {required: !0}
            }
        }), e("#vibe-register-form input").keyup(function () {
            if (this.value.length == this.maxLength) {
                e(this).next("input").length ? e(this).next("input").focus() : e(this).blur()
            }
        }), e("#vibe-register-form .next1").click(function () {
            e("#vibe-register-form").valid(), console.log(e("#vibe-register-form").valid()), s = e(this).parent(), a = e(this).parent().next(), e("#vibe-register-form").valid() ? (s = e(this).parent(), a = e(this).parent().next(), r(), e(".boostform").css("height", e(".form-step-2").height() + 100)) : o()
        }), e("#vibe-register-form .next2").click(function () {
            console.log("next"), s = e(this).parent(), a = e(this).parent().next(), r(), e(".boostform").css("height", e(".form-step-2").height() + 100)
        }), e(".previous").click(function () {
            console.log("back"), s = e(this).parent(), l = e(this).parent().prev(), n(), e(".boostform").css("height", e(".form-step-1").height() + 100)
        }), e("#register-trigger").click(function (i) {
            i.preventDefault(), e("#vibe-register-form").valid(), e("#vibe-register-form").valid() ? (console.log("SUCCESS!!!"), t()) : (o(), 1 == e("#terms").prop("checked") || e(".icheckbox_minimal").addClass("error"))
        })
    }), e("div.custom-inputs input").each(function () {
        var i = e(this), o = i.next(), t = o.text();
        o.remove(), i.iCheck({
            checkboxClass: "icheckbox_lineblock-green",
            radioClass: "iradio_lineblock-green",
            insert: '<div class="icheck_line-icon"></div>' + t
        })
    }), e("label.custom-input input").iCheck({
        checkboxClass: "icheckbox_minimal",
        radioClass: "iradio_minimal"
    }), function () {
        e("#state, #store-name, #date, #time, #tem-member, #drink-purchased").addClass("hideMe")
    }(), e("input#vibe_yes").on("ifChecked", function (i) {
        e("#reg-boostie-number, #reg-verification-code").removeClass("hideMe"), e(".boostform").css("height", e(".form-step-2").height() + 100)
    }), e("input#vibe_no").on("ifChecked", function (i) {
        e("#reg-boostie-number, #reg-verification-code").val(""), e("#reg-boostie-number, #reg-verification-code").addClass("hideMe"), e("#reg-boostie-number-error, #reg-verification-code-error").hide(), e(".boostform").css("height", e(".form-step-2").height() + 100)
    }), i();
    var s = [{testimonial: "“I downloaded the Boost app and it changed my entire life. Literally. I woke up the next day in another country in another person's body. Best app experience ever!”"}, {testimonial: "“I wasn’t feeling well the other day, so I went and visited the Doctor. He told me to download the Boost app and now I feel great. This app is medicinal magic!”"}, {testimonial: "“These other testimonials are obviously fake. The app is OK... I mean it’s worth a download. It would be cool if it came preinstalled on every phone from now on. Actually, it’s probably the best app to ever exist. Oh, and I definitely don’t work for Boost.”"}, {testimonial: "“My Financial Advisor told me to download this app and now I’m a billionaire. 10/10 would download again.”"}],
        a = Math.floor(Math.random() * s.length);
    e("#vibe-testimonail").html(s[a].testimonial), e(".vibe-login-trigger").on("click", function (i) {
        e(".my-vibe").click(), i.preventDefault()
    }), e(".vibe-login-trigger-mobile").on("click", function (i) {
        e(".boost-nav-trigger").click(), e(".my-vibe").click(), i.preventDefault()
    })
});