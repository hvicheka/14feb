jQuery(document).ready(function (e) {
    e(".expander li").on("click", function () {
        e(this).toggleClass("show-details"), e(".expander li").not(this).each(function () {
            e(this).removeClass("show-details")
        })
    }), e("#trigget-faq").on("click", function () {
        e("#vibe-terms").removeClass("show"), e("#faq").toggleClass("show"), e("#faq").hasClass("show") && setTimeout(function () {
            e("html, body").animate({scrollTop: e("#faq").offset().top - 100}, 1e3)
        }, 400)
    }), e("#trigger-terms").on("click", function () {
        e("#faq").removeClass("show"), e("#vibe-terms").toggleClass("show"), e("#vibe-terms").hasClass("show") && setTimeout(function () {
            e("html, body").animate({scrollTop: e("#vibe-terms").offset().top - 100}, 1e3)
        }, 1e3)
    }), e(".mobile-nav-expander > a").on("click", function (s) {
        window.innerWidth <= 768 && s.preventDefault(), e(this).parent().toggleClass("active")
    })
});
